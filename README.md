* Editar dentro del archivo `.env` las siguientes lineas: 
    * `ES_IP=0.0.0.0` , ingresando el ip local del servidor
    * `SOURCE_FILES_PATH=/source_files`, cambiando /source_files por el directorio raiz donde se encuentren los archivos o subdirectorios que contengan los fastq/fasta/gbfs
    * Definir el valor de la variable `MAX_CPUS` para limitar la cantidad de CPUs que se usan para procesar archivos FASTQs, FASTAs y GBFs.

* (Opcional) Para ejecutar docker sin sudo: 
    * sudo usermod -aG docker $USER
    * sudo chown $USER /var/run/docker.sock
    * cerrar sesion y volver a entrar

* Iniciar Elastic, Django (backend) y React (frontend):
    * `docker-compose build`
    * `docker-compose up`

* Una vez levantado los containers realizar los siguientes pasos:
    * Comentar las siguiente lineas (utilizando un # al comienzo de cada una de ellas):
        * django_api/django_api/settings.py: INSTALLED_APPS --> 'django.contrib.admin'
        * django_api/django_api/urls.py: urlpatterns --> admin/
    * `docker exec -it backend /code/manage.py makemigrations`
    * Descomentar las lineas comentadas previamente y ejecutar los siguientes comandos:
    * `docker exec -it backend /code/manage.py migrate`
    * `docker exec -it backend /code/manage.py collectstatic`
    * Para crear el superuser que permite acceder al panel de administración de Django, ejecutar:
        *  `docker exec -it backend /code/manage.py createsuperuser`

* Para agregar fastq / fasta / gbfs a un proyecto existen dos metodos:
    * Copiar los archivos a la carpeta que se crea para cada proyecto dentro del directorio projects de la aplicación.
    * Crear por consola un link simbolico (ln -s) a los archivos deseados que se encuentre dentro de la carpeta definida en el primer paso de la instalacion.

    _Si se utiliza el primer metodo, los archivos serán eliminados luego de ser procesados. Con el segundo método, solo se eliminarán los links simbólicos._

* Para limitar los recursos en el entrenamiento de IA:
    * Cantidad de CPUs: configurar al container del backend con el comando: `docker update --cpus 16 backend`, modificando 16 por la cantidad de cpus elegida (esta configuracion especifica supersede a la configuracion de cpus del primer item)
    * Cantidad de memoria del container del backend: `docker update --memory 6g --memory-swap 8g backend`, modificando --memory-swap por la cantidad de memoria total que puede usar el container, y --memory para definir cuanta de la memoria que define el parámetro --memory-swap se usa como memoria, y la diferencia queda como memoria swap del container. En el ejemplo: el container backend queda con 8g de memoria total, usa 6g como memoria y 2g quedan para la memoria swap.

Una vez finalizado los pasos anteriores, detener los containers (Ctrl+c una sola vez). Para iniciar la aplicacion de ahora en adelante, se puede utilizar el comando `docker-compose up -d` para que se ejecute en segundo plano.
