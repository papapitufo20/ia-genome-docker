from datetime import datetime
from elasticsearch import Elasticsearch
import os

es = Elasticsearch(
    ['192.168.0.198'], #'localhost'
    port=9200,
    http_auth=('elastic', 'matelisto'),
)

genomes_index = 'genomes-index'

def add_single_genome(genome, count):
    res = es.index(index=genomes_index, body=genome)
    print(res['result'] + ": " + str(count))

def add_genomes(genomes):
    count = 0
    for genome in genomes:
        count += 1
        res = es.index(index=genomes_index, body=genome)
        print(res['result'] + ": " + str(count))

def get_all_genomes():
    es.indices.refresh(index=genomes_index)
    
    res = es.search(index=genomes_index, body={"query": {"match_all": {}}, "size": 3200})
    
    print("Got %d Hits:" % res['hits']['total']['value'])
    for hit in res['hits']['hits']:
        print(hit["_source"])

def parse_genomes():
    genomes = list()

    folder = "D:\\Pituf0k\\Desktop\\Tesis\\contigs"

    """
    files = list()
    files.append("SRR1146862_contigs.gbk")
    files.append("SRR1150316_contigs.gbk")
    for filename in files:
    """

    files_count = 0
    for filename in os.listdir(folder):
        files_count += 1
        print("************************************** NUEVO GENOMA **********************************")
        print("Archivo: " + filename)
        file = open(folder+"\\"+filename, 'r')
        srr = {
            "name":filename[0:-12]
        }
        genes = list()
        Lines = file.readlines()
        count = 0
        while count < len(Lines):
            if 'CDS' in Lines[count]:
                gene = ''
                product = ''
                locus_tag = ''
                translation = ''
                gene_parsed = False
                count += 1
                while count < len(Lines) and not gene_parsed: #'CDS' not in Lines[count]:
                    stripped = ''
                    if '=' in Lines[count]:
                        stripped = Lines[count].strip().strip('\n').split("=")[1].strip('"')
                    gene = stripped if "/gene=" in Lines[count] else gene
                    product = stripped if "/product=" in Lines[count] else product
                    locus_tag = stripped if "/locus_tag=" in Lines[count] else locus_tag
                    if "/translation=" in Lines[count]:
                        translation = stripped
                        count+= 1
                        while (('"' not in Lines[count]) and ('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                            translation+=Lines[count].strip().strip('\n')
                            count+= 1
                        if (('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                            translation+=Lines[count].strip().strip('\n').strip('"')
                        gene_parsed = True
                    count+= 1
                gen = {
                    "gene":gene,
                    "product":product,
                    "locus_tag":locus_tag,
                    "translation":translation
                }
                genes.append(gen)
            count+=1
        srr['genes'] = genes
        add_single_genome(srr, files_count)

        #genomes.append(srr)

    #print("GENOMAS: " + str(len(genomes)))
    #add_genomes(genomes)

def update_genomes_inv():
    file = open("sr_invasion.txt", 'r')
    lines = file.readlines()
    genomes = dict()
    for line in lines:
        columns = line.split(" ")
        row = dict()
        row["MGAS"] = columns[1].strip()
        row["INV"] = columns[2].strip()
        genomes[columns[0].strip()] = row
    """
    {
        'SRR': {
            'MGAS': ...,
            'INV': ...
        },
        'SRR': {...} 
    }
    """

    total = 0
    for key, value in genomes.items():
        res = es.search(index=genomes_index, body= {"query": {"match": { "name": key}}})
        total += res["hits"]["total"]["value"]

        if res["hits"]["total"]["value"] == 1:
            u_response = es.update(index=genomes_index,doc_type='_doc',id=res["hits"]["hits"][0]["_id"],
                    body={"doc": {"MGAS": value["MGAS"], "INV": value["INV"] }})
            print(u_response)

    print("total: " + str(total))

#parse_genomes()
#update_genomes_inv()
get_all_genomes()
