from django.conf import settings
import subprocess
from . import elastic_functions
import os

def get_multifasta_muscle_tree_path(project_folder, study_id):
    return settings.GENOMES_PATH+'/'+project_folder+"/studies/"+study_id+"/multifasta_muscle_tree"

def create_multifasta(project_folder, study_id, project_name, study_name, gene):
    content = ''

    for index, t in enumerate(gene['translation']):
        content = content + '>' +  project_name + '/' + study_name + '/' + gene['product'] + '/' + str(gene['id']) + '/' + str(index) + '\n' + t + '\n'

    product = ''.join(e for e in gene['product'] if e.isalnum())
    filename = project_name + '_' + study_name + '_' + product + '_' + str(gene['id']) + '.fasta'
    filename = filename.replace(" ", "_")

    folder_url = get_multifasta_muscle_tree_path(project_folder, study_id)
    file_url = folder_url+"/"+filename
    file = open(file_url, 'w')
    file.write(content)
    file.close()

    return file_url, filename

def create_multiple_sequence_alignment(project_folder, study_id, multifasta_url, multifasta_filename):
    
    folder_url = get_multifasta_muscle_tree_path(project_folder, study_id)
    muscle_filename = multifasta_filename[:-6]+".aln"
    muscle_file_url = folder_url+"/"+muscle_filename

    bash_muscle = '/prokka/bin/muscle -align '+multifasta_url+' -output '+muscle_file_url
    subprocess.run(bash_muscle, shell=True)

    bash_delete_multifasta = 'rm '+multifasta_url
    subprocess.run(bash_delete_multifasta, shell=True)

    return muscle_file_url

def create_multiple_sequence_alignment_tree(project_folder, study_id, muscle_file_url):
    
    folder_url = get_multifasta_muscle_tree_path(project_folder, study_id)

    bash_iqtree = 'iqtree -s '+muscle_file_url+' -nt 4 -m TEST -bb 2000 -nm 5' #-nm for testing, limit number of iterations
    subprocess.run(bash_iqtree, shell=True)

    tmp_files_to_delete = ['.bionj', '.ckp.gz', '.contree', '.iqtree', '.log', '.mldist', '.model.gz', '.splits.nex', '.ckp.gz.tmp']
    for tmp_file in tmp_files_to_delete:
        bash_delete_tmp = 'rm '+folder_url+'/*'+tmp_file
        subprocess.run(bash_delete_tmp, shell=True)


def do_multiple_sequence_alignment(project_folder, project_id, studies, study_id, project_name, study_name, gene, generate_tree):
    
    multifasta_url, multifasta_filename = create_multifasta(project_folder, study_id, project_name, study_name, gene)

    muscle_url = create_multiple_sequence_alignment(project_folder, study_id, multifasta_url, multifasta_filename)

    if generate_tree:
        create_multiple_sequence_alignment_tree(project_folder, study_id, muscle_url)

    #CHANGE STATE TO READY_FOR_QUESTIONS
    elastic_functions.do_set_study_state(project_id, study_id, studies, 'READY_FOR_QUESTIONS')

def get_file(project_folder, study_id, filename):

    folder_url = get_multifasta_muscle_tree_path(project_folder, study_id)
    file_url = folder_url+"/"+filename
    file = open(file_url, 'r')
    content = file.read()
    file.close()

    bash_delete_file = 'rm '+file_url
    subprocess.run(bash_delete_file, shell=True)

    return content

def get_files_list(project_folder, study_id):
    folder_url = get_multifasta_muscle_tree_path(project_folder, study_id)

    files = []
    if os.path.isdir(folder_url):
        for name in os.listdir(folder_url):
            if (
                os.path.isfile(folder_url +'/'+ name) and len(name) > 2 
                and (name.split('.')[-1] == 'aln' or name.split('.')[-1] == 'treefile')
            ):
                files.append(name)
    return files
