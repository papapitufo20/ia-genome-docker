from os import sched_get_priority_min
from rest_framework.decorators import permission_classes
from rest_framework import permissions,status
from rest_framework.decorators import api_view
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from . import elastic_functions, file_processing_functions, ia_functions, multifasta_muscle_tree_functions
import re
from .utils import STUDY_STATES, ThreadWithReturnValue, DEFAULT_EPOCHS, DEFAULT_LEARNING_RATE, CPUS, DEFAULT_RNA_ARCH, DEFAULT_PERCENTAGE_OF_TRAINING_DATA
import csv
import traceback
import xlsxwriter

studiesType = ["Genes", "Alleles"]

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def create_study(request, project_id):
    try:
        name = request.data['name'].strip()
        name = re.sub('[^A-Za-z0-9 ]+', '', name)
        description = request.data['description'].strip()
        studyType = request.data['studyType'].strip()
        if (len(name) >= 6 and len(name) <= 200 and len(description) <= 400 and studyType in studiesType):

            study_id = "_".join(name.split(" "))
            do_res = elastic_functions.do_create_study(project_id, study_id, name, description,studyType)
            
            if (do_res == 'updated'):
                project = elastic_functions.do_get_project(project_id)
                file_processing_functions.create_study_folders(project['_source']['folder'], study_id)
                res = JsonResponse({'detail': 'Estudio creado'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': do_res}, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = JsonResponse({'detail': 'Revise los valores ingresados'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def force_state_change_study(request,project_id,study_id):
    res_study = elastic_functions.do_get_study_from_project(study_id, project_id)
    if (res_study is not None):
        state = res_study['state']
        if (state == 'LOADING_LABELS'):
            elastic_functions.do_set_study_state(project_id,study_id,None,"INITIAL")
        else:
            if (state == 'TRAINING'):
                elastic_functions.do_set_study_state(project_id,study_id,None,"READY_FOR_TRAINING")
            else:
                if (state == 'GENERATING_IQTREE' or state == 'ALIGNING_MULTIFASTA' or state == 'ANALYZING_MODEL' or state == 'EVALUATING'):
                    elastic_functions.do_set_study_state(project_id,study_id,None,"READY_FOR_QUESTIONS")
        res = JsonResponse({'detail': 'Estado Cambiado'}, status=status.HTTP_200_OK)
    else:
        res = JsonResponse({'detail': 'Estudio no encontrado'}, status=status.HTTP_400_BAD_REQUEST)
    return res
    

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def edit_study(request, project_id, study_id):
    try:
        #name = request.data['name'].strip()
        #name = re.sub('[^A-Za-z0-9 ]+', '', name)
        description = request.data['description'].strip()
        if (len(description) <= 400):
            
            do_res = elastic_functions.do_edit_study(project_id, study_id, description)
            
            if (do_res == 'updated' or do_res == 'noop'):
                res = JsonResponse({'detail': 'Estudio editado'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': do_res}, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = JsonResponse({'detail': 'Revise los valores ingresados'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def delete_study(request, project_id):
    try:
        study_id = request.data['id']
        
        do_res = elastic_functions.do_delete_study(project_id, study_id)
        
        if (do_res == 'updated'):
            project = elastic_functions.do_get_project(project_id)
            file_processing_functions.remove_study(project['_source']['folder'], study_id)
            res = JsonResponse({'detail': 'Estudio eliminado'}, status=status.HTTP_200_OK)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_study_states(request):
    return JsonResponse(STUDY_STATES, status=status.HTTP_200_OK, safe=False)

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_study(request, project_id, study_id):
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            response = dict()
            response['project'] = do_res['_source']
            response['project']['id'] = do_res['_id']
            study = list(filter(lambda s: s['id'] == study_id, response['project']['studies']))
            if len(study) == 1:
                study = study[0]
                del response['project']['studies']
                response['study'] = dict()
                for key, value in study.items():
                    response['study'][key] = value
                response['study']['results'] = file_processing_functions.get_results_from_study(do_res['_source']['folder'], study_id)
                response['study']["genomes_count"] = elastic_functions.get_genomes_label_information(do_res["_source"]["genomes_index"], study_id)
                response['study']["genomes_to_evaluate"] =  file_processing_functions.remaining_to_evaluate(settings.GENOMES_PATH+'/'+do_res['_source']['folder']+'/studies/'+study['name']+'/genomes_to_evaluate')
                response['study']['training_logs'] = ia_functions.get_training_results_logs(do_res['_source']['folder'], study_id)
                response['study']['muscle_iqtree_files'] = multifasta_muscle_tree_functions.get_files_list(do_res['_source']['folder'], study_id)
                res = JsonResponse({'result': response}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def analyze_model(request, project_id, study_id):
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            studies = do_res['_source']['studies']
            study = list(filter(lambda s: s['id'] == study_id, studies))
            if len(study) == 1:

                #CHANGE STATE TO ANALYZING_MODEL
                up_res = elastic_functions.do_set_study_state(project_id, study_id, studies, "ANALYZING_MODEL")
                
                if up_res == 'updated':
                    #ROOT THREAD SPAWNED - ASYNC PROCESS
                    t = ThreadWithReturnValue(
                        target=ia_functions.analyze_model, 
                        args=(project_id, do_res['_source']['folder'], study_id, studies)
                    )
                    t.start()
                    res = JsonResponse({'detail': 'Analizando'}, status=status.HTTP_200_OK)
                else:
                    res = 'No se pudo actualizar el estado del estudio'

                return res
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_model_analysis(request, project_id, study_id):
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            study = list(filter(lambda s: s['id'] == study_id, do_res['_source']['studies']))
            if len(study) == 1:
                svg_file = ia_functions.get_model_plots_analysis(do_res['_source']['folder'], study_id)
                res = JsonResponse(svg_file, content_type="image/svg+xml", status=status.HTTP_200_OK, safe=False)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def get_genes_info(request, project_id, study_id):
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            project = do_res['_source']
            study = list(filter(lambda s: s['id'] == study_id, project['studies']))
            if len(study) == 1:
                genes_id = request.data['genes_id']
                if len(genes_id) > 0:
                    genes = elastic_functions.get_genes_info(project, study[0], genes_id)
                    res = JsonResponse({'genes_info': genes}, status=status.HTTP_200_OK)
                else:
                    res = JsonResponse({'detail': 'No hay ids de genes'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def download_unlabeled_genomes(request, project_id, study_id):
    
    genomes = elastic_functions.get_genomes_name_unlabeled(project_id, study_id)
    if genomes is not None:
        #if len(genomes) > 0:
        res = HttpResponse(
            content_type='text/csv'
        )

        res['Content-Disposition'] = 'attachment; filename="'+project_id+'_'+study_id+'.csv"'

        writer = csv.writer(res)

        writer.writerow(['Genoma', 'Etiqueta'])
        
        for genome in genomes:
            writer.writerow([genome, ''])
        #else:
        #    res = JsonResponse({'detail': 'No hay genomas sin etiqueta para este estudio'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        res = JsonResponse({'detail': 'No se pudo exportar CSV'}, status=status.HTTP_400_BAD_REQUEST)

    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def set_label_to_genomes(request, project_id, study_id):
    try:
        genome_label_pairs = request.data['genome_label_pair']
        
        do_res = elastic_functions.do_set_project_genome_label_for_study(project_id, study_id, genome_label_pairs)

        if do_res == 'updating':
            res = JsonResponse({'result': 'Procesando etiquetas'}, status=status.HTTP_200_OK)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)

    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST) 
    
    return res

def train_RNA(request, project_id, study_id, callback, message, optimize):
    try:
        cpus = CPUS
        epochs = DEFAULT_EPOCHS
        learning_rate = DEFAULT_LEARNING_RATE
        rna_arch = DEFAULT_RNA_ARCH
        percentage_of_training_data = DEFAULT_PERCENTAGE_OF_TRAINING_DATA

        try:
            if 'cpus' in request.data:
                cpus_from_request = int(request.data['cpus'])
                cpus = cpus_from_request if (cpus_from_request > 0 and cpus_from_request <= cpus) else cpus 
        except Exception as e:
            traceback.print_exc()
            pass
        
        try:
            if 'epochs' in request.data:
                epochs_from_request = int(request.data['epochs'])
                epochs = epochs_from_request if (epochs_from_request > 0 and epochs_from_request <= 5000) else epochs 
        except Exception as e:
            traceback.print_exc()
            pass

        try:
            if 'learning_rate' in request.data:
                learning_rate_from_request = float(request.data['learning_rate'])
                learning_rate = learning_rate_from_request if (learning_rate_from_request > 0 and learning_rate_from_request <= 1) else learning_rate 
        except Exception as e:
            traceback.print_exc()
            pass

        try:
            if 'rna_arch' in request.data:
                rna_arch_from_request = request.data['rna_arch']
                rna_arch = parse_rna(rna_arch_from_request) if valid_rna(rna_arch_from_request) else rna_arch 
        except Exception as e:
            traceback.print_exc()
            pass

        try:
            if 'percentage_of_training_data' in request.data:
                percentage_of_training_data_from_request = int(request.data['percentage_of_training_data'])
                percentage_of_training_data = percentage_of_training_data_from_request if (percentage_of_training_data_from_request >= 50 and percentage_of_training_data_from_request <= 100) else percentage_of_training_data 
        except Exception as e:
            traceback.print_exc()
            pass
        
        do_res = callback(project_id, study_id, epochs, learning_rate, cpus, rna_arch, optimize, percentage_of_training_data)

        if do_res == 'training':
            res = JsonResponse({'result': message}, status=status.HTTP_200_OK)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)

    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST) 
    
    return res

def parse_rna(rna_arch):
    result = {}
    for index, layer_number in enumerate(rna_arch):
        result[index+1] = layer_number
    return result

def valid_rna(rna_arch):
    if len(rna_arch) > 0:
        for layer_number in rna_arch:
            if not isinstance(layer_number, int):
                return False
    else:
        return False

    return True

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def train(request, project_id, study_id):
    return train_RNA(request, project_id, study_id, elastic_functions.train_RNA, "Entrenando modelo", False)

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def optimize(request, project_id, study_id):
    return train_RNA(request, project_id, study_id, elastic_functions.train_RNA, "Optimizando modelo", True)

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def evaluate(request, project_id, study_id):
    try:
        project = elastic_functions.do_get_project(project_id)
        study = list(filter(lambda s: s['id'] == study_id, project['_source']['studies']))
        if (study[0]['state'] == 'READY_FOR_QUESTIONS'):
            cpus = CPUS
            try:
                if 'cpus' in request.data:
                    cpus = request.data['cpus']
            except Exception as e:
                traceback.print_exc()
                pass     
            kingdom = project['_source']['kingdom']
            folder = project['_source']['folder']
            #ROOT THREAD SPAWNED - ASYNC PROCESS
            t = ThreadWithReturnValue(
                target=file_processing_functions.process_genomes_to_evaluate_folder, 
                args=(study_id, folder, cpus, "READY_FOR_QUESTIONS", kingdom, project_id,project['_source']['studies'])
            )
            t.start()
            res = JsonResponse({'detail': 'Procesando'}, status=status.HTTP_200_OK)
        else:
            res = JsonResponse({'detail': 'El estudio no se encuentra listo para evaluar'}, status=status.HTTP_400_BAD_REQUEST) 
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)  
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def multiple_sequence_alignment(request, project_id, study_id):
    
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            studies = do_res['_source']['studies']
            study = list(filter(lambda s: s['id'] == study_id, studies))
            if len(study) == 1:

                if (study[0]['state'] == 'READY_FOR_QUESTIONS'):

                    project_folder = do_res['_source']['folder']
                    project_name = request.data['projectName']
                    study_name = request.data['studyName']
                    gene = request.data['gene']
                    generate_tree = request.data['generateTree']

                    if not generate_tree:
                        new_state = "ALIGNING_MULTIFASTA"
                    else:
                        new_state = "GENERATING_IQTREE"

                    #CHANGE STATE TO ALIGNING_MULTIFASTA OR GENERATING_IQTREE
                    up_res = elastic_functions.do_set_study_state(project_id, study_id, studies, new_state)

                    if up_res == 'updated':
                        #ROOT THREAD SPAWNED - ASYNC PROCESS
                        t = ThreadWithReturnValue(
                            target=multifasta_muscle_tree_functions.do_multiple_sequence_alignment, 
                            args=(project_folder, project_id, studies, study_id, project_name, study_name, gene, generate_tree)
                        )
                        t.start()

                        msg = 'Alineando multifasta' if not generate_tree else 'Generando IQTREE'
                        res = JsonResponse({'detail': msg}, status=status.HTTP_200_OK)
                    else:
                        res = JsonResponse({'detail': 'No se pudo actualizar el estado del estudio'}, status=status.HTTP_400_BAD_REQUEST) 
                else:
                    res = JsonResponse({'detail': 'El estado del estudio no permite esta accion'}, status=status.HTTP_404_NOT_FOUND)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def download_multiple_sequence_alignment(request, project_id, study_id):
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            studies = do_res['_source']['studies']
            study = list(filter(lambda s: s['id'] == study_id, studies))
            if len(study) == 1:

                if (study[0]['state'] == 'READY_FOR_QUESTIONS'):

                    project_folder = do_res['_source']['folder']
                    filename = request.data['filename']

                    content = multifasta_muscle_tree_functions.get_file(project_folder, study_id, filename)

                    res = HttpResponse(content=content,content_type='text/plain')

                    res['Content-Disposition'] = 'attachment; filename="'+filename
                else:
                    res = JsonResponse({'detail': 'El estado del estudio no permite esta accion'}, status=status.HTTP_404_NOT_FOUND)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def download_gen_info_xlsx(request, project_id, study_id):
    
    try:
        do_res = elastic_functions.do_get_project(project_id)
        if (do_res['found']):
            studies = do_res['_source']['studies']
            study = list(filter(lambda s: s['id'] == study_id, studies))
            if len(study) == 1:

                if (study[0]['state'] == 'READY_FOR_QUESTIONS'):

                    genomes_index = do_res['_source']['genomes_index']
                    genes_index = do_res['_source']['genes_index']
                    alleles_index = do_res['_source']['alleles_index']
                    gene = request.data['gene']

                    data_sheet1, data_sheet2 = elastic_functions.get_gen_info_for_xlsx(study_id, genomes_index, genes_index, alleles_index, gene)

                    if data_sheet1 is not None and data_sheet2 is not None:
                        
                        res = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        )

                        res['Content-Disposition'] = 'attachment; filename="'+project_id+'_'+study_id+'.xlsx"'

                        workbook = xlsxwriter.Workbook(res)

                        sheet1 = workbook.add_worksheet()
                        sheet2 = workbook.add_worksheet()

                        sheet1.write_row(0, 0, ['Translation', 'Presente en', 'Etiquetados con si', 'Etiquetados con no'])

                        for row_number, row_data in enumerate(data_sheet1):
                            sheet1.write_row(row_number+1, 0, row_data)

                        sheet2.write_row(0, 0, ['Genoma', 'Etiqueta', 'Translation'])
                        
                        for row_number, row_data in enumerate(data_sheet2):
                            sheet2.write_row(row_number+1, 0, row_data)

                        workbook.close()
                        
                    else:
                        res = JsonResponse({'detail': 'No se pudo exportar CSV'}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    res = JsonResponse({'detail': 'El estado del estudio no permite esta accion'}, status=status.HTTP_404_NOT_FOUND)
            else:
                res = JsonResponse({'detail': 'El estudio no se encuentra'}, status=status.HTTP_404_NOT_FOUND)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res
