from types import resolve_bases
from django.conf import settings
import os
import subprocess
from .utils import ThreadWithReturnValue
from . import elastic_functions
from . import ia_functions
import csv
import traceback
import pytz
from datetime import datetime

def process_fastq(project_url, file):
    filename = os.path.splitext(os.path.splitext(file)[0])[0]
    if (filename[-2:]=="_1"):
        if os.path.isfile(project_url+'/fastq/'+filename[:-2]+'_2.fastq.gz') or os.path.isfile(project_url+'/fastq/'+filename[:-2]+'_2.FASTQ.GZ'):
            if os.path.isfile(project_url+'/fastq/'+filename[:-2]+'_2.fastq.gz'):
                paired_file = project_url+'/fastq/'+filename[:-2]+'_2.fastq.gz'
            else:
                paired_file = project_url+'/fastq/'+filename[:-2]+'_2.FASTQ.GZ'
            bash_spades = 'spades.py -1 '+project_url+'/fastq/'+file+' -2 '+paired_file+' -t 1 -k 21,33,55,77,99,127 -o '+project_url+'/fastq/tmp/'+filename[:-2]
            bash_move = 'mv '+project_url+'/fastq/tmp/'+filename[:-2]+'/contigs.fasta '+project_url+'/fasta/'+filename[:-2]+'.fasta'
            bash_delete_tmp_files = 'rm -R '+project_url+'/fastq/tmp/'+filename[:-2]
            bash_delete_processed_files = 'rm '+project_url+'/fastq/'+file+' & rm '+paired_file
        else:
            print("missing paired file of "+filename)
    else:
        bash_spades = 'spades.py -s '+project_url+'/fastq/'+file+' -t 1 -k 21,33,55,77,99,127 -o '+project_url+'/fastq/tmp/'+filename
        bash_move = 'mv '+project_url+'/fastq/tmp/'+filename+'/contigs.fasta '+project_url+'/fasta/'+os.path.splitext(os.path.splitext(file)[0])[0]+'.fasta'
        bash_delete_tmp_files = 'rm -R '+project_url+'/fastq/tmp/'+filename
        bash_delete_processed_files = 'rm '+project_url+'/fastq/'+file
    
    with open(project_url+'/logs/fastq_to_fasta/'+filename[:-2]+'.txt', 'w') as file_output:
        subprocess.run(bash_spades, shell=True, stdout=file_output, stderr=file_output)

        move_output = subprocess.run(bash_move, shell=True)
        if move_output.returncode == 0:
            subprocess.run(bash_delete_processed_files, shell=True)

        subprocess.run(bash_delete_tmp_files, shell=True)

def process_fasta(project_url, file, kingdom):
    filename = os.path.splitext(os.path.splitext(file)[0])[0]
    bash_prokka = 'prokka --prefix '+filename+' --outdir '+project_url+'/prokka/'+filename+' --kingdom '+kingdom+' --addgenes --strain '+filename+' --mincontiglen 300 --cpus 1 '+project_url+'/fasta/'+file+ " --force"
    bash_move = 'mv '+project_url+'/prokka/'+filename+'/'+filename+'.gbf '+project_url+'/gbf/'
    bash_delete_tmp_files = 'rm -R '+project_url+'/prokka/'+filename
    
    with open(project_url+'/logs/fasta_to_gbf/'+filename+'.txt', 'w') as file_output:

        subprocess.run(bash_prokka, shell=True, stdout=file_output, stderr=file_output)

        move_output = subprocess.run(bash_move, shell=True)
        if move_output.returncode == 0:
            bash_delete_processed_file = 'rm '+project_url+'/fasta/'+file
            subprocess.run(bash_delete_processed_file, shell=True)
        subprocess.run(bash_delete_tmp_files, shell=True)

def process_gbf(project_url, filename, id_project, existing_genomes):
    with open(project_url+'/logs/gbf_parse/'+filename+'.txt', 'w') as file_output:
        file_output.write("Started parsing of: " + filename + "\n")
        file = open(project_url+'/gbf/'+filename)
        srr = {
            "name":filename[0:-4],
            "processed":False
        }
        genes = list()
        Lines = file.readlines()
        count = 0
        while count < len(Lines):
            if 'CDS' in Lines[count]:
                gene = ''
                product = ''
                locus_tag = ''
                translation = ''
                gene_parsed = False
                count += 1
                while count < len(Lines) and not gene_parsed:
                    stripped = ''
                    if '=' in Lines[count]:
                        stripped = Lines[count].strip().strip('\n').split("=")[1].strip('"')
                    gene = stripped if "/gene=" in Lines[count] else gene
                    product = stripped if "/product=" in Lines[count] else product
                    locus_tag = stripped if "/locus_tag=" in Lines[count] else locus_tag
                    if "/translation=" in Lines[count]:
                        translation = stripped
                        count+= 1
                        while (('"' not in Lines[count]) and ('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                            translation+=Lines[count].strip().strip('\n')
                            count+= 1
                        if (('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                            translation+=Lines[count].strip().strip('\n').strip('"')
                        gene_parsed = True
                    count+= 1
                gen = {
                    "gene":gene,
                    "product":product,
                    "locus_tag":locus_tag,
                    "translation":translation
                }
                genes.append(gen)
            count+=1
        srr['genes'] = genes
        
        result = None
        if srr['name'] not in existing_genomes:
            #result = elastic_functions.add_single_genome_to_project(srr, id_project)
            return (id_project,srr)
        else:
            result = elastic_functions.update_single_genome_on_project(srr, id_project)
            
        if result == 'created' or result == 'updated' or result == 'noop':
            if result == 'created':
                file_output.write("Inserted genome: " + filename + " with " + str(len(genes)) + " genes.\n")
            else:
                file_output.write("Updated genome: " + filename + " with " + str(len(genes)) + " genes.\n")


def add_pertenence_matrix_to_genomes_from_project(id_project, project_url):
    genomes_to_process = elastic_functions.do_get_project_non_processed_genomes_names(id_project)
    existing_genes = elastic_functions.do_get_project_genes_with_id(id_project)
    existing_alleles = elastic_functions.do_get_project_alleles_with_id(id_project)
    project = elastic_functions.do_get_project(id_project)
    genomes_pertenences=[]
    bash_deletes=[]
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        test = 0
        for hit in genomes_to_process['hits']['hits']:

            genome = elastic_functions.do_get_project_genome(genomes_index,hit["_source"]["name"])
            genome = genome["hits"]["hits"][0] if 'hits' in genome["hits"] and len(genome["hits"]["hits"]) == 1 else None
            if genome is not None:
                test = test + 1
                pertenencia_genes = list()
                pertenencia_alleles = list()
                for gen in genome['_source']['genes']:
                    pertenencia_genes.append(existing_genes[gen["translation"]])
                    if (gen["gene"] == ''):
                        pertenencia_alleles.append(existing_alleles[gen["translation"]])
                    else:
                        pertenencia_alleles.append(existing_alleles[gen["gene"]])
                #elastic_functions.update_genes_pertenences_and_set_processed_to_genome(genomes_index,genome['_id'], pertenencia_genes, pertenencia_alleles)
                genomes_pertenences.append({'_op_type': 'update','_index':genomes_index,"_id": genome['_id'],"doc": {"gen_pertenence": pertenencia_genes,"alleles_pertenence": pertenencia_alleles,"processed": True}, "doc_as_upsert":True})
                filename = genome["_source"]["name"]+".gbf"
                #bash_delete_processed_file = 'rm '+project_url+'/gbf/'+filename
                bash_deletes.append('rm '+project_url+'/gbf/'+filename)
                #subprocess.run(bash_delete_processed_file, shell=True)
        elastic_functions.do_bulk_updates(genomes_pertenences)
        for bash_delete in bash_deletes:
            subprocess.run(bash_delete, shell=True)

def insert_unique_genes(project_id):
    genomes_to_process = elastic_functions.do_get_project_non_processed_genomes_names(project_id)
    existing_genes = elastic_functions.do_get_project_genes_names_index(project_id)
    existing_alleles = elastic_functions.do_get_project_alleles_names_index(project_id)
    new_genes = dict()
    new_alleles = dict()
    count = len(existing_genes)
    count_alleles = len(existing_alleles)
    
    project = elastic_functions.do_get_project(project_id)
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        for hit in genomes_to_process['hits']['hits']:
            genome = elastic_functions.do_get_project_genome(genomes_index,hit["_source"]["name"])
            genome = genome["hits"]["hits"][0] if 'hits' in genome["hits"] and len(genome["hits"]["hits"]) == 1 else None
            if genome is not None:
                for gen in genome["_source"]["genes"]:
                    #genes index
                    if gen["translation"] not in new_genes and gen["translation"] not in existing_genes:
                        count = count + 1
                        new_genes[gen["translation"]] = dict()
                        new_genes[gen["translation"]]["gene"] = gen["gene"]
                        new_genes[gen["translation"]]["product"] = gen["product"]
                        new_genes[gen["translation"]]["translation"] = gen["translation"]
                        new_genes[gen["translation"]]["id"] = count
                    #alleles index
                    #hypothetical protein
                    if gen["gene"]=='':
                        if gen["translation"] not in new_alleles and gen["translation"] not in existing_alleles:
                            count_alleles = count_alleles + 1
                            new_alleles[gen["translation"]] = dict()
                            new_alleles[gen["translation"]]["gene"] = gen["translation"]
                            new_alleles[gen["translation"]]["product"] = gen["product"]
                            new_alleles[gen["translation"]]["translation"] = [gen["translation"]]
                            new_alleles[gen["translation"]]["id"] = count_alleles
                    #known gene
                    elif gen["gene"] not in new_alleles and gen["gene"] not in existing_alleles:
                        count_alleles = count_alleles + 1
                        new_alleles[gen["gene"]] = dict()
                        new_alleles[gen["gene"]]["gene"] = gen["gene"]
                        new_alleles[gen["gene"]]["product"] = gen["product"]
                        new_alleles[gen["gene"]]["translation"] = [gen["translation"]]
                        new_alleles[gen["gene"]]["id"] = count_alleles
                    elif gen["gene"] in new_alleles:
                        #append translation to element in new_alleles
                        if gen["translation"] not in new_alleles[gen["gene"]]["translation"]:
                            new_alleles[gen["gene"]]["translation"].append(gen["translation"])
                    elif gen["translation"] not in existing_alleles[gen["gene"]]["translation"]:
                        #append translation to element in existing_alleles
                        existing_alleles[gen["gene"]]["translation"].append(gen["translation"])
                        elastic_functions.update_alleles_translations_on_gen_from_project(existing_alleles[gen["gene"]]["translation"], existing_alleles[gen["gene"]]["_id"], project_id)
        elastic_functions.do_add_genes(project_id,new_genes)
        elastic_functions.do_add_alleles(project_id,new_alleles)
        print('termino insert_unique_genes a ' + datetime.now().strftime("%H:%M:%S"))

def process_genome_folder_fastq(id, folder, cpus, previous_state, kingdom, path_from, path_to):
    move_file_type_from_to('gz', path_from, path_to)
    do_process_parallel(id, folder, cpus, previous_state, 'gz', 'fastq', process_fastq, ())

def process_genome_folder_fasta(id, folder, cpus, previous_state, kingdom, path_from, path_to):
    move_file_type_from_to('fasta', path_from, path_to)
    do_process_parallel(id, folder, cpus, previous_state, 'fasta', 'fasta', process_fasta, (kingdom,))

def process_genome_folder_gbf(id, folder, cpus, previous_state, kingdom, path_from, path_to):
    move_file_type_from_to('gbf', path_from, path_to)
    existing_genomes = elastic_functions.do_get_project_genomes_names(id)
    do_process_parallel(id, folder, 1, None, 'gbf', 'gbf', process_gbf, (id,existing_genomes))
    try:
        elastic_functions.refresh_indices()
        insert_unique_genes(id)
        elastic_functions.refresh_indices()
        add_pertenence_matrix_to_genomes_from_project(id, settings.GENOMES_PATH+'/'+folder)
        elastic_functions.do_update_project_state(id, "READY_FOR_STUDIES")
    except Exception as e:
        elastic_functions.do_update_project_state(id, previous_state)
        traceback.print_exc()

def get_results_from_study(project_folder, study_id):
    filenames = None
    try:
        filenames = os.listdir(settings.GENOMES_PATH+'/'+project_folder+'/studies/'+study_id+'/results')
    except Exception as e:
        traceback.print_exc()
    return filenames
    
    

def evaluate_gbf_folder(study_id, project_id, project, study, folder):
    #total_genes = elastic_functions.do_get_project_genes_index(project_id)
    study_type = study['studyType']
    genomes = elastic_functions.get_genomes_labeled(project_id, study['id'])
    if study_type == "Genes":
        existing_genes = elastic_functions.do_get_project_genes_with_id(project_id)
    else:
        existing_alleles = elastic_functions.do_get_project_alleles_with_id(project_id)
    processed_srrs = list()
    for filename in os.listdir(folder):
        file = open(folder+'/'+filename)
        extension = os.path.splitext(filename)[1][1:].casefold()
        if (extension == 'gbf'):
            srr = {
                "name":filename[0:-4],
            }
            genes = list()
            Lines = file.readlines()
            count = 0
            while count < len(Lines):
                if 'CDS' in Lines[count]:
                    translation = ''
                    gene = ''
                    gene_parsed = False
                    count += 1
                    while count < len(Lines) and not gene_parsed:
                        stripped = ''
                        if '=' in Lines[count]:
                            stripped = Lines[count].strip().strip('\n').split("=")[1].strip('"')
                        gene = stripped if "/gene=" in Lines[count] else gene
                        if "/translation=" in Lines[count]:
                            translation = stripped
                            count+= 1
                            while (('"' not in Lines[count]) and ('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                                translation+=Lines[count].strip().strip('\n')
                                count+= 1
                            if (('ORIGIN' not in Lines[count]) and ('gene' not in Lines[count])):
                                translation+=Lines[count].strip().strip('\n').strip('"')
                            gene_parsed = True
                        count+= 1
                    gen = {
                        "translation":translation,
                        "gene":gene
                    }
                    genes.append(gen)
                count+=1
        pertenence = list()
        if study_type == "Genes":
            for gen in genes:
                if gen["translation"] in existing_genes:
                    pertenence.append(existing_genes[gen["translation"]])
        else:
            for gen in genes:
                if gen["gene"] == '':
                    if gen["translation"] in existing_alleles:
                        pertenence.append(existing_alleles[gen["translation"]])
                else:
                    if gen["gene"] in existing_alleles:
                        pertenence.append(existing_alleles[gen["gene"]])
        if study_type == "Genes":
            srr['gen_pertenence']= pertenence
        else:
            srr['alleles_pertenence']= pertenence
        processed_srrs.append(srr)
    return ia_functions.predict(project_id, processed_srrs, study, project)
    
def process_genomes_to_evaluate_folder(study_id, project, cpus, previous_state, kingdom, project_id, studies):
    elastic_functions.do_set_study_state(project_id, study_id, studies, "EVALUATING")
    genomes_to_evaluate_folder = settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate/tmp'
    fastq_from = settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate'
    fastq_to = genomes_to_evaluate_folder+'/fastq'
    fasta_from = settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate'
    fasta_to = genomes_to_evaluate_folder+'/fasta'
    #move_file_type_from_to('gz', settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate', genomes_to_evaluate_folder+'/fastq')
    #move_file_type_from_to('fasta', settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate', genomes_to_evaluate_folder+'/fasta')
    try:
        #if (remaining_fastq(genomes_to_evaluate_folder+'/fastq') > 0):
        process_genome_folder_fastq(project_id, project+'/studies/'+study_id+'/genomes_to_evaluate/tmp', cpus, None, kingdom, fastq_from, fastq_to)
        
        #if (remaining_fasta(genomes_to_evaluate_folder+'/fasta') > 0):
        process_genome_folder_fasta(project_id, project+'/studies/'+study_id+'/genomes_to_evaluate/tmp', cpus, None, kingdom, fasta_from, fasta_to)
        
        gbf_folder = genomes_to_evaluate_folder+'/gbf'
        #if (remaining_gbf(gbf_folder) > 0):
        study = list(filter(lambda s: s['id'] == study_id, studies))[0]
        move_file_type_from_to('gbf', settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/genomes_to_evaluate', genomes_to_evaluate_folder+'/gbf')
        result = evaluate_gbf_folder(study_id, project_id, project, study, gbf_folder)
        create_csv_file_from_dict_in(['Genome','Prediction','Accuracy'], result, settings.GENOMES_PATH+'/'+project+'/studies/'+study_id+'/results', project, study_id)
        
        elastic_functions.do_set_study_state(project_id, study_id, studies, previous_state)

        for filename in os.listdir(gbf_folder):
            bash_delete_processed_files = 'rm '+gbf_folder+'/'+filename
            subprocess.run(bash_delete_processed_files, shell=True)

    except Exception as e:
        traceback.print_exc()
        elastic_functions.do_set_study_state(project_id, study_id, studies, previous_state)

def create_csv_file_from_dict_in(csv_columns, dict, location, project, study_id):
    
    timezone_bsas = pytz.timezone('America/Buenos_Aires')
    datetime_now = datetime.now(timezone_bsas)
    csv_file = project+"_"+study_id+"_"+datetime_now.strftime("%Y-%m-%d_%H:%M:%Shs")+'.csv'
    try:
        with open(location+'/'+csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict:
                writer.writerow(data)
    except IOError:
        print("I/O error")



def do_process_parallel(id, folder, cpus, previous_state, file_type, files_folder, callback, arguments):
    try:
        threads = []
        project_url = settings.GENOMES_PATH+'/'+folder
        results=[]

        for filename in os.listdir(project_url+'/'+files_folder):
            extension = os.path.splitext(filename)[1][1:].casefold()
            name = filename.split('.')[0]
            if (extension == file_type) and not(name[-2:] == "_2"):                
                #THREAD FOR EACH FASTQ
                t = ThreadWithReturnValue(target=callback, args=(project_url,filename)+arguments)

                # IF NOT AVAILABLE CPU, WAIT
                if not (len(threads) < cpus):
                    for t_spawned in threads:
                        thread_result = t_spawned.join()
                        if thread_result is not None:
                            results.append(thread_result)
                        #CPU FREE, GO
                        threads.remove(t_spawned)
                        break
                
                #THREAD START
                threads.append(t)
                t.start()

        #WAIT ALL THREADS TO FINISH
        for t in threads:
            thread_result = t.join()
            if thread_result is not None:
                results.append(thread_result)
        
        if len(results) != 0:
            elastic_functions.do_add_bulk_genomes(results)
        #ALL THREADS FINISHED
    except Exception as e:
        traceback.print_exc()

    if previous_state is not None:
        update_res = elastic_functions.do_update_project_state(id, previous_state)
        if (update_res == 'updated' or update_res == 'noop'):
            pass
        else:
            pass
            #TODO: MMMMMMMMMM, logueamos o hacemos algo?

def remaining_file_type(folder, filetype):
    res = 0
    if os.path.isdir(folder):
        for name in os.listdir(folder):
            if os.path.isfile(folder +'/'+ name) and len(name) > 1 and name.split('.')[-1] == filetype  and name.split('.')[0][-2:] != ('_2'):
                res += 1
    return res



def remaining_gbf(folder):
    res = 0
    if os.path.isdir(folder):
        for name in os.listdir(folder):
            if os.path.isfile(folder +'/'+ name) and len(name) > 1 and name.split('.')[-1] == 'gbf':
                res += 1
    return res

def remaining_fasta(folder):
    res = 0
    if os.path.isdir(folder):
        for name in os.listdir(folder):
            if os.path.isfile(folder +'/'+ name) and len(name) > 1 and name.split('.')[-1] == 'fasta':
                res += 1
    return res

def remaining_fastq(folder):
    res = 0
    if os.path.isdir(folder):
        for name in os.listdir(folder):
            if os.path.isfile(folder +'/'+ name) and len(name) > 2 and name.split('.')[-1] == 'gz' and name.split('.')[-2] == 'fastq' and name.split('.')[0][-2:] != ('_2'):
                res += 1
    return res

def move_file_type_from_to(fileType, source, destination):
    for filename in os.listdir(source):
        extension = os.path.splitext(filename)[1][1:].casefold()
        if (extension == fileType):
            subprocess.run(('mv '+source+'/'+filename+' '+destination).split())

def remaining_to_evaluate(folder):
    return remaining_gbf(folder+'/tmp/gbf')+remaining_fasta(folder+'/tmp/fasta')+remaining_fastq(folder+'/tmp/fastq')+remaining_gbf(folder)+remaining_fasta(folder)+remaining_fastq(folder)

def remove_project(project):
    subprocess.run(('rm -R '+settings.GENOMES_PATH+'/'+project).split())

def create_project_folders(project):
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project).split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/fasta').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/fastq').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/gbf').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/prokka').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/logs').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/logs/fastq_to_fasta').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/logs/fasta_to_gbf').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/genomes/tmp/logs/gbf_parse').split())
    subprocess.run(('chmod -R 777 '+settings.GENOMES_PATH+'/'+project).split())
    subprocess.run(('chmod -R 777 '+settings.GENOMES_PATH+'/'+project+'/studies/').split())

def create_study_folders(project, study):
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study).split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/keras_model').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/training_logs').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/multifasta_muscle_tree').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/results').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/fasta').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/fastq').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/gbf').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/prokka').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/logs').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/logs/fastq_to_fasta').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/logs/fasta_to_gbf').split())
    subprocess.run(('mkdir '+settings.GENOMES_PATH+'/'+project+'/studies/'+study+'/genomes_to_evaluate/tmp/logs/gbf_parse').split())
    subprocess.run(('chmod -R 777 '+settings.GENOMES_PATH+'/'+project+'/studies/'+study).split())

def remove_study(project, study):
    subprocess.run(('rm -R '+settings.GENOMES_PATH+'/'+project+'/studies/'+study).split())

def get_csv(project_id, study_id, filename):
    project = elastic_functions.do_get_project(project_id)
    with open(settings.GENOMES_PATH+'/'+project['_source']['folder']+'/studies/'+study_id+'/results/'+filename, newline='') as f:
        reader = csv.reader(f)
        data = list(reader)
    return data

def delete_csv(project_id, study_id, filename):
    project = elastic_functions.do_get_project(project_id)  
    subprocess.run(('rm '+settings.GENOMES_PATH+'/'+project['_source']['folder']+'/studies/'+study_id+'/results/'+filename).split())