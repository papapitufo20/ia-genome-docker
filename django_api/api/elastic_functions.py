from django.conf import settings
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from .utils import STATES, STUDY_STATES, DEFAULT_EPOCHS, DEFAULT_RNA_ARCH, DEFAULT_LEARNING_RATE, ThreadWithReturnValue
from . import ia_functions
import traceback
import json

es = Elasticsearch(
    [settings.ES_IP], #'localhost'
    port=settings.ES_PORT,
    http_auth=(settings.ES_USER, settings.ES_PASSWD),
    timeout=300
)

def refresh_indices():
    es.indices.refresh()
    return None

def do_create_project(name,description,kingdom):

    parsed_name = "-".join(name.split(" "))
    
    folder = parsed_name
    project_genomes_index = parsed_name+"-genomes-index"
    project_genes_index = parsed_name+"-genes-index"
    project_alleles_index = parsed_name+"-alleles-index"

    project = {
        "name":name,
        "description":description,
        "kingdom":kingdom,
        "genomes_index": project_genomes_index,
        "genes_index": project_genes_index,
        "alleles_index": project_alleles_index,
        "folder": folder,
        "state": list(filter(lambda s: s["value"] == "INITIAL", STATES))[0]["value"]
    }

    enabled_to_create_project = False

    if es.indices.exists(index=settings.ES_INDEX_NAME):
        
        search_result = es.search(index=settings.ES_INDEX_NAME, body={
            "query": {
                "term": {
                    "name.keyword": {
                        "value": name
                    }
                }
            }
        })

        if (search_result['hits']['total']['value'] == 0):
            enabled_to_create_project = True
        else:
            res = 'Nombre ingresado no disponible'
    else:
        enabled_to_create_project = True
    
    if enabled_to_create_project:
        res = es.index(index=settings.ES_INDEX_NAME,body=project)['result']
        try:
            if es.indices.exists(index=project_genomes_index):
                es.indices.delete(index=project_genomes_index)
            
            if es.indices.exists(index=project_genes_index):
                es.indices.delete(index=project_genes_index)
            
            if es.indices.exists(index=project_alleles_index):
                es.indices.delete(index=project_alleles_index)
            
            config = {
                "mappings": {
                    "properties": {
                        "name": { 
                            "type": "text",
                            "fields": {
                                "raw": { 
                                    "type": "keyword"
                                }
                            }
                        }
                    }
                },
                "settings": {
                    "index.max_result_window": 5000000,
                    "index.max_inner_result_window": 5000000
                }
            }

            es.indices.create(index=project_genomes_index, body=config)
            es.indices.create(index=project_genes_index, body=config)
            es.indices.create(index=project_alleles_index, body=config)
        except Exception as e:
            traceback.print_exc()
            res = "Error al crear indices auxiliares: " + project_genomes_index + " , " + project_genes_index + " y: " + project_alleles_index
    
    return res

def do_edit_project(id,description,kingdom):
    
    project = {
        "doc": {
            #"name":name,
            "description":description,
            "kingdom":kingdom
        }
    }

    try:
        res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=id, body=project)['result']
    except Exception as e:
        res = str(e)
    
    return res

def do_elastic_search(index, query, sort_by, filter_path):
    all_loaded = False
    page = 1
    page_size = 10000
    result = { "hits" : { "total" : {"value" : 0 },"hits" : [] }}
    if get_index_count(index) != 0:
        while not all_loaded:
            from_value = ((page - 1) * page_size)
            
            body = {
                "query": query, 
                "size": page_size,
                "from": from_value
            }

            #TODO: esto en realidad no deberia poderse, ya que sin sort no podemos confiar en el orden del paginado de elastic
            if sort_by is not None:
                body["sort"] = [{sort_by: {"order": "asc"}}]
            
            actual_page = es.search(index=index, body=body,filter_path=filter_path, params={"track_total_hits": "true"})

            if "hits" in actual_page["hits"]:
                result["hits"]["hits"] = result["hits"]["hits"] + actual_page["hits"]["hits"]
                if "total" in actual_page["hits"]:
                    result["hits"]["total"]["value"] = result["hits"]["total"]["value"] + actual_page["hits"]["total"]["value"]
            
                all_loaded = len(actual_page['hits']['hits']) < page_size
            else:
                all_loaded = True
            
            page += 1
    return result

def do_get_project(id):
    res = es.get(index=settings.ES_INDEX_NAME, doc_type='_doc', id=id)
    return res

def do_get_study_from_project(study_id, project_id):
    project = do_get_project(project_id)
    study = list(filter(lambda s: s['id'] == study_id, project['_source']['studies']))
    if len(study) > 0:
        return study[0]
    else:
        return None

def do_get_projects():
    if es.indices.exists(index=settings.ES_INDEX_NAME):
        res = do_elastic_search(settings.ES_INDEX_NAME, {"match_all": {}}, None, [])

    else:
        res = []
    return res
    
def do_get_project_genomes_id(id_project):
    project = do_get_project(id_project)
    if project['found']:
        genomes_index = project['_source']['genomes_index']
    return genomes_index

def do_get_project_genes_id(id_project):
    project = do_get_project(id_project)
    if project['found']:
        genes_index = project['_source']['genes_index']
    return genes_index

def do_get_project_genomes_names(id_project):
    project = do_get_project(id_project)
    names = []
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        res = do_elastic_search(genomes_index, {"match_all": {}}, "name.raw",['hits.hits._source.name,hits.total.value'])
        if "hits" in res:
            for i in res['hits']['hits']:
                names.append(i['_source']['name'])
    return names

def do_get_project_genes_index(id_project):
    project = do_get_project(id_project)
    if project['found']:
        genes_index = project['_source']['genes_index']
        res = do_elastic_search(genes_index, {"match_all": {}}, "id",[])
    return res

def do_get_project_genes_with_id(id_project):
    project = do_get_project(id_project)
    if project['found']:
        genes_index = project['_source']['genes_index']
        res = do_elastic_search(genes_index, {"match_all": {}}, "id",['hits.hits._source.translation,hits.hits._source.id,hits.total.value'])
        genes = dict()
        for i in res['hits']['hits']:
            genes[i['_source']['translation']] =  i['_source']['id']
    return genes

def do_get_project_alleles_with_id(id_project):
    project = do_get_project(id_project)
    if project['found']:
        alleles_index = project['_source']['alleles_index']
        res = do_elastic_search(alleles_index, {"match_all": {}}, "id",['hits.hits._source.gene, hits.hits._source.translation, hits.hits._source.id,hits.total.value'])
        alleles = dict()
        for i in res['hits']['hits']:
            if (i['_source']['gene'] == ''):
                alleles[i['_source']['translation'][0]] =  i['_source']['id']
            else:
                alleles[i['_source']['gene']] =  i['_source']['id']
    return alleles

def do_get_project_genes_names_index(id_project):
    project = do_get_project(id_project)
    translations = []
    if project['found']:
        genes_index = project['_source']['genes_index']
        res = do_elastic_search(genes_index, {"match_all": {}}, "id",['hits.hits._source.translation,hits.total.value'])
        if "hits" in res:
            for i in res['hits']['hits']:
                translations.append(i['_source']['translation'])
    return translations

def do_get_project_alleles_names_index(id_project):
    project = do_get_project(id_project)
    names = dict()
    if project['found']:
        alleles_index = project['_source']['alleles_index']
        res = do_elastic_search(alleles_index, {"match_all": {}}, "id",['hits.hits._id', 'hits.hits._source.translation,hits.hits._source.gene,hits.total.value'])
        if "hits" in res:
            for i in res['hits']['hits']:
                if (i['_source']['gene'] == ''):
                    names[i['_source']['translation'][0]] = {"_id": i['_id'], "translation": i['_source']['translation']}
                else:
                    names[i['_source']['gene']] = {"_id": i['_id'], "translation": i['_source']['translation']}
    return names

def update_genes_pertenences_and_set_processed_to_genome(genomes_index,genome_id,pertenencia_genes, pertenencia_alleles):
    res = es.update(index=genomes_index,doc_type='_doc',id=genome_id, body={
        "doc": {
            "gen_pertenence": pertenencia_genes,
            "alleles_pertenence": pertenencia_alleles,
            "processed": True
        }
    })
    return res

def do_get_project_non_processed_genomes_names(id_project):
    project = do_get_project(id_project)
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        es.indices.refresh()
        res = do_elastic_search(genomes_index, {"match": {"processed":False}}, "name.raw",['hits.hits._source.name,hits.total.value'])
    return res

def do_add_bulk_genomes(results):
    project = do_get_project(results[0][0])
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        bulk=[]
        for result in results:
            result[1]['_index']=genomes_index
            result[1]['_type']="_doc"
            bulk.append(result[1])
        do_bulk_updates(bulk)

def do_bulk_updates(updates):
    res = helpers.bulk(es, updates, chunk_size=1000)

def do_add_genes(id_project, genes):
    project = do_get_project(id_project)
    if project['found']:
        bulk = []
        genes_index = project['_source']['genes_index']
        for translation in genes:
            #res = es.index(index=genes_index, body=genes[translation])
            genes[translation]['_index']=genes_index
            genes[translation]["_type"]="_doc"
            bulk.append(genes[translation])
        res = helpers.bulk(es, bulk, chunk_size=1000)

def do_add_alleles(id_project, alleles):
    project = do_get_project(id_project)
    bulk = []
    if project['found']:
        alleles_index = project['_source']['alleles_index']
        for translation in alleles:
            #res = es.index(index=alleles_index, body=genes[translation])
            alleles[translation]['_index']=alleles_index
            alleles[translation]["_type"]="_doc"
            bulk.append(alleles[translation])
        res = helpers.bulk(es, bulk, chunk_size=1000)
            

def do_delete_project(id):
    try:
        res = es.get(index=settings.ES_INDEX_NAME, doc_type='_doc', id=id)
        if res["found"]:
            genomes_index = res["_source"]["genomes_index"]
            genes_index = res["_source"]["genes_index"]
            alleles_index = res["_source"]["alleles_index"]
            
            if es.indices.exists(index=genomes_index):
                es.indices.delete(index=genomes_index)
            
            if es.indices.exists(index=genes_index):
                es.indices.delete(index=genes_index)
            
            if es.indices.exists(index=alleles_index):
                es.indices.delete(index=alleles_index)

        res = es.delete(index=settings.ES_INDEX_NAME, doc_type='_doc', id=id)['result']
    except Exception as e:
        traceback.print_exc()
        res = str(e)
    return res

def do_update_project_state(id,new_state):
    project = {
        "doc": {
            "state":new_state,
        }
    }

    try:
        res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=id, body=project)['result']
    except Exception as e:
        traceback.print_exc()
        res = str(e)
    return res

def get_index_count(index):
    
    search_result = es.search(index=index,size= 0,params={"track_total_hits": "true"})

    if 'hits' in search_result:
        if 'total' in search_result['hits']:
            if 'value' in search_result['hits']['total']:
                return search_result['hits']['total']['value']
    return 0

def get_genomes_with_pertenence_count(index):
    
    body={
        "query": {
            "bool": {
                "must": {
                    "exists" : {
                        "field": "gen_pertenence"
                    }
                }
            }
        },
        "size": 0
    }

    search_result = es.search(index=index, body=body, params={"track_total_hits": "true"})

    result = 0

    if 'hits' in search_result:
        if 'total' in search_result['hits']:
            if 'value' in search_result['hits']['total']:
                result = search_result['hits']['total']['value']

    return result

def do_create_study(project_id, study_id, name, description, studyType):

    project = do_get_project(project_id)
    if project['found']:
        studies = []
        if 'studies' in project['_source']:
            studies = project['_source']['studies']

        filtered_studies = list(filter(lambda s: s["name"] == name, studies))
        name_available = len(filtered_studies) == 0
        if name_available:
        
            new_study = {
                "name": name,
                "description": description,
                "id": study_id,
                "state": list(filter(lambda s: s["value"] == "INITIAL", STUDY_STATES))[0]["value"],
                "rna_arch": DEFAULT_RNA_ARCH,
                "learning_rate": DEFAULT_LEARNING_RATE,
                "total_genes": 0,
                "studyType": studyType
            }

            study_parsed_to_painless = ""
            for k, v in new_study.items():
                if (isinstance(v, dict)):
                    study_parsed_to_painless += 'n_study.'+k+' = [:];'
                    for k2, v2 in v.items():
                        study_parsed_to_painless += 'n_study.'+k+'.'+str(k2)+' = '+ str(v2) +';'
                else:
                    if (isinstance(v, str)):
                        study_parsed_to_painless += 'n_study.'+k+' = "'+v+'";'
                    else:
                        study_parsed_to_painless += 'n_study.'+k+' = '+str(v)+';'

            project = {
                "script": {
                    "source": """
                        if (ctx._source.studies == null){
                            ctx._source.studies = [];
                        }
                        def n_study = [:];"""
                        + study_parsed_to_painless + """
                        ctx._source.studies.add(n_study);
                    """,
                    "lang": "painless"
                }
            }

            try:
                res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
            except Exception as e:
                traceback.print_exc()
                res = str(e)
        else:
            res = 'Nombre ingresado no disponible'
    else:
        res = 'El proyecto no se encuentra'
    return res

def do_edit_study(project_id, study_id, description):

    project = do_get_project(project_id)
    if project['found']:

        project = {
            "script": {
                "source": """
                    for (study in ctx._source.studies){
                        if (study.id == '""" + study_id + """'){
                            study.description = '""" + description + """';  
                        }
                    }
                """,
                "lang": "painless"
            }
        }

        try:
            res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
        except Exception as e:
            traceback.print_exc()
            res = str(e)
        
    else:
        res = 'El proyecto no se encuentra'
    return res

def do_delete_study(project_id, study_id):

    project = do_get_project(project_id)
    if project['found']:

        if 'studies' in project['_source']:
            studies = project['_source']['studies']

            target_study = None
            
            for study in studies:
                if study["id"] == study_id:
                    target_study = study

            if target_study is not None:

                state = target_study["state"]
                if is_valid_state_to_do_actions(state):
                    
                    try:
                        remove_genomes_study_label(study_id, project["_source"]["genomes_index"])
                    
                        project = {
                            "script": {
                                "source": """
                                    def study_to_remove = null;
                                    for (study in ctx._source.studies){
                                        if (study.id == '""" + target_study["id"] + """'){
                                            study_to_remove = ctx._source.studies.indexOf(study);  
                                        }
                                    }
                                    if (study_to_remove != null){
                                        ctx._source.studies.remove(study_to_remove);
                                    }
                                """,
                                "lang": "painless"
                            }
                        }

                        try:
                            res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
                        except Exception as e:
                            traceback.print_exc()
                            res = str(e)
                    except Exception as e:
                        traceback.print_exc()
                        res = str(e)
                else:
                    res = 'El estudio no posee un estado válido para eliminarlo'
            else:
                res = 'El estudio no se encuentra'
        else:
            res = 'El proyecto no posee estudios'
    else:
        res = 'El proyecto no se encuentra'
    return res

def remove_genomes_study_label(study_id, genomes_index):
    
    query = {
        "script": {
            "source": "ctx._source.remove('"+study_id+"')",
            "lang": "painless"
        },
        "query": {
            "exists": {
                "field": study_id
            }
        }
    }

    res = es.update_by_query(index=genomes_index, body=query, params={"track_total_hits": "true"})
    return res

def add_single_genome_to_project(srr, id_project):
    project = do_get_project(id_project)
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        res = es.index(index=genomes_index,body=srr)
    return res["result"]

def update_single_genome_on_project(srr, id_project):
    project = do_get_project(id_project)
    if project['found']:
        genomes_index = project['_source']['genomes_index']
        genome_id = do_get_project_genome_id(genomes_index,srr['name'])
        query = {
            "doc":{
                "processed":False,
                "genes":srr["genes"]
            }
        }
        res = es.update(index=genomes_index,body=query,id=genome_id,doc_type="_doc")
    return res["result"]


def update_alleles_translations_on_gen_from_project(new_translations, allele_gene, id_project):
    project = do_get_project(id_project)
    if project['found']:
        alleles_index = project['_source']['alleles_index']
        query = {
            "script": {
                "source": "ctx._source.translation = params.new_translations",
                "lang": "painless",
                "params": {
                    "new_translations":new_translations
                }
            }
        }
        res = es.update(index=alleles_index,body=query, id=allele_gene, doc_type="_doc")
    return res["result"]


def do_get_project_genome(genomes_index,name):
    res = es.search(index=genomes_index, body={"query": {"term": {"name.raw":name}}}, params={"track_total_hits": "true"})
    return res

def do_get_project_genome_id(genomes_index,name):
    return do_get_project_genome(genomes_index,name)["hits"]["hits"][0]["_id"]

def get_genomes_label_information(index, study_id):
    
    body={
        "query": {
            "match_all": {}
        },
        "aggs": {
            "labeled_true": {
                "filter": { "term": { study_id: 1 } },
                "aggs": {
                    "amount": {
                        "value_count": {
                            "field": study_id
                        }
                    }
                }
            },
            "labeled_false": {
                "filter": { "term": { study_id: 0 } },
                "aggs": {
                    "amount": {
                        "value_count": {
                            "field": study_id
                        }
                    }
                }
            }
        },
        "size": 0
    }

    search_result = es.search(index=index, body=body, params={"track_total_hits": "true"})

    result = dict()

    if 'hits' in search_result:
        if 'total' in search_result['hits']:
            if 'value' in search_result['hits']['total']:
                result['total'] = search_result['hits']['total']['value']
    
    if 'aggregations' in search_result:
        if 'labeled_true' in search_result['aggregations'] and 'labeled_false' in search_result['aggregations']:
            if 'amount' in search_result['aggregations']['labeled_true'] and 'amount' in search_result['aggregations']['labeled_false']:
                if 'value' in search_result['aggregations']['labeled_true']['amount'] and 'value' in search_result['aggregations']['labeled_false']['amount']:
                    result['labeled_true'] = search_result['aggregations']['labeled_true']['amount']['value']
                    result['labeled_false'] = search_result['aggregations']['labeled_false']['amount']['value']

    return result

def get_genomes_name_unlabeled(project_id, study_id):

    project = do_get_project(project_id)
    if project['found']:

        genomes_index = project['_source']['genomes_index']

        body={
            "bool": {
                "must_not": {
                    "exists": {
                        "field": study_id
                    }
                }
            }
        }

        search_result = do_elastic_search(genomes_index, body, "name.raw", ['hits.hits._source.name,hits.total.value'])

        result = list()
        if 'hits' in search_result:
            if 'hits' in search_result['hits']:
                for hit in search_result['hits']['hits']:
                    result.append(hit['_source']['name'])

        return result
    return None

def get_genomes_labeled(project_id, study_id):

    project = do_get_project(project_id)
    if project['found']:

        genomes_index = project['_source']['genomes_index']

        body={
            "bool": {
                "must": {
                    "exists": {
                        "field": study_id
                    }
                }
            }
        }

        search_result = do_elastic_search(genomes_index, body, "name.raw", ['hits.hits._source.gen_pertenence,hits.hits._source.alleles_pertenence, hits.hits._source.'+study_id+',hits.total.value'])

        result = list()
        if 'hits' in search_result:
            if 'hits' in search_result['hits']:
                for hit in search_result['hits']['hits']:
                    result.append(hit['_source'])

        return result
    return None

def is_valid_state_to_do_actions(state):
    valid_states = ['INITIAL', 'READY_FOR_TRAINING', 'READY_FOR_QUESTIONS']
    return state in valid_states

def is_valid_state_to_train(state):
    valid_states = ['READY_FOR_TRAINING', 'READY_FOR_QUESTIONS']
    return state in valid_states

def is_valid_state_to_optimize(state):
    valid_states = ['READY_FOR_QUESTIONS']
    return state in valid_states

def study_validation_wrapper(project_id, study_id, inner_function, args, validation_function):

    project = do_get_project(project_id)
    if project['found']:

        project_state = project['_source']['state']
        if project_state == 'READY_FOR_STUDIES':
            
            if 'studies' in project['_source']:
                studies = project['_source']['studies']

                target_study = list(filter(lambda s: s["id"] == study_id, studies))

                if len(target_study) == 1:
                    
                    previous_state = target_study[0]["state"]
                    if validation_function(previous_state):
                        
                        res = inner_function(project_id, study_id, project, studies, target_study[0], args)
                        
                    else:
                        res = 'El estudio no posee un estado válido para cargar etiquetas'
                else:
                    res = 'El estudio no se encuentra en el proyecto'
            else:
                res = 'El proyecto no posee estudios'
        else:
            res = 'El proyecto no está listo para estudios'
    else:
        res = 'El proyecto no se encuentra'
    return res

def do_set_project_genome_label_for_study(project_id, study_id, genome_label_pairs):
    
    def inner_function(project_id, study_id, project, studies, target_study, args):
        
        genome_label_pairs = args[0]

        #CHANGE STATE TO LOADING_LABELS
        up_res = do_set_study_state(project_id, study_id, studies, "LOADING_LABELS")
        
        if up_res == 'updated':
            t = ThreadWithReturnValue(
                target=update_genomes_label, 
                args=(project_id, study_id, studies, genome_label_pairs, project["_source"]["genomes_index"], project["_source"]["genes_index"])
            )
            t.start()

            res = 'updating'
        else:
            res = 'No se pudo actualizar el estado del estudio'

        return res
    
    return study_validation_wrapper(project_id, study_id, inner_function, (genome_label_pairs,), is_valid_state_to_do_actions)

def label_is_1(label_value):
    label_value = label_value.strip()
    return label_value == "1" or label_value == "SI" or label_value == "si" or label_value == "Si"

def label_is_0(label_value):
    label_value = label_value.strip()
    return label_value == "0" or label_value == "NO" or label_value == "no" or label_value == "No"

def update_genomes_label(project_id, study_id, studies, genome_label_pairs, genomes_index, genes_index):
    
    label_with_1 = list()
    label_with_0 = list()

    for genome_label_pair in genome_label_pairs:
        if len(genome_label_pair) == 2:
            
            genome_name = genome_label_pair[0]
            label_value = genome_label_pair[1]
            
            if label_is_1(label_value):
                label_with_1.append(genome_name)
            elif label_is_0(label_value):
                label_with_0.append(genome_name)
    
    query_to_1 = {
        "script": {
            "source": "ctx._source."+study_id+" = 1",
            "lang": "painless"
        },
        "query": {
            "terms": {
                "name.raw": label_with_1
            }
        }
    }

    query_to_0 = {
        "script": {
            "source": "ctx._source."+study_id+" = 0",
            "lang": "painless"
        },
        "query": {
            "terms": {
                "name.raw": label_with_0
            }
        }
    }

    try:
        es.update_by_query(index=genomes_index, body=query_to_1)
        es.update_by_query(index=genomes_index, body=query_to_0)
    except Exception as e:
        traceback.print_exc()
        #return str(e)
        pass #continue and update as much as possible, so next time we download unlabeled genomes file will be smaller
    
    total_genes = get_index_count(genes_index)
    up_res_total_genes = do_set_study_total_genes(project_id, study_id, studies, total_genes)

    #CHANGE STATE TO READY_FOR_TRAINING
    up_res_state = do_set_study_state(project_id, study_id, studies, "READY_FOR_TRAINING")
    return [up_res_total_genes, up_res_state]

def do_set_study_total_genes(project_id, study_id, studies, total_genes):
    
    project = {
        "script": {
            "source": """
                for (study in ctx._source.studies){
                    if (study.id == '""" + study_id + """'){
                        study.total_genes = """ + str(total_genes) + """;  
                    }
                }
            """,
            "lang": "painless"
        }
    }

    try:
        res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
    except Exception as e:
        traceback.print_exc()
        res = str(e)

    return res

def do_set_study_state(project_id, study_id, studies, new_state):
    
    new_s = list(filter(lambda s: s["value"] == new_state, STUDY_STATES))[0]["value"]

    project = {
        "script": {
            "source": """
                for (study in ctx._source.studies){
                    if (study.id == '""" + study_id + """'){
                        study.state = '""" + new_s + """';  
                    }
                }
            """,
            "lang": "painless"
        }
    }

    try:
        res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
    except Exception as e:
        traceback.print_exc()
        res = str(e)

    return res

def do_set_study_rna_and_learning_rate(project_id, study_id, studies, rna, lr):
    
    rna_parsed_to_painless = 'study.rna_arch = [:];'
    for k, v in rna.items():
        rna_parsed_to_painless += 'study.rna_arch.'+str(k)+' = '+ str(v) +';'

    project = {
        "script": {
            "source": """
                for (study in ctx._source.studies){
                    if (study.id == '""" + study_id + """'){
                        study.learning_rate = """ + str(lr) + """;
                        """ + rna_parsed_to_painless + """
                    }
                }
            """,
            "lang": "painless"
        }
    }

    try:
        res = es.update(index=settings.ES_INDEX_NAME, doc_type='_doc', id=project_id, body=project)['result']
    except Exception as e:
        traceback.print_exc()
        res = str(e)
    
    return res

def train_RNA(project_id, study_id, epochs, learning_rate, cpus, rna_arch, optimize, percentage_of_training_data):
    
    def inner_function(project_id, study_id, project, studies, target_study, args):
        
        epochs = args[0]
        learning_rate = args[1]
        cpus = args[2]
        rna_arch = args[3]
        optimize = args[4]
        percentage_of_training_data = args[5]
        
        genomes = get_genomes_labeled(project_id, study_id)
        total_genes = target_study["total_genes"]
        
        if len(genomes) > 1:
            if total_genes > 1:

                do_set_study_rna_and_learning_rate(project_id, study_id, studies, rna_arch, learning_rate)

                res = ia_functions.train(project_id, project["_source"]["folder"], studies, study_id, genomes, total_genes, rna_arch, learning_rate, epochs, cpus, optimize, percentage_of_training_data)

            else:
                res = 'El numero de genes para el proyecto es muy chico'
        else:
            res = 'El numero de genomas etiquetados para el estudio es muy chico'

        return res
    
    validate_function = is_valid_state_to_train if not optimize else is_valid_state_to_optimize

    return study_validation_wrapper(project_id, study_id, inner_function, (epochs, learning_rate, cpus, rna_arch, optimize, percentage_of_training_data), validate_function)

def get_genes_info(project, study, genes_id):

    genes_index = project['genes_index']
    pertenence_attribute_name = 'gen_pertenence'
    if study['studyType'] == 'Alleles':
        genes_index = project['alleles_index']
        pertenence_attribute_name = 'alleles_pertenence'
    
    #SEARCH GENES DOCUMENTS IN INDEX
    query = {
        "query": {
            "terms": {
                "id": list(map(lambda i: int(i), genes_id))
            }
        },
        "size": 50
    }

    search_result = es.search(index=genes_index, body=query, params={"track_total_hits": "true"}, filter_path=['hits.hits._source'])

    #MAKE DICT WITH GENES SOURCE
    genes = dict()
    if 'hits' in search_result:
        if 'hits' in search_result['hits']:
            for hit in search_result['hits']['hits']:
                genes[hit['_source']['id']] = hit['_source']

    #SEARCH EACH GEN OCURRENCES IN GENOMES LABELED FOR STUDY
    ocurrences_query = {
        "query": {
            "bool": { 
                "must": [
                    {
                        "exists": {
                            "field": study['id']
                        }
                    }
                ]
            }
        },
        "aggs": {},
        "size": 0
    }

    #MAKE AGGREGATION FOR EACH GEN
    for k, v in genes.items():
        ocurrences_query["aggs"][str(k)] = {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                pertenence_attribute_name: k
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "ocurrences_with_1": {
                    "sum": {
                        "field": study['id']
                    }
                }
            }
        }

    aggs_search_result = es.search(index=project['genomes_index'], body=ocurrences_query, params={"track_total_hits": "true"}, filter_path=['aggregations'])

    #PARSE RESULTS TO SEND TO FRONT
    result = list()
    for gen_id in genes_id:
        res = genes[int(gen_id)]
        aggs = aggs_search_result['aggregations'][gen_id]
        
        total = aggs["doc_count"]
        with_1 = int(aggs["ocurrences_with_1"]["value"])
        
        res["total_ocurrences"] = total
        res["ocurrences_with_1"] = with_1
        res["ocurrences_with_0"] = total - with_1

        result.append(res)

    return result

def get_gen_info_for_xlsx(study_id, genomes_index, genes_index, alleles_index, gene):

    sheet1 = []
    sheet2 = []

    #1 -> ir a genes_index y buscar para los translations los id de esos genes 
        
    should_array = []
    for translation in gene["translation"]:
        temp_dict = {
            "match_phrase": {
                "translation": translation
            }
        }
        should_array.append(temp_dict)

    query = {
        "query": {
            "bool": {
                "should": should_array,
                "minimum_should_match": 1
            }
        },
        "size": len(gene['translation'])
    }

    search_result = es.search(index=genes_index, body=query, params={"track_total_hits": "true"}, filter_path=['hits.hits._source.id', 'hits.hits._source.translation'])

    #2 -> buscar en genomes_index, para cada gen id los genomas que tengan en gen_pertenence el id de ese gen

    max_size = get_index_count(genomes_index)

    if "hits" in search_result:
        if "hits" in search_result["hits"]:
            for hit in search_result["hits"]["hits"]:
                
                query = {
                    "query": {
                        "match": {
                            "gen_pertenence": hit["_source"]["id"]
                        }
                    },
                    "size": max_size
                }

                genome_search_result = es.search(index=genomes_index, body=query, params={"track_total_hits": "true"}, filter_path=['hits.total','hits.hits._source.name','hits.hits._source.'+study_id])

                labeled_yes = 0
                labeled_no = 0

                if "hits" in genome_search_result:
                    if "hits" in genome_search_result["hits"]:
                        for genome_hit in genome_search_result["hits"]["hits"]:
                            
                            tmp_list = [genome_hit["_source"]["name"], genome_hit["_source"][study_id], hit["_source"]["translation"]]

                            sheet2.append(tmp_list)

                            if genome_hit["_source"][study_id] == 0:
                                labeled_no = labeled_no + 1
                            elif genome_hit["_source"][study_id] == 1:
                                labeled_yes = labeled_yes + 1
                            
                        sheet1.append([hit["_source"]["translation"], labeled_yes+labeled_no, labeled_yes, labeled_no])

    return sheet1, sheet2