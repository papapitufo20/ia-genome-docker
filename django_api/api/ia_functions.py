import tensorflow as tf
import tensorflow.keras as kr

import os
import numpy as np
import math
from . import elastic_functions
from .utils import ThreadWithReturnValue, CPUS
import contextlib
import traceback
import functools

import shap
import matplotlib.pyplot as plt

tf.config.threading.set_intra_op_parallelism_threads(CPUS)
tf.config.threading.set_inter_op_parallelism_threads(CPUS)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def prepare_genomes_as_X_Y(genomes, total_genes, study_id, study_type, processed_genomes):
    x = list()
    y = list()
    if study_type is None or study_type == 'Genes':
        pertenence_type = "gen_pertenence"
    else:
        pertenence_type = "alleles_pertenence"

    genes_ocurrences = dict()
    for genome in genomes:
        for pertenence in genome[pertenence_type]:
            genes_ocurrences[pertenence] = 1 if pertenence not in genes_ocurrences else genes_ocurrences[pertenence] + 1
            
    gen_index_in_dict = 0
    genes_filtered = dict()
    for k, v in genes_ocurrences.items():
        if v > 2 and v < len(genomes):
            genes_filtered[k] = gen_index_in_dict
            gen_index_in_dict = gen_index_in_dict + 1

    total_genes_filtered = len(genes_filtered)
    print('cantidad de genes filtrados: '+str(total_genes_filtered)+' de un total de: '+str(total_genes))

    #use processed_genomes when evaluate, use genomes when train
    genomes_to_make_pertenence_matrix = processed_genomes if processed_genomes is not None else genomes
    for genome in genomes_to_make_pertenence_matrix:
        parsed_pertenence = [0] * total_genes_filtered

        if len(genome[pertenence_type]) > 0: #ignore genome if no genes
            for pertenence in genome[pertenence_type]:
                #validation to avoid using genes outside index when evaluating genome with genes not used on training
                if pertenence in genes_filtered:
                    #if pertenence <= total_genes_filtered:            
                    parsed_pertenence[genes_filtered[pertenence]] = 1

            x.append(parsed_pertenence)
            #study_id only for training
            if study_id is not None:
                y.append(genome[study_id])

    #study_id only for training
    if study_id is not None:
        arraycon0 = []
        arraycon1 = []
        for index, elem in enumerate(y):
            if elem == 0:
                arraycon0.append(x[index])
            else:
                arraycon1.append(x[index])

        x = arraycon0 + arraycon1
        y = ([0] * len(arraycon0)) + ([1] * len(arraycon1))

    return x, y, total_genes_filtered, genes_filtered

def create(total_genes, rna_arch, total_genes_filtered):

    model = kr.Sequential()
    model.add(kr.Input(shape=(total_genes_filtered,)))

    for key, value in rna_arch.items():
        model.add(kr.layers.Dense(value, activation='relu'))
    model.add(kr.layers.Dense(1, activation='sigmoid'))

    return model

def compile(model, lr):
    model.compile(loss='mse', optimizer=kr.optimizers.SGD(learning_rate=lr), metrics=['acc'])
    return model

def do_train(model, x, y, epochs, project_id, project_folder, studies, study_id, cpus, percentage_of_training_data):

    #si cambiamos esto acordarse de cambiar el 0.45 en analyze_model
    percentage = (percentage_of_training_data / 100) / 2 if percentage_of_training_data != 100 else percentage_of_training_data / 100
    split_index = math.floor(len(x) * percentage)

    x_train = x[:split_index] + x[-split_index:]
    y_train = y[:split_index] + y[-split_index:]

    x_test = x[split_index:][:-split_index]
    y_test = y[split_index:][:-split_index]

    file_url = get_train_result_file_path(project_folder, study_id)
    with open(file_url+"/results.txt", 'w') as file_output:
        with contextlib.redirect_stdout(file_output):
            print("Total samples: " + str(len(x)) + " samples\n")
            print("Train data: "+str(len(x_train))+" samples\n")            
            model.fit(np.array(x_train), np.array(y_train), epochs=epochs)
            print("\nEvaluate")
            results, test_data_size = evaluate(model, x_test, y_test) if percentage_of_training_data != 100 else ([], 0)
            print("\nTest data: " + str(test_data_size) + " samples")
            if len(results) == 2:
                print("loss: "+str(results[0])+" - acc: "+str(results[1]))
            else:
                print(results)
            print("")

    elastic_functions.do_set_study_state(project_id, study_id, studies, 'READY_FOR_QUESTIONS')

    save(model, project_folder, study_id)

    return model

def evaluate(model, x, y):
    results = model.evaluate(np.array(x), np.array(y))
    return results, len(x)

def get_train_result_file_path(project_folder, study_id):
    return '../../projects/'+project_folder+"/studies/"+study_id+"/training_logs"

def get_model_path(project_folder, study_id):
    return '../../projects/'+project_folder+"/studies/"+study_id+"/keras_model"

def save(model, project_folder, study_id):
    path = get_model_path(project_folder, study_id)
    model.save(path)

def train(project_id, project_folder, studies, study_id, genomes, total_genes, rna_arch, lr, epochs, cpus, optimize, percentage_of_training_data):
    study = list(filter(lambda s: s['id'] == study_id, studies))
    if len(study) == 1:
        study = study[0]
        study_type = study['studyType']
        x, y, total_genes_filtered, genes_filtered = prepare_genomes_as_X_Y(genomes, total_genes, study_id, study_type, None)

        model = None

        if not optimize:
            model = create(total_genes, rna_arch, total_genes_filtered)
            model = compile(model, lr)
        else:
            try:
                model = kr.models.load_model(get_model_path(project_folder, study_id))
            except Exception as e:
                traceback.print_exc()
                #ImportError	if loading from an hdf5 file and h5py is not available.
                #IOError	In case of an invalid savefile.
                return 'No se pudo cargar el modelo para el estudio'

        if model is not None:
            
            up_res = elastic_functions.do_set_study_state(project_id, study_id, studies, "TRAINING")
            if up_res == 'updated':
            
                t = ThreadWithReturnValue(
                    target=do_train, 
                    args=(model, x, y, epochs, project_id, project_folder, studies, study_id, cpus, percentage_of_training_data)
                )
                t.start()

                res = 'training'
            else:
                res = 'No se pudo actualizar el estado del estudio'
        else:
            res = 'No se pudo crear el modelo'
    else:
        res = 'No se encuentra el estudio'

    return res

def predict(project_id, processed_genomes, study, project_folder):
    study_type = study['studyType']
    genomes = elastic_functions.get_genomes_labeled(project_id, study['id'])
    x, y, total_genes_filtered, genes_filtered = prepare_genomes_as_X_Y(genomes, study['total_genes'], None, study_type, processed_genomes)
    
    model = None
    try:
        model = kr.models.load_model(get_model_path(project_folder, study['id']))
    except Exception as e:
        traceback.print_exc()
        #ImportError	if loading from an hdf5 file and h5py is not available.
        #IOError	In case of an invalid savefile.
        return 'No se pudo cargar el modelo para el estudio'
    
    if model is not None:
        #predictions = model.predict_classes(x)
        predictions = model.predict(x)
        count=0
        result = []
        for genome in processed_genomes:
            prediction = "SI" if (predictions[count][0] > 0.5).astype("int32") == 1 else "NO"
            accuracy = predictions[count][0]
            result.append({'Genome': genome['name'], 'Prediction': prediction, 'Accuracy': accuracy})
            count+=1
        return result

def get_training_results_logs(project_folder, study_id):
    file_url = get_train_result_file_path(project_folder, study_id)
    try:
        if os.path.exists(file_url+"/results.txt"):
            file = open(file_url+"/results.txt", 'r')
            lines = file.readlines()
            if len(lines) > 2000:
                lines = lines[0:1000] + ["\n","\n","... //Para ver archivo de logs completo ingresar a carpeta del estudio.","\n","\n"] + lines[-1000:]
            return lines
        else:
            return []
    except Exception as e:
        traceback.print_exc()
        return []

def analyze_model(project_id, project_folder, study_id, studies):
    try:
        study = list(filter(lambda s: s['id'] == study_id, studies))
        if len(study) == 1:
            study = study[0]
            study_type = study['studyType']
            genomes = elastic_functions.get_genomes_labeled(project_id, study_id)
            x, y, total_genes_filtered, genes_filtered = prepare_genomes_as_X_Y(genomes, None, study_id, study_type, None)

            model = None
            try:
                model = kr.models.load_model(get_model_path(project_folder, study_id))
            except Exception as ex:
                pass

            if model is not None:

                split_index = math.floor(len(x) * 0.45) #0.9 / 2 to balance 0 and 1 labels

                x_train = x[:split_index] + x[-split_index:]
                x_test = x[split_index:][:-split_index]

                shap.initjs()
                
                #explainer = shap.KernelExplainer(model, np.array(x_train))
                explainer = shap.DeepExplainer(model, np.array(x_train))

                x_test_np = np.array(x_test)

                shap_values = explainer.shap_values(x_test_np)
                
                file_url = get_train_result_file_path(project_folder, study_id)

                feature_names = list(genes_filtered.keys())
                feature_names = list(map(lambda x: "Gen: " + str(x), feature_names))

                try:
                    plt.clf()

                    shap.summary_plot(shap_values, x_test_np, plot_type="bar", plot_size=(10.80, 19.20), max_display=50, feature_names=feature_names)

                    plt.savefig(file_url+"/summary_plot.pdf", format='pdf')
                    plt.savefig(file_url+"/summary_plot.svg", format='svg')
                except:
                    traceback.print_exc()
    except Exception as e:
        traceback.print_exc()

    elastic_functions.do_set_study_state(project_id, study_id, studies, 'READY_FOR_QUESTIONS')
 
def get_model_plots_analysis(project_folder, study_id):
    file_url = get_train_result_file_path(project_folder, study_id)
    try:
        if os.path.exists(file_url+"/summary_plot.svg"):
            file = open(file_url+"/summary_plot.svg", 'r')
            file = file.read()
            return file
        else:
            return None
    except Exception as e:
        traceback.print_exc()
        return None
