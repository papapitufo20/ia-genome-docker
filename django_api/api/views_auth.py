from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from api.models import User

from .serializers import UserSerializer, GroupSerializer

from rest_framework import viewsets, permissions, authentication, status
from django.contrib.auth import authenticate
from .authentication import token_expire_handler, expires_in

from django.http import HttpResponse, JsonResponse

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from .utils import CPUS, DEFAULT_RNA_ARCH

@api_view(["POST"])
@permission_classes([permissions.AllowAny])
def login(request):
    
    user = authenticate(
        username = request.data['username'],
        password = request.data['password'] 
    )
    
    if not user:
        return JsonResponse({'detail': 'Invalid Credentials'}, status=status.HTTP_404_NOT_FOUND)
    
    # Me traigo el token asociado a un usuario o creo uno nuevo.
    token, _ = Token.objects.get_or_create(user = user)
    
    #token_expire_handler will check, if the token is expired it will generate new one
    is_expired, token = token_expire_handler(token)
    user_serialized = UserSerializer(user, context={'request': request})
    
    return JsonResponse({
        'username': user_serialized.data['username'], 
        'expires_in': expires_in(token).total_seconds(),
        'token': token.key,
        'id': user.id
    }, status=status.HTTP_200_OK)

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def default_values(request):
    return JsonResponse({'cpus': CPUS, 'default_rna': DEFAULT_RNA_ARCH}, status=status.HTTP_200_OK, safe=False)