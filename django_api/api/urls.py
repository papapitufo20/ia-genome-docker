from django.urls import include, path
from rest_framework import routers
from . import views, views_auth, views_project, views_file_processing, views_studies

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('login', views_auth.login, name='login'),
    path('default_values', views_auth.default_values, name='default_values'),

    path('create_project',views_project.create_project,name='create_project'),
    path('edit_project/<id>', views_project.edit_project, name='edit_project'),
    path('delete_project',views_project.delete_project,name='delete_project'),
    
    path('get_project/<id>',views_project.get_project,name='get_project'),
    path('get_projects',views_project.get_projects,name='get_projects'),
    path('get_project_states',views_project.get_project_states,name='get_project_states'),

    path('<project_id>/create_study',views_studies.create_study,name='create_study'),
    path('<project_id>/edit_study/<study_id>',views_studies.edit_study,name='edit_study'),
    path('<project_id>/delete_study',views_studies.delete_study,name='delete_study'),
    path('get_study_states',views_studies.get_study_states,name='get_study_states'),
    path('project/<project_id>/force_state_chage_project',views_project.force_state_chage_project,name='force_state_chage_project'),
    path('project/<project_id>/study/<study_id>/force_state_change_study',views_studies.force_state_change_study,name='force_state_change_study'),
    path('project/<project_id>/study/<study_id>',views_studies.get_study,name='get_study'),
    path('project/<project_id>/study/<study_id>/download_unlabeled_genomes',views_studies.download_unlabeled_genomes,name='download_unlabeled_genomes'),
    path('project/<project_id>/study/<study_id>/download_csv',views_file_processing.download_csv,name='download_csv'),
    path('project/<project_id>/study/<study_id>/delete_csv',views_file_processing.delete_csv,name='delete_csv'),
    path('project/<project_id>/study/<study_id>/analyze_model',views_studies.analyze_model,name='analyze_model'),
    path('project/<project_id>/study/<study_id>/get_model_analysis',views_studies.get_model_analysis,name='get_model_analysis'),
    path('project/<project_id>/study/<study_id>/get_genes_info',views_studies.get_genes_info,name='get_genes_info'),
    path('project/<project_id>/study/<study_id>/multiple_sequence_alignment',views_studies.multiple_sequence_alignment,name='multiple_sequence_alignment'),
    path('project/<project_id>/study/<study_id>/download_multiple_sequence_alignment',views_studies.download_multiple_sequence_alignment,name='download_multiple_sequence_alignment'),
    path('project/<project_id>/study/<study_id>/download_gen_info_xlsx',views_studies.download_gen_info_xlsx,name='download_gen_info_xlsx'),

    path('project/<project_id>/study/<study_id>/set_label_to_genomes',views_studies.set_label_to_genomes,name='set_label_to_genomes'),

    path('project/<project_id>/study/<study_id>/train',views_studies.train,name='train'),
    path('project/<project_id>/study/<study_id>/optimize',views_studies.optimize,name='optimize'),

    path('project/<project_id>/study/<study_id>/evaluate',views_studies.evaluate,name='evaluate'),

    path('process_fastq/<id>',views_file_processing.process_fastq,name='process_fastq'),
    path('process_fasta/<id>',views_file_processing.process_fasta,name='process_fasta'),
    path('process_gbf/<id>',views_file_processing.process_gbf,name='process_gbf'),

    path('', include(router.urls)),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
