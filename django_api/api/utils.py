from django.conf import settings

STATES = [
    {"value": "INITIAL", "text": "INICIAL"},
    {"value": "PROCESSING_FASTQ", "text": "PROCESANDO FASTQ"},
    {"value": "PROCESSING_FASTA", "text": "PROCESANDO FASTA"},
    {"value": "PROCESSING_GBF", "text": "PROCESANDO GBF"},
    {"value": "READY_FOR_STUDIES", "text": "LISTO PARA ESTUDIOS"}
]

STUDY_STATES = [
    {"value": "INITIAL", "text": "INICIAL"},
    {"value": "LOADING_LABELS", "text": "CARGANDO ETIQUETAS"},
    {"value": "READY_FOR_TRAINING", "text": "LISTO PARA ENTRENAR"},
    {"value": "TRAINING", "text": "ENTRENANDO"},
    {"value": "READY_FOR_QUESTIONS", "text": "LISTO PARA EVALUAR"},
    {"value": "EVALUATING", "text": "EVALUANDO"},
    {"value": "ANALYZING_MODEL", "text": "ANALIZANDO MODELO"},
    {"value": "ALIGNING_MULTIFASTA", "text": "ALINEANDO MULTIFASTA"},
    {"value": "GENERATING_IQTREE", "text": "GENERANDO IQTREE"}
]

CPUS = settings.MAX_CPUS

DEFAULT_EPOCHS = 1500

DEFAULT_RNA_ARCH = {
    1: 8192,
    2: 4096,
    3: 1024,
    4: 256,
    5: 64,
    6: 16
}

DEFAULT_LEARNING_RATE = 0.0005

DEFAULT_PERCENTAGE_OF_TRAINING_DATA = 90

from threading import Thread

class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        #print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return
