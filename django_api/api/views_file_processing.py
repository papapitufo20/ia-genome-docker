import csv
import traceback

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes

from . import elastic_functions, file_processing_functions
from .utils import CPUS, STATES, ThreadWithReturnValue


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def process_fastq(request, id):
    do_res = elastic_functions.do_get_project(id)
    folder = do_res['_source']['folder']
    path_from = settings.GENOMES_PATH+'/'+folder+'/genomes'
    path_to = settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fastq'
    file_type = 'gz'
    #file_processing_functions.move_file_type_from_to('gz', settings.GENOMES_PATH+'/'+folder+'/genomes', settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fastq')
    res = process_generic(request, id, file_processing_functions.remaining_fastq, "PROCESSING_FASTQ", file_processing_functions.process_genome_folder_fastq, "fastq", 'genomes/tmp', path_from, path_to, file_type)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def process_fasta(request, id):
    do_res = elastic_functions.do_get_project(id)
    folder = do_res['_source']['folder']
    path_from = settings.GENOMES_PATH+'/'+folder+'/genomes'
    path_to = settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fasta'
    file_type = 'fasta'
    #file_processing_functions.move_file_type_from_to('fasta', settings.GENOMES_PATH+'/'+folder+'/genomes', settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fasta')
    res = process_generic(request, id, file_processing_functions.remaining_fasta, "PROCESSING_FASTA", file_processing_functions.process_genome_folder_fasta, "fasta", 'genomes/tmp', path_from, path_to, file_type)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def process_gbf(request, id):
    do_res = elastic_functions.do_get_project(id)
    folder = do_res['_source']['folder']
    path_from = settings.GENOMES_PATH+'/'+folder+'/genomes'
    path_to = settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/gbf'
    file_type = 'gbf'
    #file_processing_functions.move_file_type_from_to('gbf', settings.GENOMES_PATH+'/'+folder+'/genomes', settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/gbf')
    res = process_generic(request, id, file_processing_functions.remaining_gbf, "PROCESSING_GBF", file_processing_functions.process_genome_folder_gbf, "gbf", 'genomes/tmp', path_from, path_to, file_type)
    return res


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def download_csv(request, project_id, study_id):
    filename = request.data['name']
    genomes = file_processing_functions.get_csv(project_id, study_id, filename)
    if genomes is not None:
        #if len(genomes) > 0:
        res = HttpResponse(
            content_type='text/csv'
        )

        res['Content-Disposition'] = 'attachment; filename="'+filename+'"'

        writer = csv.writer(res)
        
        for genome in genomes:
            writer.writerow(genome)
        #else:
        #    res = JsonResponse({'detail': 'No hay genomas sin etiqueta para este estudio'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        res = JsonResponse({'detail': 'No se pudo exportar CSV'}, status=status.HTTP_400_BAD_REQUEST)

    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def delete_csv(request, project_id, study_id):
    filename = request.data['filename']
    file_processing_functions.delete_csv(project_id, study_id, filename)
    return JsonResponse({'detail': 'Resultado eliminado'}, status=status.HTTP_200_OK)

def process_generic(request, id, remaining_files, new_state, process_function, process_type, folder, path_from, path_to, file_type):
    try:
        cpus = CPUS
        try:
            cpus_from_request = int(request.data['cpus'])
            cpus = cpus_from_request if (cpus_from_request > 0 and cpus_from_request <= cpus) else cpus 
        except Exception as e:
            traceback.print_exc()
            pass
        
        do_res = elastic_functions.do_get_project(id)
        if (do_res['found']):
            project_state = do_res['_source']['state']
            
            if project_state == "INITIAL" or project_state == "READY_FOR_STUDIES":
                project_folder = do_res['_source']['folder']
                kingdom = do_res['_source']['kingdom']
                files_count = remaining_files(settings.GENOMES_PATH+'/'+project_folder+'/'+folder+'/'+process_type) + file_processing_functions.remaining_file_type(path_from, file_type)
                if files_count > 0:
                    update_res = elastic_functions.do_update_project_state(id, new_state)
                    if (update_res == 'updated' or update_res == 'noop'):
                        #ROOT THREAD SPAWNED - ASYNC PROCESS
                        t = ThreadWithReturnValue(
                            target=process_function, 
                            args=(id, project_folder+'/'+folder, cpus, project_state, kingdom, path_from, path_to)
                        )
                        t.start()
                        
                        res = JsonResponse({'detail': 'Procesando '+process_type}, status=status.HTTP_200_OK)
                    else:
                        res = JsonResponse({'detail': update_res}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    res = JsonResponse({'detail': 'No hay archivos '+process_type+' para procesar'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                res = JsonResponse({'detail': 'El estado del proyecto es: ' + project_state}, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res
