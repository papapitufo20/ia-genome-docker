from rest_framework.decorators import permission_classes
from rest_framework import permissions,status
from rest_framework.decorators import api_view
from django.http import HttpResponse, JsonResponse
from . import elastic_functions
from . import file_processing_functions
from django.conf import settings
import re
from .utils import STATES
import traceback

kingdoms = ["Bacteria", "Archaea", "Mitochondria", "Viruses"]



@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def force_state_chage_project(request,project_id):
    do_get_res = elastic_functions.do_get_project(project_id)
    if (do_get_res['found']):
        elastic_functions.do_update_project_state(project_id,"READY_FOR_STUDIES")
        res = JsonResponse({'detail': 'Estado Cambiado'}, status=status.HTTP_200_OK)
    else:
        res = JsonResponse({'detail': 'Proyecto no encontrado'}, status=status.HTTP_400_BAD_REQUEST)
    return res
    


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def create_project(request):
    try:
        name = request.data['name'].strip()
        name = re.sub('[^A-Za-z0-9 ]+', '', name)
        description = request.data['description'].strip()
        kingdom = request.data['kingdom'].strip()
        if (len(name) >= 6 and len(name) <= 200 and len(description) <= 400 and kingdom in kingdoms):
            do_res = elastic_functions.do_create_project(name,description,kingdom)
            if (do_res == 'created'):
                file_processing_functions.create_project_folders("-".join(name.split(" ")))
                res = JsonResponse({'detail': 'Proyecto creado'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': do_res}, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = JsonResponse({'detail': 'Revise los valores ingresados'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def edit_project(request, id):
    try:
        #name = request.data['name'].strip()
        #name = re.sub('[^A-Za-z0-9 ]+', '', name)
        description = request.data['description'].strip()
        kingdom = request.data['kingdom'].strip()
        if (len(description) <= 400 and kingdom in kingdoms):
            do_res = elastic_functions.do_edit_project(id,description,kingdom)
            if (do_res == 'updated' or do_res == 'noop'):
                res = JsonResponse({'detail': 'Proyecto editado'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': do_res}, status=status.HTTP_400_BAD_REQUEST)
        else:
            res = JsonResponse({'detail': 'Revise los valores ingresados'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def delete_project(request):
    try:
        id = request.data['id']
        do_get_res = elastic_functions.do_get_project(id)
        if (do_get_res['found']):
            name = do_get_res['_source']['name']
            do_res = elastic_functions.do_delete_project(id)
            if (do_res == 'deleted'):
                file_processing_functions.remove_project("-".join(name.split(" ")))
                res = JsonResponse({'detail': 'Proyecto eliminado'}, status=status.HTTP_200_OK)
            else:
                res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)  
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    return res

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_project(request, id):
    try:
        do_res = elastic_functions.do_get_project(id)
        if (do_res['found']):
            do_res = add_count_to_hit(do_res)

            if 'studies' in do_res['_source']:
                studies = do_res['_source']['studies']
                for study in studies:
                    study["genomes_count"] = elastic_functions.get_genomes_label_information(do_res["_source"]["genomes_index"], study['id'])

            res = JsonResponse({'result': do_res}, status=status.HTTP_200_OK)
        else:
            res = JsonResponse({'detail': do_res}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        traceback.print_exc()
        res = JsonResponse({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)            
    return res

def add_count_to_hit(hit):
    if "folder" in hit["_source"]:
        folder = hit["_source"]["folder"]
        fastq_count = file_processing_functions.remaining_fastq(settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fastq')+file_processing_functions.remaining_fastq(settings.GENOMES_PATH+'/'+folder+'/genomes')
        fasta_count = file_processing_functions.remaining_fasta(settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/fasta')+file_processing_functions.remaining_fasta(settings.GENOMES_PATH+'/'+folder+'/genomes')
        gbf_count = file_processing_functions.remaining_gbf(settings.GENOMES_PATH+'/'+folder+'/genomes/tmp/gbf')+file_processing_functions.remaining_gbf(settings.GENOMES_PATH+'/'+folder+'/genomes')
        hit["_source"]["fastq_count"] = fastq_count
        hit["_source"]["fasta_count"] = fasta_count
        hit["_source"]["gbf_count"] = gbf_count
        hit["_source"]["genomes_count"] = elastic_functions.get_index_count(hit["_source"]["genomes_index"])
        hit["_source"]["genes_count"] = elastic_functions.get_index_count(hit["_source"]["genes_index"])
        hit["_source"]["alleles_count"] = elastic_functions.get_index_count(hit["_source"]["alleles_index"])
        hit["_source"]["genomes_with_pertenence_count"] = elastic_functions.get_genomes_with_pertenence_count(hit["_source"]["genomes_index"])
    return hit

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_projects(request):
    elastic_functions.refresh_indices()
    res = elastic_functions.do_get_projects()
    if isinstance(res, dict):
        res["hits"]["hits"] = list(map(lambda hit: add_count_to_hit(hit), res["hits"]["hits"]))
    return JsonResponse({'results': res}, status=status.HTTP_200_OK)

@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_project_states(request):
    return JsonResponse(STATES, status=status.HTTP_200_OK, safe=False)