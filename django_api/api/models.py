from django.db import models
from django.contrib.auth.models import AbstractUser, Group

# Create your models here.

class User(AbstractUser):
    is_manager = models.BooleanField(default=False)