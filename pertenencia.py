from datetime import datetime
from elasticsearch import Elasticsearch
import os

es = Elasticsearch(
    ['192.168.0.198'], #'localhost'
    port=9200,
    http_auth=('elastic', 'matelisto'),
)

genomes_index = 'genomes-index'
genes_index = 'genes-index'

def add_gene_table():
    print("buscanding genomas")
    res_genomas = es.search(index=genomes_index, body={"query": {"match_all": {}}, "size": 3200})
    print("buscanding genes")
    res_genes = es.search(index=genes_index, body={"query": {"match_all": {}}, "size": 50000},scroll='1m')
    dict_genes = dict()
    print("dict de genes")
    for hit in res_genes['hits']['hits']:
        dict_genes[hit["_source"]["translation"]]  = hit["_source"]["gen_id"]
    print("armando array para cada genoma")
    for hit in res_genomas['hits']['hits']:
        pertenencia = [0] * len(dict_genes)
        for gen in hit["_source"]["genes"]:
            pertenencia[dict_genes[gen["translation"]]] = 1
        
        u_response = es.update(index=genomes_index,doc_type='_doc',id=hit["_id"],
                    body={"doc": {"gen_pertenence": pertenencia }})
        print(u_response)
        #print(hit["_source"]["name"]," - ",pertenencia)

#def add_single_gen(gen, count):
#    res = es.index(index=genes_index, body=gen)
#    print(res['result'] + ": " + str(count))

def add_genes(genes):
    count = 0
    for translation in genes:
        genes[translation]["gen_id"] = count
        res = es.index(index=genes_index, body=genes[translation])
        print(res['result'] + ": " + str(count+1))
        count += 1

def insert_unique_genes():
    #es.indices.refresh(index=genomes_index)
    print("buscanding")
    res = es.search(index=genomes_index, body={"query": {"match_all": {}}, "size": 3200})
    
    print("Got %d Hits:" % res['hits']['total']['value'])
    
    all_genes = dict()

    for hit in res['hits']['hits']:
        #print(hit["_source"])
        print("GENOMA**********************")
        for gen in hit["_source"]["genes"]:
            print("gen: " + gen["locus_tag"])
            if gen["translation"] not in all_genes:
                all_genes[gen["translation"]] = dict()
                all_genes[gen["translation"]]["gene"] = gen["gene"]
                all_genes[gen["translation"]]["product"] = gen["product"]
                all_genes[gen["translation"]]["translation"] = gen["translation"]
                all_genes[gen["translation"]]["ocurrences"] = 1
            else:
                all_genes[gen["translation"]]["ocurrences"] = all_genes[gen["translation"]]["ocurrences"] + 1
            #all_genes[gen["locus_tag"]] = gen["translation"]

    #print("Cant de locus_tag: " + str(len(all_genes)))
    #print(all_genes)
    print("Cant de genes sin repetir: " + str(len(all_genes)))
    add_genes(all_genes)

#insert_unique_genes()
#add_gene_table()

#3113 genomas
#5174124 genes
#49267 genes sin repetir
#genes desde 30 char hasta 2900 char
#genomas con 4 genes hasta 3000 genes
#algunos genomas tienen un gen mas de una vez