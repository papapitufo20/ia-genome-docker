#!/usr/bin/env python
# coding: utf-8

# In[26]:


from elasticsearch import Elasticsearch

es = Elasticsearch(
    ['192.168.0.68'], #'localhost'
    port=9200,
    http_auth=('elastic', 'matelisto'),
    #scheme="https",
    #port=443,
    #ssl_context=context,
)

genomes_index = 'genomes-index-name-sort'

def prepare_genomes_as_X_Y():
    es.indices.refresh(index=genomes_index)
    
    all_loaded = False
    page = 1
    page_size = 500
    
    _x = list()
    _y = list()
    
    while not all_loaded:
        from_value = ((page - 1) * page_size)
        print("pido pagina: " + str(page) + " con from: " + str(from_value))
        res = es.search(index=genomes_index, body={
            "query": {"match_all": {}}, 
            "sort": [{"name.raw": {"order": "asc"}}], 
            "size": page_size,
            "from": from_value
        })
        for hit in res['hits']['hits']:
            #raw = dict()
            #raw["name"] = hit["_source"]["name"]
            #raw["gen_pertenence"] = hit["_source"]["gen_pertenence"]
            #_x.append(raw)
            _x.append(hit["_source"]["gen_pertenence"])
            _y.append(1 if hit["_source"]["INV"] == "Inv" else 0)
        
        all_loaded = len(res['hits']['hits']) < page_size
        page += 1
    
    return _x, _y
        
X, Y = prepare_genomes_as_X_Y()

print("X length: " + str(len(X)))
#for item in X:
#    print(item["name"] + " with: " + str(len(item["gen_pertenence"])))
print("Y length: " + str(len(Y)))


# In[28]:


print(len(list(filter(lambda n: n == 0,Y[1500:]))))


# In[32]:


import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
import tensorflow.keras as kr

from IPython.core.display import display, HTML

tf.compat.v1.disable_eager_execution()

print("Variables...")

lr = 0.0005           # learning rate
#nn = [256, 64, 32, 16, 8, 1]  # número de neuronas por capa.
nn = [49267, 8192, 4096, 1024, 256, 64, 16, 1]  # número de neuronas por capa.

print("Agregando capas...")

# Creamos el objeto que contendrá a nuestra red neuronal, como
# secuencia de capas.
model = kr.Sequential()

# Añadimos la capa 1
l1 = model.add(kr.layers.Dense(nn[1], activation='relu'))

# Añadimos la capa 2
l2 = model.add(kr.layers.Dense(nn[2], activation='relu'))

# Añadimos la capa 3
l3 = model.add(kr.layers.Dense(nn[3], activation='relu'))

# Añadimos la capa 3
l4 = model.add(kr.layers.Dense(nn[4], activation='relu'))

# Añadimos la capa 3
l5 = model.add(kr.layers.Dense(nn[5], activation='relu'))

# Añadimos la capa 3
l6 = model.add(kr.layers.Dense(nn[6], activation='relu'))

# Añadimos la capa 5
l7 = model.add(kr.layers.Dense(nn[7], activation='sigmoid'))

print("A compilar...")
# Compilamos el modelo, definiendo la función de coste y el optimizador.
model.compile(loss='mse', optimizer=kr.optimizers.SGD(learning_rate=lr), metrics=['acc'])

print("Termine")


# In[45]:


# Y entrenamos al modelo. Los callbacks 
#print(X[0])
#X64 = list(map(lambda item: item[0:64], X))
#import numpy as np
#xnp = np.ndarray((2,), buffer=np.array([1,2,3]),
#ynp = Y

#print(X64[0])

#import tensorflow.keras as kr
#from tensorflow.keras import backend as K
#K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=2, inter_op_parallelism_threads=2)))

import numpy as np

print("A entrenar...")
#with tf.device("/device:GPU:0"):
model.fit(np.array(X[:1350]), np.array(Y[:1350]), epochs=1000)

print("Termino de entrenar")


# In[40]:


model.save("supertrooper")


# In[44]:


print("Evaluate on test data")
results = model.evaluate(np.array(X[1350:1500]), np.array(Y[1350:1500]))
print("Mixed test loss, test acc:", results)
results = model.evaluate(np.array(X[1500:]), np.array(Y[1500:]))
print("All invasive test loss, test acc:", results)

