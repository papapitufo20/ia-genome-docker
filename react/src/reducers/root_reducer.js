import * as ACTION_TYPES from '../actions/action_types';

const initialState = {
    loading: true,
    is_authenticated: false,
    login_error: '',
    user: null,
    users: [],
    users_error: '',
    notifications: [],
    main_content_loading: false,
    main_content_error: '',
    main_content_success: '',
    project_states: [],
    study_states: [],
    cpus: 0,
    default_rna: {}
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.LOGIN_REQUEST:
            return {
                ...state,
                loading: true
            }
        case ACTION_TYPES.LOGIN_SUCCESS:
            return {
                ...initialState,
                is_authenticated: true,
                loading: false,
                user: action.payload,
                login_error: '',
            }
        case ACTION_TYPES.LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
                login_error: action.payload
            }
        case ACTION_TYPES.LOGOUT:
            return {
                ...state,
                loading: false,
                is_authenticated: false,
                user: null,
                login_error: action.payload,
            }
        case ACTION_TYPES.MAIN_CONTENT_LOADING:
            return {
                ...state,
                main_content_loading: action.payload
            }
        case ACTION_TYPES.MAIN_CONTENT_ERROR:
            return {
                ...state,
                main_content_error: action.payload,
                main_content_success: ''
            }
        case ACTION_TYPES.MAIN_CONTENT_SUCCESS:
            return {
                ...state,
                main_content_error: '',
                main_content_success: action.payload
            }
        case ACTION_TYPES.SET_USERS:
            return {
                ...state,
                users: action.payload,
                users_error: ''
            }
        case ACTION_TYPES.GET_USERS_ERROR:
            return {
                ...state,
                users: [],
                users_error: action.payload
            }
        // NOTIFICATIONS
        case ACTION_TYPES.SET_NOTIFICATIONS:
            return {
                ...state,
                notifications: action.payload
            }
        case ACTION_TYPES.APPEND_NOTIFICATION:
            return {
                ...state,
                notifications: [action.payload, ...state.notifications]
            }
        //PROJECTS
        case ACTION_TYPES.SET_PROJECT_STATES:
            return {
                ...state,
                project_states: action.payload
            }
        //STUDIES
        case ACTION_TYPES.SET_STUDY_STATES:
            return {
                ...state,
                study_states: action.payload
            }
        case ACTION_TYPES.SET_CPUS:
            return {
                ...state,
                cpus: action.payload
            }
        case ACTION_TYPES.SET_DEFAULT_RNA:
            return {
                ...state,
                default_rna: action.payload
            }
        default:
            return state;
    }
}

export default rootReducer;