import React, { useEffect } from 'react';
import Main from '../main/main';

function PageNotFound() {

    useEffect(() => {
		document.title = "Página no encontrada";
	}, []);

    return (
        <Main title="Página no encontrada."></Main>
    );
}
export default PageNotFound;