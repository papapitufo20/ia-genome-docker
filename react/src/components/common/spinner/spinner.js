import React from 'react';
import './spinner.css';

function Spinner(props){
	return (
		<div className={"spinner col-sm-4 offset-sm-4 " + props.className}>
			<div className="loader"></div>
		</div>        
	);
}

export default Spinner;