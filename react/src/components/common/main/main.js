import React from 'react';
import './main.css';
import { useSelector } from 'react-redux';
import Spinner from '../spinner/spinner';

function Main(props){
    
	const users_error = useSelector(state => state.users_error);
	const main_content_error = useSelector(state => state.main_content_error);
	const main_content_success = useSelector(state => state.main_content_success);
	const main_content_loading = useSelector(state => state.main_content_loading);
    
	return (
		<div className="main-content">
            <div className="col-sm-12 header row">
                <div className="col-sm-4 vertical-align-center">
                    <h2 className="one-five-rem">{props.title}</h2>
                </div>
                {
                    users_error !== '' ?
                        <div className="col-sm-4 alert alert-danger">
                            <span>Error: {users_error}</span>
                        </div>
                    : ''
                }
                {
                    main_content_error !== '' ?
                        <div className="col-sm-4 alert alert-danger one-rem">
                            <span>{main_content_error}</span>
                        </div>
                    : ''
                }
                {
                    main_content_success !== '' ?
                        <div className="col-sm-4 alert alert-success">
                            <span>{main_content_success}</span>
                        </div>
                    : ''
                }
                <div className={"col-sm-4" + ((users_error !== '' || main_content_error !== '' || main_content_success !== '') ? "" : " offset-sm-4")}>
                    {props.render_right}
                </div>
            </div>
            {
                main_content_loading ?
                    <Spinner></Spinner>
                : 
                    <div className={"col-sm-12 " + props.className}>
                        {props.children}
                    </div>
            }
		</div>
	);
}

export default Main;