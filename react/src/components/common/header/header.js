import React from 'react';
import './header.css';
import * as AUTH_ACTIONS from '../../../actions/auth_actions';
import { useSelector, useDispatch } from 'react-redux';
import NotificationsPopup from '../notifications-popup/notifications-popup';
import { Link } from 'react-router-dom';

function Header(){

    const dispatch = useDispatch();
    const user = useSelector(state => state.user);

	return (
		<header>
            <nav className="navbar navbar-top navbar-default" role="navigation">
                <ul className="nav navbar-nav navbar-left ">
                    <li data-seccion="Proyectos"><a href="/projects">Proyectos</a></li>
                </ul>
                <ul className="nav navbar-nav navbar-right ">
                    <li data-seccion="User"><a className="username">{"@"+user}</a></li>
                    {/*<li data-seccion="Notificaciones"><a><NotificationsPopup></NotificationsPopup></a></li>*/}
                    <li data-seccion="Inicio"><a href="/">Inicio</a></li>
                    <li data-seccion="Identidad"><a onClick={()=>dispatch(AUTH_ACTIONS.logout(""))}>Cerrar sesion</a></li>
                </ul>
            </nav>
        </header>
	);
}

export default Header;
