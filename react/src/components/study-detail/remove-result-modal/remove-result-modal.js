import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { CONFIG } from '../../../config';

import Modal from 'react-modal';
import { customConfirmationModalStyles } from '../../../common';

Modal.setAppElement('#root');

function RemoveResultModal(props){

    const [error, setError] = useState('');
	const [success, setSuccess] = useState('');
    const [hideActions, setHideActions] = useState(true);

    const handleRemove = () => {
		setSuccess('');
		setError('');
        props.setReloadResults(true);
		axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+props.removeModalData.project_id+'/study/'+props.removeModalData.study_id+'/delete_csv', {
				filename: props.removeModalData.filename,
                
			},
			{
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				setSuccess('Eliminado correctamente');
				setError('');
                setHideActions(true);
            })
            .catch(error => {
                console.log(error);
                setSuccess('');
				setError('Error al eliminar');
                setHideActions(false);
            });
	}

    useEffect(() => {
        if (props.removeModalOpened){
            setSuccess('');
		    setError('');
		    setHideActions(false);
        }
    }, [props.removeModalOpened]);

    return (
        <Modal
            isOpen={props.removeModalOpened}
            style={customConfirmationModalStyles}
            contentLabel="Study modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }
                {
                    success !== '' ?
                        <div className="col-sm-12 alert alert-success">
                                <span>{success}</span>
                        </div>
                    //else
                    : ''
                }

                <h2 className="no-border">{ "Eliminar resultado '" + props.removeModalData.filename + "'?" }</h2>

                {
                    !hideActions ?
                        <div className="col-sm-12 mt-3">
                            <button type="button" className="btn btn-success mr-2" onClick={() => handleRemove()}>
                                Eliminar <i className="fa fa-check ml-2"></i>
                            </button>
                            <button type="button" className="btn btn-danger" onClick={props.closeRemoveModal}>
                                Cancelar <i className="fa fa-times ml-2"></i>
                            </button>
                        </div>
                    //else
                    : 
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-primary" onClick={props.closeRemoveModal}>Cerrar</button>
                        </div>
                }

            </div>
        </Modal>
    )
}

export default RemoveResultModal;