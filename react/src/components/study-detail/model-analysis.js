import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import { CONFIG } from '../../config';
import { useDispatch } from 'react-redux';
import * as GENERIC_ACTIONS from '../../actions/generic_actions';
import Spinner from '../common/spinner/spinner';

function ModelAnalysis(props){

    const dispatch = useDispatch();

    const [topGenes, setTopGenes] = useState([]);
    const svgContainer = useRef(null);
    const [refreshModelAnalysis, setRefreshModelAnalysis] = useState(false);
    const [multifastaMuscleTreeBtnClass, setMultifastaMuscleTreeBtnClass] = useState("");

    const [clickedXLSXgenId, setClickedXLSXgenId] = useState(null);

    const getGenesInfo = (genes_id) => {
        axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/get_genes_info',
            {
                genes_id: genes_id
            },
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                setTopGenes(response.data.genes_info);
            })
            .catch(error => {
                console.log(error);
            });
    }

    const getStudyModelAnalysis = () => {
        axios.get(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/get_model_analysis',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                var parser = new DOMParser();
                var doc = parser.parseFromString(response.data, "image/svg+xml");

                doc.rootElement.setAttribute("width", "100%");
                if (svgContainer && svgContainer.current){
                    Array.from(svgContainer.current.childNodes).map(x => svgContainer.current.removeChild(x));

                    let viewBox = doc.rootElement.getAttribute("viewBox");
                    viewBox = viewBox.split(" ");
                    viewBox[1] = "100";
                    viewBox = viewBox.join(" ");
                    doc.rootElement.setAttribute("viewBox", viewBox);

                    svgContainer.current.appendChild(doc.rootElement);
                    //let nodes = getAllCommentNodes(svgContainer.current.childNodes[0]);

                    let nodes = svgContainer.current.childNodes[0].querySelectorAll('[id^="ytick_"]');

                    let genes_id = [];
                    try {
                        nodes.forEach(ytick_ => {
                            let text_ = ytick_.querySelector('[id^="text_"]');
                            if (text_){
                                text_.childNodes.forEach(x => {
                                    if (x.nodeType === 8){ //8 is comment, searching node like <!-- Gen XXX -->
                                        let comment_data = x.data.trim().split(": ");
                                        if (comment_data && comment_data.length > 1){
                                            genes_id.push(comment_data[1]);
                                        }
                                    }
                                });
                            }
                        });
                        genes_id = genes_id.reverse();
                        getGenesInfo(genes_id);
                        //setTopGenes(genes_id);
                    }
                    catch (exception){
                        console.log("Error parsing svg");
                        console.log(exception);
                    }
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    const handleGetFeatureImportance = () => {
        axios.get(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/analyze_model',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                window.scrollTo(0,0);
                props.getStudy(true);
                dispatch(GENERIC_ACTIONS.mainContentError(''));
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentError("Error al obtener importancia de genes"));
            });
    }

    function download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
      
        element.style.display = 'none';
        document.body.appendChild(element);
      
        element.click();
      
        document.body.removeChild(element);
    }

    const handleDownloadMultiFasta = (gene, projectName, studyName) => {
        if (props.study.state === "READY_FOR_QUESTIONS"){
            let text = '';
            gene.translation.forEach((t, index) => text = text + '>' +  projectName + '/' + studyName + '/' + gene.product + '/' + gene.id + '/' + index + '\n' + t + '\n');
            let filename = projectName + '_' + studyName + '_' + gene.product + '_' + gene.id + '.fasta';
            download(filename, text);
        }
    }

    const handleMultipleSequenceAlignment = (gene, projectName, studyName, generateTree) => {
        if (props.study.state === "READY_FOR_QUESTIONS"){
            axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/multiple_sequence_alignment',
                {
                    gene: gene,
                    projectName: projectName,
                    studyName: studyName,
                    generateTree: generateTree
                },
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    }
                })
                .then(response => {
                    window.scrollTo(0,0);
                    props.getStudy(true);
                    dispatch(GENERIC_ACTIONS.mainContentError(''));
                })
                .catch(error => {
                    console.log(error);
                    let err_msg = generateTree ? "Error al generar IQTREE" : "Error al alinear multifasta";
                    dispatch(GENERIC_ACTIONS.mainContentError(err_msg));
                });
        }
    }

    const handleDownloadMultipleSequenceAlignment = (filename) => {
        if (props.study.state === "READY_FOR_QUESTIONS"){
            axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/download_multiple_sequence_alignment',
                {
                    filename: filename,
                },
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    }
                })
                .then(response => {
                    download(filename, response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    const handleDownloadGenInfo = (gene, projectName, studyName) => {
        if (clickedXLSXgenId === null){
            setClickedXLSXgenId(gene.id);
            axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+props.project_id+'/study/'+props.study_id+'/download_gen_info_xlsx',
                {
                    gene: gene,
                    projectName: projectName,
                    studyName: studyName,
                },
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    },
                    responseType: 'arraybuffer'
                })
                .then(response => {
                    const url = window.URL.createObjectURL(new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}));
                    const link = document.createElement('a');
                    link.href = url;
                    let genDesc = gene.gene ? gene.gene : gene.product;
                    link.setAttribute('download', projectName+'_'+studyName+'_'+genDesc+'.xlsx');
                    document.body.appendChild(link);
                    link.click();
                    setClickedXLSXgenId(null);
                })
                .catch(error => {
                    console.log(error);
                    dispatch(GENERIC_ACTIONS.mainContentError("Error: " + error.response.data.detail));
                    setClickedXLSXgenId(null);
                });
        }
    }

    const showGenInfo = (e, g) => {
        let elements = document.getElementsByClassName("gen-info-container " + g);
        if (elements && elements.length > 0){
            let els = elements[0].getElementsByClassName("more-info");
            if (els && els.length > 0){
                if (e.target.classList.contains("fa-chevron-down")){
                    els[0].classList.add("show");
                    e.target.classList.remove("fa-chevron-down");
                    e.target.classList.add("fa-chevron-up");
                }
                else {
                    els[0].classList.remove("show");
                    e.target.classList.remove("fa-chevron-up");
                    e.target.classList.add("fa-chevron-down");
                }
            }
        }
    }

    useEffect(() => {
        if (props.study && (props.study.state === 'ALIGNING_MULTIFASTA' || props.study.state === 'GENERATING_IQTREE')){
            setMultifastaMuscleTreeBtnClass("btn-non-action not-allowed");
        }
        else {
            setMultifastaMuscleTreeBtnClass("");
        }

        if (props.study && props.study.state === 'ANALYZING_MODEL'){
            setRefreshModelAnalysis(true);
        }
        else if (refreshModelAnalysis) {
            setRefreshModelAnalysis(false);
            getStudyModelAnalysis();
        }
    }, [props.study]);

    useEffect(() => {
        getStudyModelAnalysis();
	}, []);


    /* EXAMPLES
     * "streptococcus_pyogenes_invasiv_Exotoxin_type_C_2731.aln"
     * "streptococcus_pyogenes_invasiv_Exotoxin_type_C_2731.aln.treefile"
    */
    const getMuscleAlignmentFilenames = (muscle_iqtree_files, gene_id) => {
        let alignmentFilename = null;
        let treeFilename = null;
        muscle_iqtree_files.forEach(f => {
            let splitted = f.split("_");
            if (splitted && splitted.length > 0){
                let last = splitted[splitted.length-1];
                let splitted2 = last.split(".");
                if (splitted2 && splitted2.length > 0){
                    let id = splitted2[0];
                    if (id == gene_id){
                        let last2 = splitted2[splitted2.length-1];
                        if (last2 == "aln"){
                            alignmentFilename = f;
                        }
                        else if (last2 == "treefile"){
                            treeFilename = f;
                        }
                    }
                }
            }
        });
        return [alignmentFilename, treeFilename];
    }

    return (
        <div>
            <div className="row col-sm-12 pt-3 pb-2 mb-3 border-bottom">
                <div className="col-sm-8">
                    <h3 className="one-five-rem">Análisis del modelo</h3>
                </div>
                <div className="col-sm-4">
                {
                    props.study.state === "READY_FOR_QUESTIONS" ?
                        <div className="row flex-end">
                            <button className="btn btn-primary bold col-sm-5" onClick={handleGetFeatureImportance}>Analizar modelo</button>
                        </div>
                    : ''
                }
                </div>
            </div>
            
            <div className="row col-sm-12 mb-5">
                <div className="col-sm-6" ref={svgContainer}>
                    
                </div>
                <div className="col-sm-6 pl-3 pr-3">
                    {
                        topGenes ?
                            topGenes.map(g => {

                                let [alignmentFilename, treeFilename] = getMuscleAlignmentFilenames(props.study.muscle_iqtree_files, g.id);

                                return <div className={"row col-sm-12 gen-info-container pb-3 pt-3 "+g.id}>
                                    
                                    <div className="col-sm-11">
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Gen id:</label>{g.id} -
                                            <label className="bold mr-1 ml-1">Gen:</label>
                                                {
                                                    props.study.studyType === "Alleles" && g.translation && g.translation.length === 1 ?
                                                        "-"
                                                    : g.gene ? g.gene : "-"}
                                        </div>
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Product:</label>{g.product}
                                        </div>
                                    </div>
                                    <div className="col-sm-1 collapse-button-container text-align-right">
                                        <i className="fa fa-chevron-down" onClick={(e) => showGenInfo(e, g.id)}></i>
                                    </div>
                                    <div className="more-info">
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Presente en:</label>
                                            {g.total_ocurrences} genomas
                                        </div>
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Presente en:</label>
                                            {g.ocurrences_with_1} genomas etiquetados con SI
                                        </div>
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Presente en:</label>
                                            {g.ocurrences_with_0} genomas etiquetados con NO
                                        </div>
                                        <div className="col-sm-12">
                                            <label className="bold mr-1">Translations:</label>
                                            {
                                                typeof g.translation === "string" ?
                                                    <p className="word-break-all">{g.translation}</p>
                                                : g.translation && g.translation.length > 0 ?
                                                    g.translation.map(t => {
                                                        return <p className="word-break-all">{t}</p>
                                                    })
                                                : '-'
                                            }
                                        </div>
                                    </div>
                                    {
                                        props.study.studyType === "Alleles" ?
                                            <div className="col-sm-12">
                                                
                                                <button 
                                                    className={"btn btn-primary "+multifastaMuscleTreeBtnClass+" bold col-sm-2 mr-2"} 
                                                    onClick={() => handleDownloadMultiFasta(g, props.project.name, props.study.name)}>
                                                        Multifasta
                                                </button>
                                                
                                                <button 
                                                    className={"btn btn-primary "+(alignmentFilename ? "btn-success " : "")+multifastaMuscleTreeBtnClass+" bold col-sm-3 mr-2"} 
                                                    onClick= {() => {
                                                        if (!alignmentFilename){
                                                            handleMultipleSequenceAlignment(g, props.project.name, props.study.name, false)
                                                        }
                                                        else {
                                                            handleDownloadMultipleSequenceAlignment(alignmentFilename)
                                                        }
                                                    }}>
                                                        {alignmentFilename ? "Descargar alineamiento" : "Alinear multifasta"}
                                                </button>
                                                
                                                <button 
                                                    className={"btn btn-primary "+(treeFilename ? "btn-success " : "")+multifastaMuscleTreeBtnClass+" bold col-sm-3 mr-2"} 
                                                    onClick= {() => {
                                                        if (!treeFilename){
                                                            handleMultipleSequenceAlignment(g, props.project.name, props.study.name, true)
                                                        }
                                                        else {
                                                            handleDownloadMultipleSequenceAlignment(treeFilename)
                                                        }
                                                    }}>
                                                        {treeFilename ? "Descargar IQTREE" : "Generar IQTREE"}
                                                </button>

                                                <button
                                                    id={g.id}
                                                    className={"btn btn-primary "+multifastaMuscleTreeBtnClass+" bold col-sm-3" +(clickedXLSXgenId === g.id ? " button-spinner" : "")}
                                                    onClick= {(e) => {
                                                        handleDownloadGenInfo(g, props.project.name, props.study.name)
                                                    }}>
                                                        {
                                                            clickedXLSXgenId !== g.id ?
                                                                'Detalle (xlsx)'
                                                            :
                                                                <div className="row">
                                                                    <div className="pt-1">Procesando...</div>
                                                                    <Spinner></Spinner>
                                                                </div>
                                                        }
                                                </button>
                                            </div>
                                        : ''
                                    }
                                </div>
                            })
                        : ''
                    }
                </div>
            </div>
        </div>
    )
}

export default ModelAnalysis;