import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from "react-router-dom";
import { tableTheme, paginationStyles, paginationOptions } from '../../common';
import DataTable, { createTheme } from 'react-data-table-component';

import axios from 'axios';
import { CONFIG } from '../../config';
import * as GENERIC_ACTIONS from '../../actions/generic_actions';

import Spinner from '../common/spinner/spinner';
import Main from '../common/main/main';

import { CSVLink } from 'react-csv';
import CSVReader from 'react-csv-reader';
import TrainModal from './train-modal';
import EvaluateModal from './evaluate-modal';
import { rna_arch_to_display } from '../../common';
import RemoveResultModal from './remove-result-modal/remove-result-modal';
import { MDBDropdown, MDBDropdownMenu, MDBDropdownToggle, MDBDropdownItem, MDBDropdownLink } from 'mdb-react-ui-kit';
import ForceStateChangeStudyModal from '../studies/force-state-change-study-modal/force-state-change-study-modal';

import ModelAnalysis from './model-analysis';

const buildColumns = (handleDownloadCsv, handleRemoveCsv) => {
	return [
		{
			name: 'Nombre',
			selector: 'name',
			sortable: true,
			width: '90%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<a className="custom-link white-font" href='#'>{row.name}</a>
				</div>
		},
        {
			name: 'Acciones',
			selector: '',
			sortable: false,
			width: '10%',
			style: {
				height: '80px'
			},
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<button className="btn btn-primary btn-circle mr-2" onClick={() => handleDownloadCsv(row)}><i className="fa fa-download"></i></button>
					<button className="btn btn-danger btn-circle mr-2" onClick={() => handleRemoveCsv(row)}><i className="fa fa-trash"></i></button>
				</div>
		}
	]};


function StudyDetail(props){

    let project_id = null;
    let study_id = null;
    let pathname = useLocation().pathname;
    let splitted = pathname ? pathname.split("/") : [];
    if (splitted && splitted.length == 5){
        project_id = splitted[2];
        study_id = splitted[4];
    }

    const dispatch = useDispatch();
    const [project, setProject] = useState(null);
    const [study, setStudy] = useState(null);
    const [rows, setRows] = useState(null);
    const [ForceStateChangeModalOpened, setForceStateChangeModalOpened] = useState(false);

    const studyStates = useSelector(state => state.study_states);

    const getStudy = (hideSpinner) => {
        if (!hideSpinner){
            dispatch(GENERIC_ACTIONS.mainContentLoading(true));
        }
        axios.get(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id,
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                setProject(response.data.result.project);
                setStudy(response.data.result.study);
                setRows(response.data.result.study.results.map(x => {return {name:x}}))		
                
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError(''));
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError("Error al obtener estudio"));
            });
    }

    useEffect(() => {
        document.title = "Estudio";
        getStudy();
        let refreshTimeout = setInterval(() => {
			getStudy(true);
		}, CONFIG.REFRESH_TIMEOUT);
		return () => { clearInterval(refreshTimeout); }
	}, []);

    const [dataForDownload, setDataForDownload] = useState([]);
    const [downloadReady, setDownloadReady] = useState(false);
    const csvLink = useRef(null);
    const [downloadingUnlabeledCSV, setDownloadingUnlabeledCSV] = useState(false);
    const [trainModalOpened, setTrainModalOpened] = useState(false);
	const [trainModalData, setTrainModalData] = useState(null);
    const[csvFilename, setCsvFilename] = useState('');

    useEffect(() => {
        if (dataForDownload && dataForDownload.length > 0){
            setDownloadReady(true);
        }
    }, [dataForDownload]);

    useEffect(() => {
        if (csvLink && csvLink.current && downloadReady) {
            setDownloadingUnlabeledCSV(false);
            dispatch(GENERIC_ACTIONS.mainContentError(''));
            csvLink.current.link.click();
            setDownloadReady(false);
        }
    }, [downloadReady]);

    const handleDownloadUnlabeled = () => {
        setDataForDownload([]);
        setDownloadingUnlabeledCSV(true);
        axios.get(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id+'/download_unlabeled_genomes',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                setCsvFilename(project.name+'_'+study.name+'.csv')
                setDataForDownload(response.data);
            })
            .catch(error => {
                setDownloadingUnlabeledCSV(false);
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentError("Error: " + error.response.data.detail));
            });
    }

    const handleLoadLabels = () => {
        let csvReader = document.getElementById("csv-upload");
        if (csvReader) {
            csvReader.click();
        }
    }

    //send csv info (genomes names with label for RNA)
    const handleOnFileLoaded = (data, fileInfo, originalFile) => {
        dispatch(GENERIC_ACTIONS.mainContentLoading(true));
        axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id+'/set_label_to_genomes',
            {
                genome_label_pair: data.slice(1) //0 has columns names
            },
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                getStudy();
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError(''));
                setTrainModalOpened(false);
            })
            .catch(error => {
                setTrainModalOpened(false);
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError("Error al obtener estudio"));
            });
    }

    const handleOnFileLoadError = () => {
        dispatch(GENERIC_ACTIONS.mainContentError("Error al cargar archivo"));
    }

    const handleTrain = () => {
        setTrainModalData({
            callback: (cpus, epochs, lr, rna, percentageOfTrainingData) => fitRNA("train", "entrenar", cpus, epochs, lr, rna, percentageOfTrainingData), 
            action: 'Entrenar', 
            warning: "El entrenamiento inicia el aprendizaje de un nuevo modelo, utilizando todos los genomas etiquetados para este estudio.", 
            optimize: false,
            rna_arch: study.rna_arch,
            learning_rate: study.learning_rate
        });
		setTrainModalOpened(true);
    }

    const handleOptimize = () => {
        setTrainModalData({
            callback: (cpus, epochs, lr, rna, percentageOfTrainingData) => fitRNA("optimize", "optimizar", cpus, epochs, lr, rna, percentageOfTrainingData), 
            action: 'Optimizar', 
            warning: "La optimización continua con el aprendizaje del modelo ya creado, utilizando todos los genomas etiquetados para este estudio.", 
            optimize: true,
            rna_arch: study.rna_arch,
            learning_rate: study.learning_rate
        });
		setTrainModalOpened(true);
    }

    const fitRNA = (train_type, message, cpus, epochs, lr, rna, percentageOfTrainingData) => {
        axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id+'/'+train_type,
            {
                cpus: cpus,
                epochs: epochs,
                learning_rate: lr,
                rna_arch: rna,
                percentage_of_training_data: percentageOfTrainingData
            },
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                getStudy(true);
                dispatch(GENERIC_ACTIONS.mainContentError(''));
                setTrainModalOpened(false);
            })
            .catch(error => {
                setTrainModalOpened(false);
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentError("Error: " + error.response.data.detail));
            });
    }

    const [evaluateModalOpened, setEvaluateModalOpened] = useState(false);

    const evaluate = (cpus) => {
        axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id+'/evaluate',
            {
                cpus: cpus
            },
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                getStudy(true);
                dispatch(GENERIC_ACTIONS.mainContentError(''));
                setEvaluateModalOpened(false);
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentError("Error: " + error.response.data.detail));
                setEvaluateModalOpened(false);
            });
    }

    const handleDownloadCsv = (row) => {
        setDataForDownload([]);
        axios.post(CONFIG.API_SERVER_ADDRESS+'/project/'+project_id+'/study/'+study_id+'/download_csv',
            {
                name:row.name
            },
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                setCsvFilename(row.name);
                setDataForDownload(response.data);
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentError("Error: " + error.response.data.detail));
            });
	};


    const [removeModalOpened, setRemoveModalOpened] = useState(false);
    const [removeModalData, setRemoveModalData] = useState({
		filename: "",
	});
    const [reloadResults, setReloadResults] = useState(false);

    const handleRemoveCsv = (row) => {
		setRemoveModalData({
            project_id: project_id,
            study_id: study_id,
			filename: row.name,
		});
		setRemoveModalOpened(true);
	}
    const closeRemoveModal = () => {
		setRemoveModalOpened(false);
		if (reloadResults){
			getStudy();
		}
		setReloadResults(false);
	};

    const columns = buildColumns(handleDownloadCsv, handleRemoveCsv);

    return (
        <Main 
            title={
                <span>
                    Estudio: {project ? 
                        <a 
                            className="custom-link white-font" 
                            href={"/project/"+project.id}>{project.name}
                        </a> : ""
                    } / {study ? study.name : ""}
                </span> 
            }
            className="projects-container pt-3" 
            render_right={
                <div className="row flex-end">    
                    <div className="col-sm-8">
                        {
                            study === null ? '' :
                            project && project.state === "READY_FOR_STUDIES" ?
                                study.state === "INITIAL" || study.state === "READY_FOR_TRAINING" || study.state === "READY_FOR_QUESTIONS" ?
                                    <div className="row flex-end">
                                        <button className="btn btn-primary bold col-sm-5" onClick={handleLoadLabels}>Cargar etiquetas</button>
                                        {
                                            !downloadingUnlabeledCSV ?
                                                <button className="btn btn-primary bold col-sm-5 ml-2" onClick={handleDownloadUnlabeled}>Exportar CSV</button>
                                            :
                                                <button type="button" className="btn btn-warning col-sm-5 ml-2 button-spinner" onClick={() => {}}>
                                                    <div className="row">
                                                        <div className="pt-1">Exportando...</div>
                                                        <Spinner></Spinner>
                                                    </div>
                                                </button>
                                        }
                                    </div>
                                : studyStates.length > 0 ?
                                    <div className="row flex-end">
                                        <button className="btn btn-warning bold col-sm-8 not-allowed button-spinner">
                                            <div className="row">
                                                <div className="pt-1">{studyStates.filter(s => s.value === study.state)[0].text}...</div>
                                                <Spinner></Spinner>
                                            </div>
                                        </button>
                                    </div>
                                : <button className="btn btn-warning bold col-sm-12 not-allowed">{study.state}</button>
                            : <button className="btn btn-non-action bold col-sm-12 px-0 arrow-cursor zero-nine-rem">Proyecto no listo para estudios</button>
                        }
                    </div>
                    <div className="col-sm-2 ml-2">
                        <MDBDropdown>
                            <MDBDropdownToggle color='warning' className="px-1 col-sm-12">
                                ...
                                <i className="fa fa-chevron-down ml-1"></i>
                            </MDBDropdownToggle>
                            <MDBDropdownMenu>
                                <MDBDropdownItem>
                                    <MDBDropdownLink onClick={()=>setForceStateChangeModalOpened(true)}>Forzar cambio de estado</MDBDropdownLink>
                                </MDBDropdownItem>
                            </MDBDropdownMenu>
                        </MDBDropdown>
                    </div>
                    {
                        project && study ?
                            <React.Fragment>
                                <CSVLink
                                    data={dataForDownload}
                                    filename={csvFilename}
                                    ref={csvLink}
                                />
                                <CSVReader
                                    cssClass="hidden"
                                    onFileLoaded={handleOnFileLoaded}
                                    onError={handleOnFileLoadError}
                                    inputId="csv-upload"
                                />
                            </React.Fragment>
                        : ''
                    }
                </div>
			}>
			
			{ study ?
				<React.Fragment>
					<div className="col-sm-12">
						<div className="col-sm-12 row mb-3">
                            <div className="col-sm-3">
								<label className="fs-1-2">Estado</label>
								<input className="form-control" readOnly value={studyStates.length > 0 ? studyStates.filter(s => s.value === study.state)[0].text : study.state}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Descripcion</label>
								<input className="form-control" readOnly value={study.description}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Carpeta</label>
								<input className="form-control" readOnly value={study.id}></input>
							</div>
						</div>
						<div className="col-sm-12 row mb-3">
							<div className="col-sm-3">
								<label className="fs-1-2">Genomas en proyecto</label>
								<input className="form-control" readOnly value={study.genomes_count.total}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Genomas etiquetados</label>
								<input className="form-control" readOnly value={study.genomes_count.labeled_true + study.genomes_count.labeled_false}></input>
							</div>
                            <div className="col-sm-3 offset-sm-1">
                                <div className="col-sm-12 row">
                                    <div className="col-sm-6">
                                        <label className="fs-1-2">Etiquetados con SI</label>
                                    </div>
                                    <div className="col-sm-6">
                                        <label className="fs-1-2">Etiquetados con NO</label>
                                    </div>
                                </div>
                                
                                <div class="input-group">
                                    <input className="form-control" readOnly value={study.genomes_count.labeled_true}></input>
                                    <input className="form-control" readOnly value={study.genomes_count.labeled_false}></input>
                                </div>
							</div>
                            
						</div>
                        <div className="col-sm-12 row mb-3">
                            <div className="col-sm-3">
								<label className="fs-1-2">Tipo de estudio</label>
								<input className="form-control" readOnly value={study.studyType}></input>
						    </div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Arquitectura RNA</label>
								<textarea 
                                    className="form-control" 
                                    readOnly value={rna_arch_to_display(study.rna_arch)}
                                    rows={study && study.rna_arch ? Object.keys(study.rna_arch).length : 1}
                                ></textarea>
							</div>
                            <div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Genomas para evaluar</label>
								<input className="form-control" readOnly value={study.genomes_to_evaluate}></input>
						    </div>
						</div>
					</div>

                    <div className="row col-sm-12 pt-3 pb-2 mt-4 mb-3 border-bottom">
                        <div className="col-sm-8">
                            <h3 className="one-five-rem">Evaluaciones</h3>
                        </div>
                        <div className="col-sm-4">
                            {
                                study.state === "READY_FOR_QUESTIONS" ?
                                    <div className="row flex-end">
                                        {
                                            study.genomes_to_evaluate > 0 ?
                                                <button className="btn btn-primary bold col-sm-5" onClick={() => setEvaluateModalOpened(true)}>Evaluar</button>
                                            : ""
                                        }
                                    </div>
                                : ''
                            }
                        </div>
                    </div>
                    
                    <div className="mb-4">
                        <DataTable 
                            columns={columns} 
                            data={rows} 
                            theme="custom" 
                            pagination={true}
                            noDataComponent="No hay resultados."
                            paginationComponentOptions={paginationOptions}
                            customStyles={paginationStyles}
                            subHeader={false}
                        />
                    </div>

                    <div className="row col-sm-12 pt-3 pb-2 mb-3 border-bottom">
                        <div className="col-sm-8">
                            <h3 className="one-five-rem">Logs de entrenamiento de RNA</h3>
                        </div>
                        <div className="col-sm-4">
                        {
                            study.state === "READY_FOR_TRAINING" || study.state === "READY_FOR_QUESTIONS" ?
                                <div className="row flex-end">
                                    <button className="btn btn-primary bold col-sm-5" onClick={handleTrain}>Entrenar</button>
                                    {
                                        study.state === "READY_FOR_QUESTIONS" ?
                                            <button className="btn btn-primary bold col-sm-5 ml-2" onClick={handleOptimize}>Optimizar</button>
                                        : ''
                                    }
                                </div>
                            : ''
                        }
                        </div>
                    </div>

                    <div className="col-sm-12 training-logs-console">
                        {
                            study.training_logs && study.training_logs.length > 0 ?
                                study.training_logs.map(line =>
                                    line === "\n" ?
                                        <br/>
                                    :
                                        <div className="col-sm-12">{line.replaceAll("\b", "")}</div>
                                )
                            : '-'
                        }
                    </div>

                    <ModelAnalysis
                        project_id={project_id}
                        study_id={study_id}
                        getStudy={getStudy}
                        project={project}
                        study={study}
                    ></ModelAnalysis>

                    <TrainModal
						trainModalOpened={trainModalOpened}	
						trainModalData={trainModalData}
						closeTrainModal={() => setTrainModalOpened(false)}
					></TrainModal>
                    
                    <RemoveResultModal
                        removeModalOpened={removeModalOpened}
                        closeRemoveModal={closeRemoveModal}
                        removeModalData={removeModalData}
                        setReloadResults={setReloadResults}
			        ></RemoveResultModal>
                    
                    <EvaluateModal
						evaluateModalOpened={evaluateModalOpened}	
						evaluate={evaluate}
						closeEvaluateModal={() => setEvaluateModalOpened(false)}
					></EvaluateModal>

                    <ForceStateChangeStudyModal
						ForceStateChangeModalOpened={ForceStateChangeModalOpened}	
						projectId={project_id}
                        studyId={study_id}
						closeForceStateChangeModal={() => setForceStateChangeModalOpened(false)}
					></ForceStateChangeStudyModal>
					
				</React.Fragment>
				: ''
			}
			
        </Main>
    );
}

export default StudyDetail;