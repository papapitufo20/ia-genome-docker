import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Spinner from '../common/spinner/spinner';
import Modal from 'react-modal';
import { customConfirmationModalStyles, rna_arch_to_display } from '../../common';

Modal.setAppElement('#root');

function TrainModal(props) {

    const max_cpus = useSelector(state => state.cpus);
    const default_rna = useSelector(state => state.default_rna);

    const [cpus, setCpus] = useState(max_cpus);
    const [epochs, setEpochs] = useState(1500);
    const [learningRate, setLearningRate] = useState(0.0005);
    const [archRNA, setArchRNA] = useState(rna_arch_to_display(default_rna));
    const [error, setError] = useState('');
    const [preparing, setPreparing] = useState(false);
    const [percentageOfTrainingData, setPercentageOfTrainingData] = useState(90);

    const handleCpusChange = (e) => {
        let new_cpus_value = e.target.value !== "" ? parseInt(e.target.value) : "";
        setCpus(new_cpus_value);
    }

    const handleEpochsChange = (e) => {
        let new_epochs_value = e.target.value !== "" ? parseInt(e.target.value) : "";
        setEpochs(new_epochs_value);
    }

    const handleLearningRateChange = (e) => {
        let new_lr_value = e.target.value !== "" ? parseFloat(e.target.value) : "";
        setLearningRate(new_lr_value);
    }

    const handleRNAChange = (e) => {
        let new_rna_value = e.target.value;
        setArchRNA(new_rna_value);
    }

    const handlePercentageChange = (e) => {
        let new_percentage_value = e.target.value !== "" ? parseInt(e.target.value) : "";
        setPercentageOfTrainingData(new_percentage_value);
    }

    const parseRNA = (rna) => {
        
        let new_layers = [];
        
        for (let line of rna.split("\n")){
            let sliced = line.slice(line.indexOf(":")+1);
            let neurons_number = sliced.slice(0, sliced.indexOf("neuronas"));
            neurons_number = neurons_number.trim();
            if (!isNaN(neurons_number) && neurons_number > 0){
                new_layers[new_layers.length] = Number(neurons_number);
            }
            else return null;
        }

        return new_layers;
    }

    const handleOnClick = () => {
        if (cpus > 0 && cpus <= max_cpus){
            if (epochs > 0 && epochs <= 5000){
                if ((learningRate > 0 && learningRate <= 1) || props.trainModalData.optimize){
                    let rna = parseRNA(archRNA);
                    if ((rna && rna.length > 0) || props.trainModalData.optimize){
                        if (percentageOfTrainingData >= 50 && percentageOfTrainingData <= 100){
                            setPreparing(true);
                            props.trainModalData.callback(cpus, epochs, learningRate, rna, percentageOfTrainingData);
                        }
                        else setError('El porcentaje de datos para el entrenamiento debe estar entre 50 y 100');
                    }
                    else setError('La arquitectura de la RNA es invalida');
                }
                else {
                    setError('El learning rate debe estar entre 0 y 1');
                }
            }
            else {
                setError('La cantidad de pasadas debe estar entre 0 y 5000');
            }
        }
        else {
            setError('La cantidad de CPUs debe estar entre 0 y ' + max_cpus);
        }
    }

    useEffect(() => {
        setPreparing(false);
        if (props.trainModalOpened){
            setError('');
            setCpus(max_cpus);
            setEpochs(1500);
            setLearningRate(props.trainModalData.learning_rate);
            setArchRNA(rna_arch_to_display(props.trainModalData.rna_arch));
            setPercentageOfTrainingData(90);
        }
    }, [props.trainModalOpened]);

    return (
        <Modal
            isOpen={props.trainModalOpened}
            style={customConfirmationModalStyles}
            contentLabel="Project modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }

                {
                    props.trainModalData ?
                        <React.Fragment>
                            <h2 className="no-border">{ props.trainModalData.action + "?" }</h2>
                            <h4 className="no-border red-font bold">{ props.trainModalData.warning }</h4>

                            {/*<div className="col-sm-12 mt-3">
                                <label className="fs-1-2">Cantidad de CPUs</label>
                                <input className="form-control" value={cpus} onChange={handleCpusChange} type="number"></input>
                            </div>*/}

                            <div className="col-sm-12 mt-3">
                                <label className="fs-1-2">Cantidad de pasadas</label>
                                <input className="form-control" value={epochs} onChange={handleEpochsChange} type="number"></input>
                            </div>

                            <div className="col-sm-12 mt-3">
                                <label className="fs-1-2">Porcentaje de datos para entrenar (el resto se usa para testear el aprendizaje)</label>
                                <input className="form-control" value={percentageOfTrainingData} onChange={handlePercentageChange} type="number"></input>
                            </div>

                            {
                                !props.trainModalData.optimize ?
                                    <React.Fragment>
                                        <div className="col-sm-12 mt-3">
                                            <label className="fs-1-2">Learning rate</label>
                                            <input className="form-control" value={learningRate} onChange={handleLearningRateChange} type="number"></input>
                                        </div>

                                        <div className="col-sm-12 mt-3">
                                            <label className="fs-1-2">Arquitectura RNA</label>
                                            <textarea 
                                                className="form-control" 
                                                value={archRNA}
                                                onChange={handleRNAChange}
                                                rows={archRNA ? (archRNA.match(/\n/g) || []).length+1 : 1}
                                            ></textarea>
                                        </div>
                                    </React.Fragment>
                                : ''
                            }

                            <div className="col-sm-12 mt-3">
                                {
                                    !preparing ?
                                        <React.Fragment>
                                            <button type="button" className="btn btn-success mr-2" onClick={handleOnClick}>
                                                {props.trainModalData.action} <i className="fa fa-check ml-2"></i>
                                            </button>
                                                <button type="button" className="btn btn-danger" onClick={props.closeTrainModal}>
                                                Cancelar <i className="fa fa-times ml-2"></i>
                                            </button>
                                        </React.Fragment>
                                    :
                                        <button type="button" className="btn btn-warning col-sm-4 button-spinner" onClick={() => {}}>
                                            <div className="row">
                                                <div className="pt-1">Preparando...</div>
                                                <Spinner></Spinner>
                                            </div>
                                        </button>
                                }
                            </div>
                        </React.Fragment>
                    : ''
                }

            </div>
        </Modal>
    )
}

export default TrainModal;