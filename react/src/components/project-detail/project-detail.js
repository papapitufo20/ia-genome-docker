import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from "react-router-dom";

import axios from 'axios';
import { CONFIG } from '../../config';
import * as GENERIC_ACTIONS from '../../actions/generic_actions';

import Spinner from '../common/spinner/spinner';
import Main from '../common/main/main';
import Studies from '../studies/studies';
import ProcessModal from './process-modal';
import ForceStateChangeProjectModal from '../projects/force-state-change-project-modal/force-state-change-project-modal';
import { MDBDropdown, MDBDropdownMenu, MDBDropdownToggle, MDBDropdownItem, MDBDropdownLink } from 'mdb-react-ui-kit';

function ProjectDetail(props){

    let project_id = null;
    let pathname = useLocation().pathname;
    let splitted = pathname ? pathname.split("/") : [];
    if (splitted && splitted.length == 3){
        project_id = splitted[2];
    }

    const dispatch = useDispatch();
	const [rows, setRows] = useState([]);

	const [project, setProject] = useState(null);

	const projectStates = useSelector(state => state.project_states);

	const [processModalOpened, setProcessModalOpened] = useState(false);
	const [processModalData, setProcessModalData] = useState(null);
	const [ForceStateChangeModalOpened, setForceStateChangeModalOpened] = useState(false);
	

    const getProject = hideSpinner => {
		if (!hideSpinner){
			dispatch(GENERIC_ACTIONS.mainContentLoading(true));
		}
        axios.get(CONFIG.API_SERVER_ADDRESS+'/get_project/'+project_id,
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                let proj = response.data.result._source;
				let parsedRows = proj && proj.studies ?
					proj.studies.map((study, index) => {
							return {
								index: index+1,
								name: study.name,
								studyType:  study.studyType,
								description: study.description,
								id: study.id,
								state: study.state,
								nn_architecture: study.nn_architecture,
								genomes_count: study.genomes_count
							}
						})
					: [];
				setRows(parsedRows);
				setProject(proj);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError(''));
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError("Error al obtener proyectos"));
            });
    }

    useEffect(() => {
        document.title = "Proyecto";
        getProject();
		let refreshTimeout = setInterval(() => {
			getProject(true);
		}, CONFIG.REFRESH_TIMEOUT);
		return () => { clearInterval(refreshTimeout); }
	}, []);

	const genericProcess = (endpoint, type, cpus) => {
		dispatch(GENERIC_ACTIONS.mainContentLoading(true));
        axios.post(CONFIG.API_SERVER_ADDRESS+'/'+endpoint+'/'+project_id, 
			{
				cpus: cpus
			},
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                console.log(response);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
				getProject();
				setProcessModalOpened(false);
                //dispatch(GENERIC_ACTIONS.mainContentSuccess('Procesando '+type+'!'));
            })
            .catch(error => {
				setProcessModalOpened(false);
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError("Error al procesar "+type));
            });
	}

	const handleProcessFASTQ = () => {
		setProcessModalData({callback: (cpus) => genericProcess('process_fastq', 'FASTQ', cpus), type: 'FASTQ'})
		setProcessModalOpened(true);
	}

	const handleProcessFASTA = () => {
		setProcessModalData({callback: (cpus) => genericProcess('process_fasta', 'FASTA', cpus), type: 'FASTA'})
		setProcessModalOpened(true);
	}

	const handleProcessGBF = () => {
		setProcessModalData({callback: (cpus) => genericProcess('process_gbf', 'GBF', cpus), type: 'GBF'})
		setProcessModalOpened(true);
	}

    return (

        <Main title={"Proyecto: " + (project ? project.name : "")} className="projects-container pt-3" render_right={
				<div className="row flex-end">
					<div className="col-sm-8">
						{
							project === null ? '' :
							project.state === "INITIAL" || project.state === "READY_FOR_STUDIES" ?
								project.fastq_count > 0 ?
									<button className="btn btn-success bold col-sm-12" onClick={handleProcessFASTQ}>Procesar FASTQ</button>
								: project.fasta_count > 0 ?
									<button className="btn btn-success bold col-sm-12" onClick={handleProcessFASTA}>Procesar FASTA</button>
								: project.gbf_count > 0 ?
									<button className="btn btn-success bold col-sm-12" onClick={handleProcessGBF}>Procesar GBF</button>
								: <button className="btn btn-non-action bold col-sm-12 px-0 arrow-cursor">Nada para procesar</button> 
							: projectStates.length > 0 ?
								<div className="row flex-end">
									<button className="btn btn-warning bold col-sm-8 not-allowed button-spinner">
										<div className="row">
											<div className="pt-1">{projectStates.filter(s => s.value === project.state)[0].text}...</div>
											<Spinner></Spinner>
										</div>
									</button>
								</div>
							: <button className="btn btn-warning bold col-sm-12 not-allowed">{project.state}</button>
						}
					</div>
					<div className="col-sm-2 ml-2">
						<MDBDropdown>
							<MDBDropdownToggle color='warning' className="px-1 col-sm-12">
								...
								<i className="fa fa-chevron-down ml-1"></i>
							</MDBDropdownToggle>
							<MDBDropdownMenu>
								<MDBDropdownItem>
									<MDBDropdownLink onClick={()=>setForceStateChangeModalOpened(true)}>Forzar cambio de estado</MDBDropdownLink>
								</MDBDropdownItem>
							</MDBDropdownMenu>
						</MDBDropdown>
					</div>
				</div>
			}>
			
			{ project ?
				<React.Fragment>
					<div className="col-sm-12">
						<div className="col-sm-12 row mb-3">
							{/*<div className="col-sm-3">
								<label className="fs-1-2">Nombre</label>
								<input className="form-control" readOnly value={project.name}></input>
							</div>*/}
							<div className="col-sm-3">
								<label className="fs-1-2">Estado</label>
								<input className="form-control" readOnly value={projectStates.length > 0 ? projectStates.filter(s => s.value === project.state)[0].text : project.state}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Descripcion</label>
								<input className="form-control" readOnly value={project.description}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Reino</label>
								<input className="form-control" readOnly value={project.kingdom}></input>
							</div>
						</div>
						<div className="col-sm-12 row mb-3">
							<div className="col-sm-3">
								<label className="fs-1-2">FASTQ</label>
								<input className="form-control" readOnly value={project.fastq_count}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">FASTA</label>
								<input className="form-control" readOnly value={project.fasta_count}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">GBF</label>
								<input className="form-control" readOnly value={project.gbf_count}></input>
							</div>
						</div>
						<div className="col-sm-12 row mb-3">
							<div className="col-sm-3">
								<label className="fs-1-2">Genomas</label>
								<input className="form-control" readOnly value={project.genomes_count}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Genes</label>
								<input className="form-control" readOnly value={project.genes_count}></input>
							</div>
							<div className="col-sm-3 offset-sm-1">
								<label className="fs-1-2">Posibles alelos</label>
								<input className="form-control" readOnly value={project.alleles_count}></input>
							</div>
						</div>
						<div className="col-sm-12 row mb-3">
							<div className="col-sm-3">
								<label className="fs-1-2">Genomas con matriz de pertenencia</label>
								<input className="form-control" readOnly value={project.genomes_with_pertenence_count}></input>
							</div>
						</div>
					</div>
					
					<Studies 
						rows={rows}
						getProject={getProject}
						project_id={project_id}
					></Studies>

					<ProcessModal
						processModalOpened={processModalOpened}	
						processModalData={processModalData}
						closeProcessModal={() => setProcessModalOpened(false)}
					></ProcessModal>

					<ForceStateChangeProjectModal
						ForceStateChangeModalOpened={ForceStateChangeModalOpened}	
						projectId={project_id}
						closeForceStateChangeModal={() => setForceStateChangeModalOpened(false)}
					></ForceStateChangeProjectModal>


				</React.Fragment>
				: ''
			}
			
        </Main>
    );
}

export default ProjectDetail;