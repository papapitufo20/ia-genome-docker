import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Spinner from '../common/spinner/spinner';
import Modal from 'react-modal';
import { customConfirmationModalStyles } from '../../common';

Modal.setAppElement('#root');

function ProcessModal(props) {

    const max_cpus = useSelector(state => state.cpus);

    const [cpus, setCpus] = useState(max_cpus);
    const [error, setError] = useState('');
    const [preparing, setPreparing] = useState(false);

    const handleCpusChange = (e) => {
        let new_cpus_value = e.target.value;
        setCpus(new_cpus_value);
    }

    const handleOnClick = () => {
        if (cpus > 0 && cpus <= max_cpus){
            setPreparing(true);
            props.processModalData.callback(cpus);
        }
        else {
            setError('La cantidad de CPUs debe estar entre 1 y ' + max_cpus);
        }
    }

    useEffect(() => {
        setPreparing(false);
        if (props.processModalOpened){
            setError('');
            setCpus(max_cpus);
        }
    }, [props.processModalOpened]);

    return (
        <Modal
            isOpen={props.processModalOpened}
            style={customConfirmationModalStyles}
            contentLabel="Project modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }

                {
                    props.processModalData ?
                        <React.Fragment>
                            <h2 className="no-border">{ "Procesar " + props.processModalData.type + "s?" }</h2>

                            <div className="col-sm-12 mt-3">
                                <label className="fs-1-2">Cantidad de CPUs</label>
                                <input className="form-control" value={cpus} onChange={handleCpusChange}></input>
                            </div>

                            <div className="col-sm-12 mt-3">
                                {
                                    !preparing ?
                                        <React.Fragment>
                                            <button type="button" className="btn btn-success mr-2" onClick={handleOnClick}>
                                                Procesar <i className="fa fa-check ml-2"></i>
                                            </button>
                                            <button type="button" className="btn btn-danger" onClick={props.closeProcessModal}>
                                                Cancelar <i className="fa fa-times ml-2"></i>
                                            </button>
                                        </React.Fragment>
                                    :
                                        <button type="button" className="btn btn-warning col-sm-4 button-spinner" onClick={() => {}}>
                                            <div className="row">
                                                <div className="pt-1">Preparando...</div>
                                                <Spinner></Spinner>
                                            </div>
                                        </button>
                                }
                            </div>
                        </React.Fragment>
                    : ''
                }

            </div>
        </Modal>
    )
}

export default ProcessModal;