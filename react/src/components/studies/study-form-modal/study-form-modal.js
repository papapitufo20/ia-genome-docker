import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { CONFIG } from '../../../config';

import Spinner from '../../common/spinner/spinner';
import Modal from 'react-modal';
import { customModalStyles } from '../../../common';

Modal.setAppElement('#root');

function StudyFormModal(props){

    const [modalTitle, setModalTitle] = useState("Nuevo estudio");
	const [studyId, setStudyId] = useState(null);
	const [studyName, setStudyName] = useState("");	
	const [studyDescription, setStudyDescription] = useState("");
    const [studyType, setStudyType] = useState("");
    const [studyTypeMessage, setStudyTypeMessage] = useState("");
    const [nameMessage, setNameMessage] = useState("");	
	const [descriptionMessage, setDescriptionMessage] = useState("");
	const [hideActions, setHideActions] = useState(true);
	const [error, setError] = useState('');
	const [success, setSuccess] = useState('');
    const [preparing, setPreparing] = useState(false);

    const handleNameChange = (e) => {
		let newName = e.target.value;
        newName = newName.replace(/[^a-zA-Z0-9 ]/g, "");
		setStudyName(newName);
		if (newName && newName.length >= 6 && newName.length <= 200){
			setNameMessage("");
			setHideActions(false);
		}
		else {
			setNameMessage("Ingrese un nombre entre 6 y 200 caracteres");
			setHideActions(true);
		}
	};

	const handleDescriptionChange = (e) => {
		let newDescription = e.target.value;
		setStudyDescription(newDescription);
		if (!newDescription || (newDescription && newDescription.length <= 400)){
			setDescriptionMessage("");
			setHideActions(false);
		}
		else {
			setDescriptionMessage("Ingrese una descripcion de hasta 400 caracteres");
			setHideActions(true);
		}
	};

	const handleSave = (idStudy) => {
		let path = "/"+props.projectId+"/create_study";
		if (idStudy){
			path = "/"+props.projectId+"/edit_study/"+idStudy;
		}
		setSuccess('');
		setError('');
        props.setReloadStudies(true);
        setPreparing(true);
		axios.post(CONFIG.API_SERVER_ADDRESS+path, {
				name: studyName,
				description: studyDescription,
                studyType: studyType
			},
			{
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				setSuccess(idStudy ? 'Guardado correctamente' : 'Creado correctamente');
				setError('');
                setHideActions(true);
                setPreparing(false);
            })
            .catch(error => {
                console.log(error);
                setSuccess('');
				setError((idStudy ? 'Error al guardar: ' : 'Error al crear estudio: ') + error.response.data.detail);
				setHideActions(false);
                setPreparing(false);
            });
	}

    useEffect(() => {
        setModalTitle(props.modalData.title);
        setStudyId(props.modalData.studyId);
        setStudyName(props.modalData.studyName);
        setStudyDescription(props.modalData.studyDescription);
    }, [props.modalData]);

    useEffect(() => {
        setPreparing(false);
        if (props.modalOpened){
            setSuccess('');
		    setError('');
		    setHideActions(true);
        }
    }, [props.modalOpened]);

    const handleStudyTypeChange = (e) => {
        let newStudyType = e.target.value;
		setStudyType(newStudyType);
		if (newStudyType !== ""){
			setStudyTypeMessage("");
			setHideActions(false);
		}
		else {
			setStudyTypeMessage("Seleccione entre posibles genes unicos o posibles alelos");
			setHideActions(true);
		}
    }

    return (
        <Modal
            isOpen={props.modalOpened}
            style={customModalStyles}
            contentLabel="Study modal"
        >
            <div className="modal-container">

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }
                {
                    success !== '' ?
                        <div className="col-sm-12 alert alert-success">
                                <span>{success}</span>
                        </div>
                    //else
                    : ''
                }

                <h2>{ modalTitle }</h2>			
                
                <div className="row mb-3">
                    <label className="fs-1-2">Nombre:</label>
                    <input className={"form-control" + (studyId ? " read-only" : "")} value={studyName} onChange={handleNameChange} readOnly={studyId ? true : false}/>
                    <p className={"help-block error" + (nameMessage === "" ? " hidden" : "")}>{nameMessage}</p>
                </div>

                <div className="row mb-3">
                    <label className="fs-1-2">Descripcion:</label>
                    <input className="form-control" value={studyDescription} onChange={handleDescriptionChange}/>
                    <p className={"help-block error" + (descriptionMessage === "" ? " hidden" : "")}>{descriptionMessage}</p>
                </div>
                {
                    !studyId ?
                        <div className="row mb-3">
                            <label className="fs-1-2">Genes o alelos:</label>
                            <select className="form-control" value={studyType} onChange={handleStudyTypeChange}>
                                <option value="">Utilizar genes o posibles alelos?</option>
                                {
                                    ["Genes", "Alleles"].map(option => {
                                        return <option value={option}>{option}</option>
                                    })
                                }                        
                            </select>
                            <p className={"help-block error" + (studyTypeMessage === "" ? " hidden" : "")}>{studyTypeMessage}</p>
                        </div>
                        :''
                }

                {
                    !hideActions ?
                        <div className="col-sm-12">
                            {
                                !preparing ?
                                    <React.Fragment>
                                        <button type="button" className="btn btn-success mr-2" onClick={() => handleSave(studyId)}>
                                            {studyId ? "Guardar" : "Crear"}<i className="fa fa-check ml-2"></i>
                                        </button>
                                        <button type="button" className="btn btn-danger" onClick={props.closeModal}>
                                            Cancelar <i className="fa fa-times ml-2"></i>
                                        </button>
                                    </React.Fragment>
                                :
                                    <button type="button" className="btn btn-warning col-sm-2 button-spinner" onClick={() => {}}>
                                        <div className="row">
                                            <div className="pt-1">{studyId ? "Guardando..." : "Creando..."}</div>
                                            <Spinner></Spinner>
                                        </div>
                                    </button>
                            }
                        </div>
                    //else
                    : 
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-primary" onClick={props.closeModal}>Cerrar</button>
                        </div>
                }


            </div>
        </Modal>
    )
}

export default StudyFormModal;