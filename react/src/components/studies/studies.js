import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";

import DataTable, { createTheme } from 'react-data-table-component';
import { tableTheme, paginationStyles, paginationOptions } from '../../common';

import StudyFormModal from './study-form-modal/study-form-modal';
import RemoveStudyModal from './remove-study-modal/remove-study-modal';

createTheme('custom', tableTheme);

const buildColumns = (handleEditStudy, handleRemoveStudy, handleGoToStudy, studyStates, project_id) => {
	return [
		{
			name: 'ID',
			selector: 'id',
			sortable: false,
			omit: true
		},
		{
			name: '#',
			selector: 'index',
			sortable: false,
			width: '5rem',
			style: {
				height: '80px'
			}
		},
		{
			name: 'Nombre',
			selector: 'name',
			sortable: true,
			width: '9%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<a className="custom-link white-font" href={"/project/"+project_id+"/study/"+row.id}>{row.name}</a>
				</div>
		},
		{
			name: 'Descripcion',
			selector: 'description',
			sortable: true,
			width: '13%',
		},
		{
			name: 'Tipo',
			selector: 'studyType',
			sortable: true,
			width: '11%',
		},
		{
			name: 'Carpeta',
			selector: 'id',
			sortable: true,
			width: '11%',
		},
		{
			name: 'Estado',
			sortable: true,
			width: '11%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{studyStates.length > 0 ? studyStates.filter(s => s.value === row.state)[0].text : row.state}
				</div>
		},
		{
			name: 'Genomas en proyecto',
			sortable: true,
			width: '11%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{row.genomes_count.total}
				</div>
		},
		{
			name: 'Genomas etiquetados',
			sortable: true,
			width: '12%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{row.genomes_count.labeled_true + row.genomes_count.labeled_false}
				</div>
		},
		{
			name: 'Acciones',
			selector: '',
			sortable: false,
			width: 'calc(20% - 5rem)',
			style: {
				height: '80px'
			},
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<button className="btn btn-primary btn-circle mr-2" onClick={() => handleGoToStudy(row)}><i className="fa fa-eye"></i></button>
					<button className="btn btn-success btn-circle mr-2" onClick={() => handleEditStudy(row)}><i className="fa fa-pencil-alt"></i></button>
					<button className="btn btn-danger btn-circle" onClick={() => handleRemoveStudy(row)}><i className="fa fa-trash"></i></button>
				</div>
		}
	]};

function Studies(props){

	const history = useHistory();
	const studyStates = useSelector(state => state.study_states);

    const [modalOpened, setModalOpened] = useState(false);
	const [modalData, setModalData] = useState({
		title: "Crear estudio",
		studyId: null,
		studyName: "",
		studyDescription: "",
	});

	const closeModal = () => {
		setModalOpened(false);
		if (reloadStudies){
			props.getProject();
		}
		setReloadStudies(false);
	};

	const handleNewStudy = () => {
		setModalData({
			title: "Crear estudio",
			studyId: null,
			studyName: "",
			studyDescription: "",
		});
		setModalOpened(true);
	};

	const handleEditStudy = (study) => {
		setModalData({
			title: "Editar estudio",
			studyId: study.id,
			studyName: study.name,
			studyDescription: study.description,
		});
		setModalOpened(true);
	};

	const [removeModalOpened, setRemoveModalOpened] = useState(false);
	const [removeModalData, setRemoveModalData] = useState({
		studyId: null,
		studyName: "",
		studyDescription: ""
	});

	const closeRemoveModal = () => {
		setRemoveModalOpened(false);
		if (reloadStudies){
			props.getProject();
		}
		setReloadStudies(false);
	};

	const handleRemoveStudy = (study) => {
		setRemoveModalData({
			studyId: study.id,
			studyName: study.name,
			studyDescription: study.description
		});
		setRemoveModalOpened(true);
	}

    const handleGoToStudy = (study) => {
		history.push("/project/"+props.project_id+"/study/"+study.id);
		history.go();
	}

	const [reloadStudies, setReloadStudies] = useState(false);
	const columns = buildColumns(handleEditStudy, handleRemoveStudy, handleGoToStudy, studyStates, props.project_id);

    return (
        <React.Fragment>

			<div className="row col-sm-12 pt-3 pb-2 mb-3 border-bottom">
				<div className="col-sm-8">
					<h3 className="one-five-rem">Estudios</h3>
				</div>
				<div className="col-sm-4">
					<div className="col-sm-10 offset-sm-2">
						<button className="btn btn-success bold col-sm-12" onClick={handleNewStudy}>Nuevo estudio</button>
					</div>
				</div>
			</div>

            <DataTable 
                columns={columns} 
                data={props.rows} 
                theme="custom" 
                pagination={true}
                noDataComponent="No hay resultados."
                paginationComponentOptions={paginationOptions}
                customStyles={paginationStyles}
                subHeader={false}
            />
            
            <StudyFormModal
                modalOpened={modalOpened}
                closeModal={closeModal}
                modalData={modalData}
                setReloadStudies={setReloadStudies}
				projectId={props.project_id}
            ></StudyFormModal>

            <RemoveStudyModal
                removeModalOpened={removeModalOpened}
                closeRemoveModal={closeRemoveModal}
                removeModalData={removeModalData}
                setReloadStudies={setReloadStudies}
				projectId={props.project_id}
            ></RemoveStudyModal>

        </React.Fragment>
    )
}
export default Studies