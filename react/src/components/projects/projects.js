import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";

import axios from 'axios';
import { CONFIG } from '../../config';
import * as GENERIC_ACTIONS from '../../actions/generic_actions';

import Main from '../common/main/main';
import DataTable, { createTheme } from 'react-data-table-component';
import { tableTheme, paginationStyles, paginationOptions } from '../../common';
import ProjectFormModal from './project-form-modal/project-form-modal';
import RemoveProjectModal from './remove-project-modal/remove-project-modal';

createTheme('custom', tableTheme);

const buildColumns = (handleEditProject, handleRemoveProject, handleGoToProject, projectStates) => {
	return [
		{
			name: 'ID',
			selector: 'id',
			sortable: false,
			omit: true
		},
		{
			name: '#',
			selector: 'index',
			sortable: false,
			width: '5rem',
			style: {
				height: '80px'
			}
		},
		{
			name: 'Nombre',
			selector: 'name',
			sortable: true,
			width: '12%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<a className="custom-link white-font" href={"/project/"+row.id}>{row.name}</a>
				</div>
		},
		/*{
			name: 'Descripcion',
			selector: 'description',
			sortable: true,
			width: '15%',
			cell: row =>
				<div data-tag="allorRowEvents" className="break-spaces">
					{row.description}
				</div>
		},*/
		{
			name: 'Carpeta',
			selector: 'folder',
			sortable: true,
			width: '11%',
		},
		{
			name: 'Reino',
			selector: 'kingdom',
			sortable: true,
			width: '7%',
		},
		{
			name: 'Genomas',
			selector: 'genomes_count',
			sortable: true,
			width: '7%',
		},
		{
			name: 'Genes',
			selector: 'genes_count',
			sortable: true,
			width: '7%',
		},
		{
			name: 'Posibles alelos',
			selector: 'alleles_count',
			sortable: true,
			width: '7%',
		},
		{
			name: 'Genomas con matriz',
			selector: 'genomes_with_pertenence_count',
			sortable: true,
			width: '7%',
		},
		{
			name: 'Estado',
			sortable: true,
			width: '8%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{projectStates.length > 0 ? projectStates.filter(s => s.value === row.state)[0].text : row.state}
				</div>
		},
		{
			name: 'FASTQ',
			sortable: true,
			width: '5%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{row.fastq_count}
				</div>
		},
		{
			name: 'FASTA',
			sortable: true,
			width: '5%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{row.fasta_count}
				</div>
		},
		{
			name: 'GBF',
			sortable: true,
			width: '5%',
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					{row.gbf_count}
				</div>
		},
		{
			name: 'Acciones',
			selector: '',
			sortable: false,
			width: 'calc(20% - 5rem)',
			style: {
				height: '80px'
			},
			cell: row => 
				<div data-tag="allowRowEvents col-sm-12">
					<button className="btn btn-primary btn-circle mr-2" onClick={() => handleGoToProject(row)}><i className="fa fa-eye"></i></button>
					<button className="btn btn-success btn-circle mr-2" onClick={() => handleEditProject(row)}><i className="fa fa-pencil-alt"></i></button>
					<button className="btn btn-danger btn-circle mr-2" onClick={() => handleRemoveProject(row)}><i className="fa fa-trash"></i></button>
				</div>
		}
	]};

function Projects(){

    const dispatch = useDispatch();
	const [rows, setRows] = useState([]);
	const history = useHistory();

	const projectStates = useSelector(state => state.project_states);

    const getProjects = (hideSpinner) => {
        if (!hideSpinner){
			dispatch(GENERIC_ACTIONS.mainContentLoading(true));
		}
        axios.get(CONFIG.API_SERVER_ADDRESS+'/get_projects',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				let parsedRows = []
				if (response.data.results.hits){ 
					parsedRows = response.data.results.hits.hits.map((project, index) => {
						return {
							index: index+1,
							id: project._id,
							name: project._source.name,
							description: project._source.description,
							kingdom: project._source.kingdom,
							fastq_count: project._source.fastq_count,
							fasta_count: project._source.fasta_count,
							gbf_count: project._source.gbf_count,
							state: project._source.state,
							genomes_count: project._source.genomes_count,
							genes_count: project._source.genes_count,
							alleles_count: project._source.alleles_count,
							folder: project._source.folder,
							genomes_with_pertenence_count: project._source.genomes_with_pertenence_count
						}
					});
				}
				setRows(parsedRows);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError(''));
            })
            .catch(error => {
                console.log(error);
                dispatch(GENERIC_ACTIONS.mainContentLoading(false));
                dispatch(GENERIC_ACTIONS.mainContentError("Error al obtener proyectos"));
            });
    }

    useEffect(() => {
        document.title = "Proyectos";
        getProjects();
		let refreshTimeout = setInterval(() => {
			getProjects(true);
		}, CONFIG.REFRESH_TIMEOUT);
		return () => { clearInterval(refreshTimeout); }
	}, []);

	const handleGoToProject = (project) => {
		history.push("project/"+project.id);
		history.go();
	}

	const [modalOpened, setModalOpened] = useState(false);
	const [modalData, setModalData] = useState({
		title: "Crear proyecto",
		projectId: null,
		projectName: "",
		projectDescription: "",
		projectKingdom: ""
	});
	const closeModal = () => {
		setModalOpened(false);
		if (reloadProjects){
			getProjects();
		}
		setReloadProjects(false);
	};
	const handleNewProject = () => {
		setModalData({
			title: "Crear proyecto",
			projectId: null,
			projectName: "",
			projectDescription: "",
			projectKingdom: ""
		});
		setModalOpened(true);
	};
	const handleEditProject = (project) => {
		setModalData({
			title: "Editar proyecto",
			projectId: project.id,
			projectName: project.name,
			projectDescription: project.description,
			projectKingdom: project.kingdom
		});
		setModalOpened(true);
	};

	const [removeModalOpened, setRemoveModalOpened] = useState(false);
	const [removeModalData, setRemoveModalData] = useState({
		projectId: null,
		projectName: "",
		projectDescription: ""
	});
	const closeRemoveModal = () => {
		setRemoveModalOpened(false);
		if (reloadProjects){
			getProjects();
		}
		setReloadProjects(false);
	};
	const handleRemoveProject = (project) => {
		setRemoveModalData({
			projectId: project.id,
			projectName: project.name,
			projectDescription: project.description
		});
		setRemoveModalOpened(true);
	}

	const [reloadProjects, setReloadProjects] = useState(false);
	const columns = buildColumns(handleEditProject, handleRemoveProject, handleGoToProject, projectStates);

    return (

        <Main title="Proyectos" className="projects-container pt-3" render_right={
				<div className="col-sm-10 offset-sm-2">
					<button className="btn btn-success bold col-sm-12" onClick={handleNewProject}>Nuevo proyecto</button>
				</div>
			}>
			<DataTable 
				columns={columns} 
				data={rows} 
				theme="custom" 
				pagination={true}
				noDataComponent="No hay resultados."
				paginationComponentOptions={paginationOptions}
				customStyles={paginationStyles}
				subHeader={false}
			/>
			<ProjectFormModal
				modalOpened={modalOpened}
				closeModal={closeModal}
				modalData={modalData}
				setReloadProjects={setReloadProjects}
			></ProjectFormModal>
			<RemoveProjectModal
				removeModalOpened={removeModalOpened}
				closeRemoveModal={closeRemoveModal}
				removeModalData={removeModalData}
				setReloadProjects={setReloadProjects}
			></RemoveProjectModal>
        </Main>
    );
}

export default Projects;