import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { CONFIG } from '../../../config';

import Modal from 'react-modal';
import { customConfirmationModalStyles } from '../../../common';

Modal.setAppElement('#root');

function ForceStateChangeProjectModal(props){
    
	const [error, setError] = useState('');
	const [success, setSuccess] = useState('');
    const [hideActions, setHideActions] = useState(false);

	const handleForceStateChange = () => {
		setSuccess('');
		setError('');
		axios.post(CONFIG.API_SERVER_ADDRESS+"/project/"+props.projectId+"/force_state_chage_project", {
			},
			{
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				setSuccess('Estado cambiado exitosamente');
                setHideActions(true);
				setError('');
            })
            .catch(error => {
                console.log(error);
                setHideActions(false);
                setSuccess('');
				setError('Error al cambiar estado');
            });
	}

    useEffect(() => {
        if (props.ForceStateChangeModalOpened){
            setHideActions(false);
            setSuccess('');
		    setError('');
        }
    }, [props.ForceStateChangeModalOpened]);

    return (
        <Modal
            isOpen={props.ForceStateChangeModalOpened}
            style={customConfirmationModalStyles}
            contentLabel="Project modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }
                {
                    success !== '' ?
                        <div className="col-sm-12 alert alert-success">
                                <span>{success}</span>
                        </div>
                    //else
                    : ''
                }

                <h2 className="no-border">{ "Forzar cambio de estado del proyecto?" }</h2>
                <h5 className='white-font'>Esta acción solo debe ser utilizada si el proyecto se encuentra en un estado invalido</h5>
                {!hideActions ?
                    <div className="col-sm-12 mt-3">
                        <button type="button" className="btn btn-success mr-2" onClick={() => handleForceStateChange()}>
                            Forzar <i className="fa fa-check ml-2"></i>
                        </button>
                        <button type="button" className="btn btn-danger" onClick={props.closeForceStateChangeModal}>
                            Cancelar <i className="fa fa-times ml-2"></i>
                        </button>
                    </div>
                :
                    <div className="col-sm-12">
                        <button type="button" className="btn btn-primary" onClick={props.closeForceStateChangeModal}>Cerrar</button>
                    </div>
                }
            </div>
        </Modal>
    )
}

export default ForceStateChangeProjectModal;