import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { CONFIG } from '../../../config';

import Spinner from '../../common/spinner/spinner';
import Modal from 'react-modal';
import { customConfirmationModalStyles } from '../../../common';

Modal.setAppElement('#root');

function RemoveProjectModal(props){
    
	const [error, setError] = useState('');
	const [success, setSuccess] = useState('');
    const [hideActions, setHideActions] = useState(true);
    const [preparing, setPreparing] = useState(false);

	const handleRemove = () => {
		setSuccess('');
		setError('');
        props.setReloadProjects(true);
        setPreparing(true);
		axios.post(CONFIG.API_SERVER_ADDRESS+"/delete_project", {
				id: props.removeModalData.projectId,
			},
			{
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				setSuccess('Eliminado correctamente');
				setError('');
                setHideActions(true);
                setPreparing(false);
            })
            .catch(error => {
                console.log(error);
                setSuccess('');
				setError('Error al eliminar');
                setHideActions(false);
                setPreparing(false);
            });
	}

    useEffect(() => {
        setPreparing(false);
        if (props.removeModalOpened){
            setSuccess('');
		    setError('');
		    setHideActions(false);
        }
    }, [props.removeModalOpened]);

    return (
        <Modal
            isOpen={props.removeModalOpened}
            style={customConfirmationModalStyles}
            contentLabel="Project modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }
                {
                    success !== '' ?
                        <div className="col-sm-12 alert alert-success">
                                <span>{success}</span>
                        </div>
                    //else
                    : ''
                }

                <h2 className="no-border">{ "Eliminar proyecto '" + props.removeModalData.projectName + "'?" }</h2>

                {
                    !hideActions ?
                        !preparing ?
                            <React.Fragment>
                                <div className="col-sm-12 mt-3">
                                    <button type="button" className="btn btn-success mr-2" onClick={() => handleRemove()}>
                                        Eliminar <i className="fa fa-check ml-2"></i>
                                    </button>
                                    <button type="button" className="btn btn-danger" onClick={props.closeRemoveModal}>
                                        Cancelar <i className="fa fa-times ml-2"></i>
                                    </button>
                                </div>
                            </React.Fragment>
                        :
                            <button type="button" className="btn btn-warning col-sm-4 button-spinner" onClick={() => {}}>
                                <div className="row">
                                    <div className="pt-1">Eliminando...</div>
                                    <Spinner></Spinner>
                                </div>
                            </button>
                    //else
                    : 
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-primary" onClick={props.closeRemoveModal}>Cerrar</button>
                        </div>
                }

            </div>
        </Modal>
    )
}

export default RemoveProjectModal;