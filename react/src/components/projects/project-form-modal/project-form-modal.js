import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { CONFIG } from '../../../config';

import Spinner from '../../common/spinner/spinner';
import Modal from 'react-modal';
import { customModalStyles } from '../../../common';

Modal.setAppElement('#root');

function ProjectFormModal(props){
    
    const [modalTitle, setModalTitle] = useState("Nuevo proyecto");
	const [projectId, setProjectId] = useState(null);
	const [projectName, setProjectName] = useState("");	
	const [projectDescription, setProjectDescription] = useState("");
	const [projectKingdom, setProjectKingdom] = useState("");

    const [nameMessage, setNameMessage] = useState("");	
	const [descriptionMessage, setDescriptionMessage] = useState("");
	const [kingdomMessage, setKingdomMessage] = useState("");
	const [hideActions, setHideActions] = useState(true);
	const [error, setError] = useState('');
	const [success, setSuccess] = useState('');
    const [preparing, setPreparing] = useState(false);

    const handleNameChange = (e) => {
		let newName = e.target.value;
        newName = newName.replace(/[^a-zA-Z0-9 ]/g, "");
		setProjectName(newName);
		if (newName && newName.length >= 6 && newName.length <= 200){
			setNameMessage("");
			setHideActions(false);
		}
		else {
			setNameMessage("Ingrese un nombre entre 6 y 200 caracteres");
			setHideActions(true);
		}
	};

	const handleDescriptionChange = (e) => {
		let newDescription = e.target.value;
		setProjectDescription(newDescription);
		if (!newDescription || (newDescription && newDescription.length <= 400)){
			setDescriptionMessage("");
			setHideActions(false);
		}
		else {
			setDescriptionMessage("Ingrese una descripcion de hasta 400 caracteres");
			setHideActions(true);
		}
	};

    const handleKingdomChange = (e) => {
        let newKingdom = e.target.value;
		setProjectKingdom(newKingdom);
		if (newKingdom !== ""){
			setKingdomMessage("");
			setHideActions(false);
		}
		else {
			setKingdomMessage("Seleccione un reino para el proyecto");
			setHideActions(true);
		}
    }

	const handleSave = (idProject) => {
		let path = "/create_project";
		if (idProject){
			path = "/edit_project/"+idProject;
		}
		setSuccess('');
		setError('');
        props.setReloadProjects(true);
        setPreparing(true);
		axios.post(CONFIG.API_SERVER_ADDRESS+path, {
				name: projectName,
				description: projectDescription,
                kingdom: projectKingdom
			},
			{
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
				setSuccess(idProject ? 'Guardado correctamente' : 'Creado correctamente');
				setError('');
                setHideActions(true);
                setPreparing(false);
            })
            .catch(error => {
                console.log(error);
                setSuccess('');
				setError((idProject ? 'Error al guardar: ' : 'Error al crear proyecto: ') + error.response.data.detail);
				setHideActions(false);
                setPreparing(false);
            });
	}

    useEffect(() => {
        setModalTitle(props.modalData.title);
        setProjectId(props.modalData.projectId);
        setProjectName(props.modalData.projectName);
        setProjectDescription(props.modalData.projectDescription);
        setProjectKingdom(props.modalData.projectKingdom);
    }, [props.modalData]);

    useEffect(() => {
        setPreparing(false);
        if (props.modalOpened){
            setSuccess('');
		    setError('');
		    setHideActions(true);
        }
    }, [props.modalOpened]);

    return (
        <Modal
            isOpen={props.modalOpened}
            style={customModalStyles}
            contentLabel="Project modal"
        >
            <div className="modal-container">		

                {
                    error !== '' ?
                        <div className="col-sm-12 alert alert-danger">
                                <span>{error}</span>
                        </div>
                    //else
                    : ''
                }
                {
                    success !== '' ?
                        <div className="col-sm-12 alert alert-success">
                                <span>{success}</span>
                        </div>
                    //else
                    : ''
                }

                <h2>{ modalTitle }</h2>			
                
                <div className="row mb-3">
                    <label className="fs-1-2">Nombre:</label>
                    <input className={"form-control" + (projectId ? " read-only" : "")} value={projectName} onChange={handleNameChange} readOnly={projectId ? true : false}/>
                    <p className={"help-block error" + (nameMessage === "" ? " hidden" : "")}>{nameMessage}</p>
                </div>

                <div className="row mb-3">
                    <label className="fs-1-2">Descripcion:</label>
                    <input className="form-control" value={projectDescription} onChange={handleDescriptionChange}/>
                    <p className={"help-block error" + (descriptionMessage === "" ? " hidden" : "")}>{descriptionMessage}</p>
                </div>
                {
                    !projectId ?
                        <div className="row mb-3">
                            <label className="fs-1-2">Reino:</label>
                            <select className="form-control" value={projectKingdom} onChange={handleKingdomChange}>
                                <option value="">Seleccione un reino</option>
                                {
                                    ["Bacteria", "Archaea", "Mitochondria", "Viruses"].map(option => {
                                        return <option value={option}>{option}</option>
                                    })
                                }                        
                            </select>
                            <p className={"help-block error" + (kingdomMessage === "" ? " hidden" : "")}>{kingdomMessage}</p>
                        </div>
                        :''
                }

                {
                    !hideActions ?
                        <div className="col-sm-12">
                            {
                                !preparing ?
                                    <React.Fragment>
                                        <button type="button" className="btn btn-success mr-2" onClick={() => handleSave(projectId)}>
                                            {projectId ? "Guardar" : "Crear"}<i className="fa fa-check ml-2"></i>
                                        </button>
                                        <button type="button" className="btn btn-danger" onClick={props.closeModal}>
                                            Cancelar <i className="fa fa-times ml-2"></i>
                                        </button>
                                    </React.Fragment>
                                :
                                    <button type="button" className="btn btn-warning col-sm-2 button-spinner" onClick={() => {}}>
                                        <div className="row">
                                            <div className="pt-1">{projectId ? "Guardando..." : "Creando..."}</div>
                                            <Spinner></Spinner>
                                        </div>
                                    </button>
                            }
                        </div>
                    //else
                    : 
                        <div className="col-sm-12">
                            <button type="button" className="btn btn-primary" onClick={props.closeModal}>Cerrar</button>
                        </div>
                }

            </div>
        </Modal>
    )
}

export default ProjectFormModal;