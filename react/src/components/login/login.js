import React, { useEffect, useState } from 'react';
import './login.css';
import * as AUTH_ACTIONS from '../../actions/auth_actions';
import { useDispatch, useSelector } from 'react-redux';

function Login(){

    const dispatch = useDispatch();
	const login_error = useSelector(state => state.login_error);

	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(AUTH_ACTIONS.login(username, password));
	};

	useEffect(() => {
		document.title = "Iniciar sesión | Dashboard";
	}, []);

	return (
		<div className="login col-sm-4 offset-sm-4">
			{
				login_error !== '' ?
					<div className="col-sm-12 alert alert-danger">
	                        <span>{login_error}</span>
					</div>
				//else
				: ''
			}
			<h1 className="col-sm-12 pb-1 border-bottom">Iniciar sesión</h1>
			<form className="col-sm-12 text-align-left" onSubmit={handleSubmit}>
				<div className="form-group item-form">
					<label className="fs-1-2">Nombre de usuario</label>
					<input autoFocus type="text" className="form-control" placeholder="Nombre de usuario" value={username} onChange={(e) => setUsername(e.target.value)}></input>
				</div>
				<div className="form-group item-form">
					<label className="fs-1-2">Contraseña</label>
					<input type="password" className="form-control" placeholder="Contraseña" value={password} onChange={(e) => setPassword(e.target.value)}></input>
				</div>
				<button type="submit" className="btn btn-primary col-sm-12">Iniciar sesion</button>
			</form>
		</div>        
	);
}

export default Login;