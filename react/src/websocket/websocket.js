import * as NOTIFICATIONS_ACTIONS from '../actions/notifications_actions';
import { CONFIG } from '../config';

let ws = null;

export const initWebSocket = (store) => {

    console.log("open websocket");

    let token = localStorage.getItem('access_token');
    ws = new WebSocket(CONFIG.WS_SERVER_ADDRESS+"?token="+token);
    
    // Connection opened
    ws.onopen = (event) => {
        console.log("websocket opened");

        //EXAMPLE TO SEND MESSAGE
        /*let msg = {
            type: 'something',
            data: 'some text'
        };
        sendMessage(msg);*/
    };

    // Listen for messages
    ws.onmessage = (event) => {
        console.log(event);
        let message = JSON.parse(event.data);
        if (message.type === "ACK"){
            console.log(message.data);
        }
        if (message.type === "notification"){
            newNotification(message.data);
        }
    };

    ws.onclose = () => {
        console.log("websocket closed");
        token = localStorage.getItem('access_token');
        if (token){ //if is authenticated try reopen websocket, or something
            console.log("websocket reconnecting in 5 sec");
            setTimeout(() => initWebSocket(store), 5000);
        }
    };
    
    const newNotification = (notification) => {
        console.log(notification);
        if (store){
            store.dispatch(NOTIFICATIONS_ACTIONS.appendNotificationFromWebSocket(notification));
        }
    };
}

export const closeWebSocket = () => {
    if (ws != null){
        console.log("close websocket");
        ws.close();
    }
}

/*
export const sendMessage = (msg) => {
    if (ws != null){ // && ws.readyState !== WebSocket.CLOSED){
        console.log("send websocket message");
        ws.send(JSON.stringify(msg));
    }
}*/