import * as ACTION_TYPES from './action_types';
import axios from 'axios';
import { CONFIG } from '../config';

export const checkAuth = () => {
    return dispatch => {
        
        const accessToken = localStorage.getItem('access_token');
        const username = localStorage.getItem('username');
        if (accessToken && username){
            dispatch(loginSuccess(username));
            /*axios.get(CONFIG.API_SERVER_ADDRESS+'/user/',
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    }
                })
                .then(response => {
                    const username = response.data.username;
                    const group = response.data.groups && response.data.groups[0] ? response.data.groups[0].name : '' ;
                    localStorage.setItem('group', group); 
                    if (username && username !== ""){
                        const user = {...response.data};
                        dispatch(loginSuccess(user, group));
                    }
                    else {
                        dispatch(logout("La sesión ha expirado"));
                    }
                    
                })
                .catch(error => {
                    dispatch(loginFailure(error.message));
                });*/
        }
        else {
            dispatch(logoutClient(""));
        }
    }
}

export const login = (username, password) => {
    return dispatch => {
        dispatch(loginRequest()); //to set 'loading' in true

        axios.post(CONFIG.API_SERVER_ADDRESS+'/login', {
                username: username,
                password: password
            })
            .then(response => {
                localStorage.setItem('access_token', response.data.token);                
                localStorage.setItem('username', response.data.username);                
                dispatch(loginSuccess(response.data.username));
            })
            .catch(error => {
                console.log(error);
                if (
                    (error.message.includes("400")) || 
                    (error.message.includes("404")) || 
                    (error.message.includes("401"))
                ){
                    dispatch(loginFailure("Usuario y/o contraseña incorrectos"));
                }
                else {
                    dispatch(loginFailure("Error inesperado, vuelva a intentarlo"));
                }
            });
    }
}

export const loginRequest = () => {
    return {
        type: ACTION_TYPES.LOGIN_REQUEST
    }
}

export const loginSuccess = (user) => {
    return {
        type: ACTION_TYPES.LOGIN_SUCCESS,
        payload: user
    }
}

export const loginFailure = error => {
    return {
        type: ACTION_TYPES.LOGIN_FAILURE,
        payload: error
    }
}

export const logout = error => {
    return dispatch => {
        axios.get(CONFIG.API_SERVER_ADDRESS+'/logout/',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                dispatch(logoutClient(error));
            })
            .catch(error => {
                console.log(error);
                dispatch(logoutClient(""));
            });
    }
}

export const logoutClient = error => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('username');
    return {
        type: ACTION_TYPES.LOGOUT,
        payload: error
    }
}

export const getDefaultValues = () => {
    return dispatch => {
        axios.get(CONFIG.API_SERVER_ADDRESS+'/default_values',
            {
                headers: {
                    'Authorization': 'Token ' + localStorage.getItem("access_token")
                }
            })
            .then(response => {
                const cpus = response.data.cpus;
                const default_rna = response.data.default_rna;
                dispatch(setMaxCPUs(cpus));
                dispatch(setDefaultRNA(default_rna));
            })
            .catch(error => {
                dispatch(setMaxCPUs(1));
            });
    }
}

export const setMaxCPUs = cpus => {
    return {
        type: ACTION_TYPES.SET_CPUS,
        payload: cpus
    }
}

export const setDefaultRNA = default_rna => {
    return {
        type: ACTION_TYPES.SET_DEFAULT_RNA,
        payload: default_rna
    }
}