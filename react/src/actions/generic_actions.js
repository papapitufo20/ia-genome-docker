import * as ACTION_TYPES from './action_types';

export const mainContentLoading = (loading) => {
    return {
        type: ACTION_TYPES.MAIN_CONTENT_LOADING,
        payload: loading
    }
}

export const mainContentError = (error) => {
    return {
        type: ACTION_TYPES.MAIN_CONTENT_ERROR,
        payload: error
    }
}

export const mainContentSuccess = (msg) => {
    return {
        type: ACTION_TYPES.MAIN_CONTENT_SUCCESS,
        payload: msg
    }
}