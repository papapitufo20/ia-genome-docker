import * as ACTION_TYPES from './action_types';
import axios from 'axios';
import { CONFIG } from '../config';
import * as AUTH_ACTIONS from './auth_actions';

export const getProjectStates = () => {
    return dispatch => {
        const accessToken = localStorage.getItem('access_token');
        if (accessToken){
            axios.get(CONFIG.API_SERVER_ADDRESS+'/get_project_states',
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    }
                })
                .then(response => {
                    const project_states = response.data;
                    dispatch(setProjectStates(project_states));              
                })
                .catch(error => {
                    console.log(error.message);
                });
        }
        else {
            dispatch(AUTH_ACTIONS.logoutClient(""));
        }
    }
}

export const setProjectStates = project_states => {
    return {
        type: ACTION_TYPES.SET_PROJECT_STATES,
        payload: project_states
    }
}