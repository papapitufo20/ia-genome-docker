import * as ACTION_TYPES from './action_types';
import axios from 'axios';
import { CONFIG } from '../config';
import * as AUTH_ACTIONS from './auth_actions';

export const getStudyStates = () => {
    return dispatch => {
        const accessToken = localStorage.getItem('access_token');
        if (accessToken){
            axios.get(CONFIG.API_SERVER_ADDRESS+'/get_study_states',
                {
                    headers: {
                        'Authorization': 'Token ' + localStorage.getItem("access_token")
                    }
                })
                .then(response => {
                    const study_states = response.data;
                    dispatch(setStudyStates(study_states));              
                })
                .catch(error => {
                    console.log(error.message);
                });
        }
        else {
            dispatch(AUTH_ACTIONS.logoutClient(""));
        }
    }
}

export const setStudyStates = study_states => {
    return {
        type: ACTION_TYPES.SET_STUDY_STATES,
        payload: study_states
    }
}