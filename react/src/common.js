export const customModalStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        width                 : '70%',
		height                : 'auto',
		minHeight             : '500px',
        maxHeight             : '850px',
		backgroundColor       : '#232529',//'#282c34',
		border                : '2px solid rgb(255 255 255)'
    }
};

export const customConfirmationModalStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        width                 : '40%',
		height                : 'auto',
		minHeight             : '200px',
        maxHeight             : '850px',
		backgroundColor       : '#232529',//'#282c34',
		border                : '2px solid rgb(255 255 255)'
    }
};

export const paginationStyles = {
	pagination: {
		style: {
        	color: 'white',
			fontSize: '1rem'
		},
		pageButtonsStyle: {
			fill: 'white'
		}
	}
}

export const tableTheme = {
    text: {
        primary: '#FFFFFF',
        secondary: '#FFFFFF',
    },
    background: {
        default: '#232529',//'#282c34',
    },
    context: {
        background: '#232529',//'#282c34',
        text: '#FFFFFF',
    }
}

export const paginationOptions = {								
    rowsPerPageText: 'Filas por página:', 
    rangeSeparatorText: 'de', 
    noRowsPerPage: false, 
    selectAllRowsItem: false, 
    selectAllRowsItemText: 'Todo'
}

export const formatDate = (str) => {
    var date = new Date(str);
    var now = new Date();
    now.setHours(0,0,0,0);

    var hours_offset = (now.getTime() - date.getTime()) / 36e5;
    
    var date_day = date.getDate();
    var now_day = now.getDate();

    var day_prefix = "";
    if (hours_offset > 24){
        return formatDate(str);
    }
    else {
        day_prefix = (now_day - date_day) === 1 ? "Ayer " : "Hoy ";
    }

    var hours = date.getHours();
    var minutes = date.getMinutes();
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    return day_prefix + hours + ':' + minutes + 'hs ';
}

export const formatDateToDDMMAA = (str) => {
    var date = new Date(str);
    var now = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();

    if (now.getDate() === date.getDate()) {
        return hours + ':' + minutes + 'hs ';
    } else {
        return parseInt(now.getDate()-1) + "-" + parseInt(now.getMonth()+1) + "-" + now.getFullYear().toString().substr(-2);        
    }
}

export const rna_arch_to_display = (rna_arch) => {
    let result = '';
    for (let key in rna_arch){
        result += 'Capa '+key+': ' + rna_arch[key]+' neuronas\n';
    }
    if (result !== ''){
        result = result.slice(0,-1);
    }
    return result;
}

const filterNone = () => {
    return NodeFilter.FILTER_ACCEPT;
}

export const getAllCommentNodes = (rootElem) => {
    var comments = [];
    // Fourth argument, which is actually obsolete according to the DOM4 standard, is required in IE 11
    var iterator = document.createNodeIterator(rootElem, NodeFilter.SHOW_COMMENT, filterNone, false);
    var curNode;
    while (curNode = iterator.nextNode()) {
        comments.push(curNode);
    }
    return comments;
}