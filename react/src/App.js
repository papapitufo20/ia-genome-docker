import './App.css';
import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import * as AUTH_ACTIONS from './actions/auth_actions';
import * as PROJECT_ACTIONS from './actions/project_actions';
import * as STUDY_ACTIONS from './actions/study_actions';
import axios from 'axios';
import * as WEBSOCKET from './websocket/websocket';

import Login from './components/login/login';
import Spinner from './components/common/spinner/spinner';
import Header from './components/common/header/header';
import Projects from './components/projects/projects';
import ProjectDetail from './components/project-detail/project-detail';
import StudyDetail from './components/study-detail/study-detail';
//import Notifications from './components/common/notifications/notifications';
import PageNotFound from './components/common/page-not-found/page-not-found.js';

function App() {

	const is_authenticated = useSelector(state => state.is_authenticated);
	const loading = useSelector(state => state.loading);
	const dispatch = useDispatch();

	const setHttpInterceptor = () => {
		axios.interceptors.response.use(response => {
			return response;
		}, error => {
			if (error.message.includes("401")) {
				dispatch(AUTH_ACTIONS.logoutClient("La sesión ha expirado"));
			}
			return Promise.reject(error);
		});
	}

	useEffect(() => {
		setHttpInterceptor();
		dispatch(AUTH_ACTIONS.checkAuth());
		dispatch(PROJECT_ACTIONS.getProjectStates());
		dispatch(STUDY_ACTIONS.getStudyStates());
		dispatch(AUTH_ACTIONS.getDefaultValues());
	}, []);

	useEffect(() => {
		if (is_authenticated){
			//WEBSOCKET.initWebSocket(props.store);
		}
		else {
			WEBSOCKET.closeWebSocket();
		}
	}, [is_authenticated]);

	return (
		<Router>
			{loading ?
				<Spinner></Spinner>
				: //else
				!is_authenticated ?
					<Switch>
						<Route exact={true} path="/">
							<Login />
						</Route>
						<Route path="*">
							<Redirect to="/"></Redirect>
						</Route>
					</Switch>
				: //else
				<div>
					<Header></Header>
					<Switch>
						<Route exact={true} path="/">
							<Redirect to="/projects"></Redirect>
						</Route>
						<Route exact={true} path="/projects">
							<Projects></Projects>
						</Route>
						<Route exact={true} path="/project/:project_id">
							<ProjectDetail></ProjectDetail>
						</Route>
						<Route exact={true} path="/project/:project_id/study/:study_id">
							<StudyDetail></StudyDetail>
						</Route>
						<Route path="*">
							<PageNotFound />
						</Route>
					</Switch>
				</div>
			}				
		</Router>
	);
}

export default App;
