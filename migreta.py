from datetime import datetime
from elasticsearch import Elasticsearch
import os

es_origen = Elasticsearch(
    ['192.168.0.109'], #'localhost'
    port=9200,
    http_auth=('elastic', 'matelisto'),
)
es_destino = Elasticsearch(
    ['192.168.0.68'], #'localhost'
    port=9200,
    http_auth=('elastic', 'matelisto'),
)

genomes_index = 'genomes-index'
genes_index = 'genes-index'

def migrate_genes_table():
    print("buscanding genes")
    res_genes = es_origen.search(index=genes_index, body={"query": {"match_all": {}}, "size": 50000})
    for hit in res_genes['hits']['hits']:
        response = es_destino.index(index=genes_index, body=hit['_source'])
        print(response)

def migrate_genomes_table():
    
    for i in range(0, 4):
        print("****************** buscanding genomes *******************")
        result = es_origen.search(index=genomes_index+"-name-sorted-2", body={
            "query": {
                "match_all": {}
            }, 
            "sort": [
                {
                    "name.raw": {
                        "order": "asc"
                    }
                }
            ], 
            "from": 800 * i, 
            "size": 800
        })
    
        for hit in result['hits']['hits']:
            response = es_destino.index(index=genomes_index, body=hit['_source'])
            print(response)

#migrate_genes_table()
migrate_genomes_table()